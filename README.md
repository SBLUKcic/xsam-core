# Xsam Core

[![](https://jitpack.io/v/com.gitlab.SBLUKcic/xsam-core.svg)](https://jitpack.io/#com.gitlab.SBLUKcic/xsam-core)
[![pipeline status](https://gitlab.com/SBLUKcic/xsam-core/badges/master/pipeline.svg)](https://gitlab.com/SBLUKcic/xsam-core/pipelines)
[![coverage report](https://gitlab.com/SBLUKcic/xsam-core/badges/master/coverage.svg)](https://sblukcic.gitlab.io/xsam-core/jacoco/index.html)
[![Javadocs](https://img.shields.io/badge/javadocs-yes-blue.svg)](https://sblukcic.gitlab.io/xsam-core/docs/javadoc/index.html)

Xsam Core is a library for working with Sam and Xsam file formats in Java.

Xsam Core is used in Xsamkit, available from the snap store:
 
 [![Get it from the Snap Store](https://snapcraft.io/static/images/badges/en/snap-store-black.svg)](https://snapcraft.io/xsamkit)

Built in Java 8, Xsam core contains a lot of functional programming concepts including stream based operation and predicates.

Hosted on JitPack and available for use with [gradle or maven](https://jitpack.io/#com.gitlab.SBLUKcic/xsam-core):

To use Xsam core with gradle:

```
allprojects {
  repositories {
    ...
	maven { url 'https://jitpack.io' }
  }
}
```

Add the dependency:

```
dependencies {
  implementation 'com.gitlab.SBLUKcic:xsam-core:Tag'
}
```

## Core Packages

### Model

Contains classes to model both Xsam and sam records and files, in both paired-end and single-end configuration. Factories are provided, as well as enums for certain characteristics of the Xsam read, such as footprints, mapping strands and mapping types. 

### Conversion

This package contains classes to convert from Sam -> Xsam and back again. The `XsamConverter` interface has two implementation, based on how to treat certain reads. For example in `XsamConverterZero`, repeat-repeat reads are treated as such. in `XsamConverterOne`, the same reads 
are treated as non-mapping XX reads.

### Filtering

Contains reusable functional methods for filtering, Xsam and sam reads based on their fields.

### IO

Serialization and de-serialization of XsamFiles is handled here.

### Sorted

A handful of Sorting methods is provided for sorting of Xsam Chunks. 

### Multithreading

Classes to help with distributing of Xsam reads in chunks to be processed by workers.  

### Utils

Utility classes for working with Xsam records. 



