package com.sbl.providers;

import com.sbl.model.PairedSamRecord;
import com.sbl.model.SamRecord;

import java.util.stream.Stream;

public class SamPairProviders {

  private static SamRecord recordUR1 = new SamRecord("SBL_XSNP077_ID:184296_BC1:CTATTTCAG_\t99\tmm01\t188710023\t70\t2S39M\t=\t188710074\t90\tNNGAATAATAATAGTGGGGGATCCACTAGGGCTGTGGCTCC\t~~IIIIIIIIIIIIIIIIIIIIIIIIIIHHHIIIIIIIIII\tPG:Z:novoalign\tAS:i:12\tUQ:i:12\tNM:i:0\tMD:Z:39\tPQ:i:20\tSM:i:70\tAM:i:70");
  private static SamRecord recordUR2 = new SamRecord("SBL_XSNP077_ID:184296_BC1:CTATTTCAG_\t147\tmm01\t188710074\t70\t39M\t=\t188710023\t-90\tTCCTTGGTCAGCTGTGTTTTAACTTGAGGAGGCCTTCCN\tIHIIIIIIIIIIIIIIIIIHIHIIIIIIIIIIIIIIII~\tPG:Z:novoalign\tAS:i:6\tUQ:i:6\tNM:i:1\tMD:Z:38A0\tPQ:i:20\tZS:Z:R\tSM:i:70\tAM:i:70");

  private static PairedSamRecord urPair = new PairedSamRecord(recordUR1, recordUR2);

  static Stream<PairedSamRecord> urPairProvider() { return Stream.of(urPair); }

}
