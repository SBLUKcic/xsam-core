package com.sbl.providers;

import com.sbl.conversion.XsamConverter;
import com.sbl.conversion.XsamConverterFactory;
import com.sbl.model.PairedRecord;
import com.sbl.model.SamRecord;
import com.sbl.model.XsamRecord;

import java.util.*;
import java.util.stream.Stream;

public class XsamRecordProviders {

  private static XsamRecord record1 = new XsamRecord("SBL_XSNP077_ID:184296_BC1:CTATTTCAG_\t99\tmm01\t188710023\t70\t2S39M\t=\t188710074\t90\tNNGAATAATAATAGTGGGGGATCCACTAGGGCTGTGGCTCC\t~~IIIIIIIIIIIIIIIIIIIIIIIIIIHHHIIIIIIIIII\txl:i:188710023\txr:i:188710061\txs:i:39\txd:A:f\txm:A:u\txa:A:\"\"\t\txL:i:188710023\txR:i:188710112\txS:i:90\txW:i:12\txP:i:0\txQ:i:0\txC:A:\"\"\txD:A:\"\"\tPG:Z:novoalign\tAS:i:12\tUQ:i:12\tNM:i:0\tMD:Z:39\tPQ:i:20\tSM:i:70\tAM:i:70");
  private static XsamRecord record2 = new XsamRecord("SBL_XSNP077_ID:184296_BC1:CTATTTCAG_\t147\tmm01\t188710074\t70\t39M\t=\t188710023\t-90\tTCCTTGGTCAGCTGTGTTTTAACTTGAGGAGGCCTTCCN\tIHIIIIIIIIIIIIIIIIIHIHIIIIIIIIIIIIIIII~\txl:i:188710074\txr:i:188710112\txs:i:39\txd:A:r\txm:A:u\txa:A:\"\"\t\txL:i:188710023\txR:i:188710112\txS:i:90\txW:i:12\txP:i:0\txQ:i:0\txC:A:\"\"\txD:A:\"\"\tPG:Z:novoalign\tAS:i:6\tUQ:i:6\tNM:i:1\tMD:Z:38A0\tPQ:i:20\tSM:i:70\tAM:i:70");
  private static XsamRecord dRecord1 = new XsamRecord("SBL_XSNP077_ID:184296_BC1:CTATTTCAG_\t99\tmm02\t188710023\t70\t2S39M\tmm01\t188710074\t90\tNNGAATAATAATAGTGGGGGATCCACTAGGGCTGTGGCTCC\t~~IIIIIIIIIIIIIIIIIIIIIIIIIIHHHIIIIIIIIII\txl:i:188710023\txr:i:188710061\txs:i:39\txd:A:f\txm:A:u\txa:A:\"\"\t\txL:i:188710023\txR:i:188710112\txS:i:90\txW:i:12\txP:i:0\txQ:i:0\txC:A:\"\"\txD:A:\"\"\tPG:Z:novoalign\tAS:i:12\tUQ:i:12\tNM:i:0\tMD:Z:39\tPQ:i:20\tSM:i:70\tAM:i:70");
  private static XsamRecord dRecord2 = new XsamRecord("SBL_XSNP077_ID:184296_BC1:CTATTTCAG_\t147\tmm01\t188710074\t70\t39M\tmm02\t188710023\t-90\tTCCTTGGTCAGCTGTGTTTTAACTTGAGGAGGCCTTCCN\tIHIIIIIIIIIIIIIIIIIHIHIIIIIIIIIIIIIIII~\txl:i:188710074\txr:i:188710112\txs:i:39\txd:A:r\txm:A:u\txa:A:\"\"\t\txL:i:188710023\txR:i:188710112\txS:i:90\txW:i:12\txP:i:0\txQ:i:0\txC:A:\"\"\txD:A:\"\"\tPG:Z:novoalign\tAS:i:6\tUQ:i:6\tNM:i:1\tMD:Z:38A0\tPQ:i:20\tSM:i:70\tAM:i:70");
  private static  XsamRecord qcRecord1 = new XsamRecord("SBL_XSNP077_ID:184296_BC1:CTATTTCAG_\t99\tmm02\t188710023\t70\t2S39M\t*\t188710074\t90\tNNGAATAATAATAGTGGGGGATCCACTAGGGCTGTGGCTCC\t~~IIIIIIIIIIIIIIIIIIIIIIIIIIHHHIIIIIIIIII\txl:i:188710023\txr:i:188710061\txs:i:39\txd:A:f\txm:A:u\txa:A:\"\"\t\txL:i:188710023\txR:i:188710112\txS:i:90\txW:i:12\txP:i:0\txQ:i:0\txC:A:\"\"\txD:A:\"\"\tPG:Z:novoalign\tAS:i:12\tUQ:i:12\tNM:i:0\tMD:Z:39\tPQ:i:20\tSM:i:70\tAM:i:70\tZS:Z:QC");
  private static  XsamRecord nmRecord2 = new XsamRecord("SBL_XSNP077_ID:184296_BC1:CTATTTCAG_\t147\t*\t188710074\t70\t39M\tmm01\t188710023\t-90\tTCCTTGGTCAGCTGTGTTTTAACTTGAGGAGGCCTTCCC\tIHIIIIIIIIIIIIIIIIIHIHIIIIIIIIIIIIIIII~\txl:i:188710074\txr:i:188710112\txs:i:39\txd:A:r\txm:A:u\txa:A:\"\"\t\txL:i:188710023\txR:i:188710112\txS:i:90\txW:i:12\txP:i:0\txQ:i:0\txC:A:\"\"\txD:A:\"\"\tPG:Z:novoalign\tAS:i:6\tUQ:i:6\tNM:i:1\tMD:Z:38A0\tPQ:i:20\tSM:i:70\tAM:i:70\tZS:Z:NM");


  private static XsamRecord record1SoftEnding = new XsamRecord("SBL_XSNP077_ID:184296_BC1:CTATTTCAG_\t99\tmm01\t188710023\t70\t39M2S\t=\t188710074\t90\tGAATAATAATAGTGGGGGATCCACTAGGGCTGTGGCTCCNN\t~~IIIIIIIIIIIIIIIIIIIIIIIIIIHHHIIIIIIIIII\txl:i:188710023\txr:i:188710061\txs:i:39\txd:A:f\txm:A:u\txa:A:\"\"\txL:i:188710023\txR:i:188710112\txS:i:90\txW:i:12\txP:i:0\txQ:i:0\txC:A:\"\"\txD:A:\"\"\tPG:Z:novoalign\tAS:i:12\tUQ:i:12\tNM:i:0\tMD:Z:39\tPQ:i:20\tSM:i:70\tAM:i:70");
  private static XsamRecord record2SoftEnding = new XsamRecord("SBL_XSNP077_ID:184296_BC1:CTATTTCAG_\t147\tmm01\t188710074\t70\t38M1S\t=\t188710023\t-90\tTCCTTGGTCAGCTGTGTTTTAACTTGAGGAGGCCTTCCN\tIHIIIIIIIIIIIIIIIIIHIHIIIIIIIIIIIIIIII~\txl:i:188710074\txr:i:188710112\txs:i:39\txd:A:r\txm:A:u\txa:A:\"\"txL:i:188710023\txR:i:188710112\txS:i:90\txW:i:12\txP:i:0\txQ:i:0\txC:A:\"\"\txD:A:\"\"\tPG:Z:novoalign\tAS:i:6\tUQ:i:6\tNM:i:1\tMD:Z:38A0\tPQ:i:20\tSM:i:70\tAM:i:70");


  static Stream<List<XsamRecord>> listOfRecords(){
    return Stream.of(Arrays.asList(record1, record2, dRecord1, dRecord2, qcRecord1, nmRecord2, record1SoftEnding, record2SoftEnding));
  }

  static Stream<List<XsamRecord>> randomLongListOfRecords(){

    List<XsamRecord> records = new ArrayList<>();

    Random r = new Random();

    String[] rnames = new String[]{"mm01", "mm02", "mm03", "mm04"};

    XsamConverterFactory f = new XsamConverterFactory();
    XsamConverter converter = f.getXsamConverter(1);

    for(int i = 0; i < 10000; i++){
      StringJoiner sb = new StringJoiner("\t");
      sb.add(String.format("SBL_XSNP077_ID:%s", r.nextInt()));
      sb.add(String.valueOf(r.nextInt(4095)));
      sb.add(rnames[r.nextInt(3)]);
      int pos = r.nextInt();
      if(pos < 0){
        pos = Math.abs(pos);
      }
      sb.add(String.valueOf(pos));
      sb.add("70");
      sb.add("39M");
      sb.add("=");
      sb.add("pos");
      sb.add("90");
      sb.add("NGCTGTCTGTGTGTATATATCTCACACATGT");
      sb.add("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
      sb.add("PG:Novoalign");
      sb.add("AS:i:12");

      XsamRecord record = converter.convertSingleRecord(new SamRecord(sb.toString()));
      records.add(record);
    }

    return Stream.of(records);

  }

  static Stream<XsamRecord> softEndingStream(){
    return Stream.of(record1SoftEnding, record2SoftEnding);
  }

  static Stream<XsamRecord> positionedRecordStream(){
    return Stream.of(record1, record2, dRecord1, dRecord2, qcRecord1);
  }

  static Stream<XsamRecord> nonMatchRecordStream(){
    return Stream.of(nmRecord2);
  }
}
