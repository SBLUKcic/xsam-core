package com.sbl.providers;

import com.sbl.conversion.XsamConverter;
import com.sbl.conversion.XsamConverterFactory;
import com.sbl.conversion.model.XsamReturn;
import com.sbl.model.PairedSamRecord;
import com.sbl.model.PairedXsamRecord;
import com.sbl.model.SamRecord;
import com.sbl.model.XsamRecord;

import java.util.*;
import java.util.stream.Stream;

public class XsamPairProviders {

  private static XsamRecord record1 = new XsamRecord("SBL_XSNP077_ID:184296_BC1:CTATTTCAG_\t99\tmm01\t188710023\t70\t2S39M\t=\t188710074\t90\tNNGAATAATAATAGTGGGGGATCCACTAGGGCTGTGGCTCC\t~~IIIIIIIIIIIIIIIIIIIIIIIIIIHHHIIIIIIIIII\txl:i:188710023\txr:i:188710061\txs:i:39\txd:A:f\txm:A:u\txa:A:\"\"\txL:i:188710023\txR:i:188710112\txS:i:90\txW:i:12\txP:i:0\txQ:i:0\txC:A:\"\"\txD:A:\"\"\tPG:Z:novoalign\tAS:i:12\tUQ:i:12\tNM:i:0\tMD:Z:39\tPQ:i:20\tSM:i:70\tAM:i:70");
  private static XsamRecord record2 = new XsamRecord("SBL_XSNP077_ID:184296_BC1:CTATTTCAG_\t147\tmm01\t188710074\t70\t39M\t=\t188710023\t-90\tTCCTTGGTCAGCTGTGTTTTAACTTGAGGAGGCCTTCCN\tIHIIIIIIIIIIIIIIIIIHIHIIIIIIIIIIIIIIII~\txl:i:188710074\txr:i:188710112\txs:i:39\txd:A:r\txm:A:u\txa:A:\"\"txL:i:188710023\txR:i:188710112\txS:i:90\txW:i:12\txP:i:0\txQ:i:0\txC:A:\"\"\txD:A:\"\"\tPG:Z:novoalign\tAS:i:6\tUQ:i:6\tNM:i:1\tMD:Z:38A0\tPQ:i:20\tSM:i:70\tAM:i:70");

  private static XsamRecord record3 = new XsamRecord("SBL_XSMJR320b_ID:2293779\t65\tmm01\t3014444\t70\t2S39M\t=\t3014575\t170\tNNATTGTTCCTTTAAAAATTTTCTTTTGACCCCTTCTGTTC\t~~EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEAEE<EEEEE\txl:i:3014444\txr:i:3014482\txs:i:39\txd:A:f\txm:A:u\txa:A:\"\"\txL:i:3014444\txR:i:3014613\txS:i:170\txW:i:92\txP:i:0\txQ:i:0\txC:A:\"\"\txD:A:\"\"\tPG:Z:novoalign\tAS:i:12\tUQ:i:12\tNM:i:0\tMD:Z:39");
  private static XsamRecord record4 = new XsamRecord("SBL_XSMJR320b_ID:2293779\t129\tmm01\t3014575\t57\t39M\t=\t3014444\t-170\tGAACACAACTTCTGTTCCAATCTAATCGTGCAAGACCGN\tEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE~\txl:i:3014575\txr:i:3014613\txs:i:39\txd:A:f\txm:A:u\txa:A:\"\"\txL:i:3014444\txR:i:3014613\txS:i:170\txW:i:92\txP:i:0\txQ:i:0\txC:A:\"\"\txD:A:\"\"\tPG:Z:novoalign\tAS:i:6\tUQ:i:6\tNM:i:1\tMD:Z:38G0");

  private static XsamRecord record5 = new XsamRecord("SBL_XSMJR320c_ID:2293778\t65\tmm01\t3014444\t70\t2S39M\t=\t3014575\t170\tNNATTGTTCCTTTAAAAATTTTCTTTTGACCCCTTCTGTTC\t~~EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEAEE<EEEEE\txl:i:3014444\txr:i:3014482\txs:i:39\txd:A:f\txm:A:u\txa:A:\"\"\txL:i:3014444\txR:i:3014613\txS:i:170\txW:i:92\txP:i:0\txQ:i:0\txC:A:\"\"\txD:A:\"\"\tPG:Z:novoalign\tAS:i:12\tUQ:i:12\tNM:i:0\tMD:Z:39");
  private static XsamRecord record6 = new XsamRecord("SBL_XSMJR320c_ID:2293778\t129\tmm01\t3014575\t57\t39M\t=\t3014444\t-170\tGAACACAACTTCTGTTCCAATCTAATCGTGCAAGACCGN\tEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE~\txl:i:3014575\txr:i:3014613\txs:i:39\txd:A:f\txm:A:u\txa:A:\"\"\txL:i:3014444\txR:i:3014613\txS:i:170\txW:i:92\txP:i:0\txQ:i:0\txC:A:\"\"\txD:A:\"\"\tPG:Z:novoalign\tAS:i:6\tUQ:i:6\tNM:i:1\tMD:Z:38G0");

  private static XsamRecord record7 = new XsamRecord("SBL_XSMJR320d_ID:2293770\t65\tmm01\t3014444\t70\t2S39M\t=\t3014575\t170\tNNATTGTTCCTTTAAAAATTTTCTTTTGACCCCTTCTGTTC\t~~EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEAEE<EEEEE\txl:i:3014444\txr:i:3014482\txs:i:39\txd:A:f\txm:A:u\txa:A:\"\"\txL:i:3014444\txR:i:3014615\txS:i:170\txW:i:92\txP:i:0\txQ:i:0\txC:A:\"\"\txD:A:\"\"\tPG:Z:novoalign\tAS:i:12\tUQ:i:12\tNM:i:0\tMD:Z:39");
  private static XsamRecord record8 = new XsamRecord("SBL_XSMJR320d_ID:2293770\t129\tmm01\t3014575\t57\t41M\t=\t3014444\t-170\tGAACACAACTTCTGTTCCAATCTAATCGTGCAAGACCGTAN\tEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE~\txl:i:3014575\txr:i:3014615\txs:i:39\txd:A:f\txm:A:u\txa:A:\"\"\txL:i:3014444\txR:i:3014615\txS:i:170\txW:i:92\txP:i:0\txQ:i:0\txC:A:\"\"\txD:A:\"\"\tPG:Z:novoalign\tAS:i:6\tUQ:i:6\tNM:i:1\tMD:Z:38G0");


  private static PairedXsamRecord pair = new PairedXsamRecord(record1, record2);
  private static PairedXsamRecord pair2 = new PairedXsamRecord(record3, record4);
  private static PairedXsamRecord pair3 = new PairedXsamRecord(record5, record6);
  private static PairedXsamRecord pair4 = new PairedXsamRecord(record7, record8);

  private static XsamRecord dRecord1 = new XsamRecord("SBL_XSNP077_ID:184296_BC1:CTATTTCAG_\t99\tmm02\t188710023\t70\t2S39M\tmm01\t188710074\t90\tNNGAATAATAATAGTGGGGGATCCACTAGGGCTGTGGCTCC\t~~IIIIIIIIIIIIIIIIIIIIIIIIIIHHHIIIIIIIIII\txl:i:188710023\txr:i:188710061\txs:i:39\txd:A:f\txm:A:u\txa:A:\"\"\txL:i:188710023\txR:i:188710112\txS:i:90\txW:i:12\txP:i:0\txQ:i:0\txC:A:\"\"\txD:A:\"\"\tPG:Z:novoalign\tAS:i:12\tUQ:i:12\tNM:i:0\tMD:Z:39\tPQ:i:20\tSM:i:70\tAM:i:70");
  private static XsamRecord dRecord2 = new XsamRecord("SBL_XSNP077_ID:184296_BC1:CTATTTCAG_\t147\tmm01\t188710074\t70\t39M\tmm02\t188710023\t-90\tTCCTTGGTCAGCTGTGTTTTAACTTGAGGAGGCCTTCCN\tIHIIIIIIIIIIIIIIIIIHIHIIIIIIIIIIIIIIII~\txl:i:188710074\txr:i:188710112\txs:i:39\txd:A:r\txm:A:u\txa:A:\"\"\txL:i:188710023\txR:i:188710112\txS:i:90\txW:i:12\txP:i:0\txQ:i:0\txC:A:\"\"\txD:A:\"\"\tPG:Z:novoalign\tAS:i:6\tUQ:i:6\tNM:i:1\tMD:Z:38A0\tPQ:i:20\tSM:i:70\tAM:i:70");

  private static PairedXsamRecord dPair = new PairedXsamRecord(dRecord1, dRecord2);

  private static  XsamRecord qcRecord1 = new XsamRecord("SBL_XSNP077_ID:184296_BC1:CTATTTCAG_\t99\tmm02\t188710023\t70\t2S39M\t*\t188710074\t90\tNNGAATAATAATAGTGGGGGATCCACTAGGGCTGTGGCTCC\t~~IIIIIIIIIIIIIIIIIIIIIIIIIIHHHIIIIIIIIII\txl:i:188710023\txr:i:188710061\txs:i:39\txd:A:f\txm:A:u\txa:A:\"\"\txL:i:188710023\txR:i:188710112\txS:i:90\txW:i:12\txP:i:0\txQ:i:0\txC:A:\"\"\txD:A:\"\"\tPG:Z:novoalign\tAS:i:12\tUQ:i:12\tNM:i:0\tMD:Z:39\tPQ:i:20\tSM:i:70\tAM:i:70\tZS:Z:QC");
  private static  XsamRecord nmRecord2 = new XsamRecord("SBL_XSNP077_ID:184296_BC1:CTATTTCAG_\t147\t*\t188710074\t70\t39M\tmm01\t188710023\t-90\tTCCTTGGTCAGCTGTGTTTTAACTTGAGGAGGCCTTCCN\tIHIIIIIIIIIIIIIIIIIHIHIIIIIIIIIIIIIIII~\txl:i:188710074\txr:i:188710112\txs:i:39\txd:A:r\txm:A:u\txa:A:\"\"\txL:i:188710023\txR:i:188710112\txS:i:90\txW:i:12\txP:i:0\txQ:i:0\txC:A:\"\"\txD:A:\"\"\tPG:Z:novoalign\tAS:i:6\tUQ:i:6\tNM:i:1\tMD:Z:38A0\tPQ:i:20\tSM:i:70\tAM:i:70\tZS:Z:NM");

  private static PairedXsamRecord xxPair = new PairedXsamRecord(qcRecord1, nmRecord2);

  private static XsamRecord record1WithFlankext = new XsamRecord("SBL_XSNP077_ID:184296_BC1:CTATTTCAG_\t99\tmm01\t188710023\t70\t2S39M\t=\t188710074\t90\tNNGAATAATAATAGTGGGGGATCCACTAGGGCTGTGGCTCC\t~~IIIIIIIIIIIIIIIIIIIIIIIIIIHHHIIIIIIIIII\txl:i:188710023\txr:i:188710061\txs:i:39\txd:A:f\txm:A:u\txa:A:\"\"\txLseq:i:ATCG\txRseq:i:ATTT\txL:i:188710023\txR:i:188710112\txS:i:90\txW:i:12\txP:i:0\txQ:i:0\txC:A:\"\"\txD:A:\"\"\tPG:Z:novoalign\tAS:i:12\tUQ:i:12\tNM:i:0\tMD:Z:39\tPQ:i:20\tSM:i:70\tAM:i:70");
  private static XsamRecord record2WithFlankext = new XsamRecord("SBL_XSNP077_ID:184296_BC1:CTATTTCAG_\t147\tmm01\t188710074\t70\t39M\t=\t188710023\t-90\tTCCTTGGTCAGCTGTGTTTTAACTTGAGGAGGCCTTCCN\tIHIIIIIIIIIIIIIIIIIHIHIIIIIIIIIIIIIIII~\txl:i:188710074\txr:i:188710112\txs:i:39\txd:A:r\txm:A:u\txa:A:\"\"\txLseq:i:ATCG\txRseq:i:ATTT\txL:i:188710023\txR:i:188710112\txS:i:90\txW:i:12\txP:i:0\txQ:i:0\txC:A:\"\"\txD:A:\"\"\tPG:Z:novoalign\tAS:i:6\tUQ:i:6\tNM:i:1\tMD:Z:38A0\tPQ:i:20\tSM:i:70\tAM:i:70");

  private static PairedXsamRecord pairWithFlankext = new PairedXsamRecord(record1WithFlankext, record2WithFlankext);

  private static XsamRecord record1WithFlankextR = new XsamRecord("SBL_XSNP077_ID:184296_BC1:CTATTTCAG_\t99\tmm01\t188710023\t70\t2S39M\t=\t188710074\t90\tNNGAATAATAATAGTGGGGGATCCACTAGGGCTGTGGCTCC\t~~IIIIIIIIIIIIIIIIIIIIIIIIIIHHHIIIIIIIIII\txl:i:188710023\txr:i:188710061\txs:i:39\txd:A:r\txm:A:u\txa:A:\"\"\txLseq:i:ATCG\txRseq:i:ATTT\txL:i:188710023\txR:i:188710112\txS:i:90\txW:i:12\txP:i:0\txQ:i:0\txC:A:\"\"\txD:A:\"\"\tPG:Z:novoalign\tAS:i:12\tUQ:i:12\tNM:i:0\tMD:Z:39\tPQ:i:20\tSM:i:70\tAM:i:70");
  private static XsamRecord record2WithFlankextR = new XsamRecord("SBL_XSNP077_ID:184296_BC1:CTATTTCAG_\t147\tmm01\t188710074\t70\t39M\t=\t188710023\t-90\tTCCTTGGTCAGCTGTGTTTTAACTTGAGGAGGCCTTCCN\tIHIIIIIIIIIIIIIIIIIHIHIIIIIIIIIIIIIIII~\txl:i:188710074\txr:i:188710112\txs:i:39\txd:A:r\txm:A:u\txa:A:\"\"\txLseq:i:ATCG\txRseq:i:ATTT\txL:i:188710023\txR:i:188710112\txS:i:90\txW:i:12\txP:i:0\txQ:i:0\txC:A:\"\"\txD:A:\"\"\tPG:Z:novoalign\tAS:i:6\tUQ:i:6\tNM:i:1\tMD:Z:38A0\tPQ:i:20\tSM:i:70\tAM:i:70");

  private static PairedXsamRecord pairWithFlankextR = new PairedXsamRecord(record1WithFlankextR, record2WithFlankextR);

  private static XsamRecord record1SoftEnding = new XsamRecord("SBL_XSNP077_ID:184296_BC1:CTATTTCAG_\t99\tmm01\t188710023\t70\t39M2S\t=\t188710074\t90\tGAATAATAATAGTGGGGGATCCACTAGGGCTGTGGCTCCNN\t~~IIIIIIIIIIIIIIIIIIIIIIIIIIHHHIIIIIIIIII\txl:i:188710023\txr:i:188710061\txs:i:39\txd:A:f\txm:A:u\txa:A:\"\"\txL:i:188710023\txR:i:188710112\txS:i:90\txW:i:12\txP:i:0\txQ:i:0\txC:A:\"\"\txD:A:\"\"\tPG:Z:novoalign\tAS:i:12\tUQ:i:12\tNM:i:0\tMD:Z:39\tPQ:i:20\tSM:i:70\tAM:i:70");
  private static XsamRecord record2SoftEnding = new XsamRecord("SBL_XSNP077_ID:184296_BC1:CTATTTCAG_\t147\tmm01\t188710074\t70\t38M1S\t=\t188710023\t-90\tTCCTTGGTCAGCTGTGTTTTAACTTGAGGAGGCCTTCCN\tIHIIIIIIIIIIIIIIIIIHIHIIIIIIIIIIIIIIII~\txl:i:188710074\txr:i:188710112\txs:i:39\txd:A:r\txm:A:u\txa:A:\"\"txL:i:188710023\txR:i:188710112\txS:i:90\txW:i:12\txP:i:0\txQ:i:0\txC:A:\"\"\txD:A:\"\"\tPG:Z:novoalign\tAS:i:6\tUQ:i:6\tNM:i:1\tMD:Z:38A0\tPQ:i:20\tSM:i:70\tAM:i:70");

  private static PairedXsamRecord pairSoftEnding = new PairedXsamRecord(record1SoftEnding, record2SoftEnding);


  static Stream<List<PairedXsamRecord>> pairedXsamRecordProvider(){


    List<PairedXsamRecord> records = new ArrayList<>();

    Random r = new Random();

    String[] rnames = new String[]{"mm01", "mm02", "mm03", "mm04"};

    XsamConverterFactory f = new XsamConverterFactory();
    XsamConverter converter = f.getXsamConverter(1);

    for(int i = 0; i < 10000; i++){
      StringJoiner sb = new StringJoiner("\t");
      StringJoiner sb2 = new StringJoiner("\t");
      String ID = String.format("SBL_XSNP077_ID:%s", r.nextInt());

      sb.add(ID);
      sb2.add(ID);

      sb.add(String.valueOf(r.nextInt(4095)));
      sb2.add(String.valueOf(r.nextInt(4095)));

      String rname = rnames[r.nextInt(3)];
      sb.add(rname);
      sb2.add(rname);

      int pos = r.nextInt( 20000000);
      if(pos < 0){
        pos = Math.abs(pos);
      }
      sb.add(String.valueOf(pos));

      int spanLength = r.nextInt(10000000);
      if(spanLength < 0){
        spanLength = Math.abs(spanLength);
      }

      sb2.add(String.valueOf(pos+spanLength));

      sb.add("70");
      sb.add("39M");
      sb.add("=");
      sb.add("pos");
      sb.add("90");
      sb.add("NGCTGTCTGTGTGTATATATCTCACACATGT");
      sb.add("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
      sb.add("PG:Novoalign");
      sb.add("AS:i:12");

      sb2.add("70");
      sb2.add("39M");
      sb2.add("=");
      sb2.add("pos");
      sb2.add("90");
      sb2.add("NGCTGTCTGTGTGTATATATCTCACACATGT");
      sb2.add("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
      sb2.add("PG:Novoalign");
      sb2.add("AS:i:12");




      try {

        XsamReturn record = converter.convertRecordPair(new PairedSamRecord(new SamRecord(sb.toString()), new SamRecord(sb2.toString())));
        records.add(record.getPair());


        if (record.getOptionalPair().isPresent()) {
          records.add(record.getOptionalPair().get());
        }

      }catch (ArrayIndexOutOfBoundsException ae){
        System.out.println(ae.getMessage());
      }
    }

    return Stream.of(records);
  }

  static Stream<PairedXsamRecord> softEndingRecordPairProvider() { return  Stream.of(pairSoftEnding); }

  static Stream<PairedXsamRecord> flankExtPairProvider() { return Stream.of(pairWithFlankext); }

  static Stream<PairedXsamRecord> flankExtPairProviderR() { return Stream.of(pairWithFlankextR); }

  static Stream<PairedXsamRecord> ppePairProvider(){
    return Stream.of(pair, pair2);
  }

  static Stream<List<PairedXsamRecord>> ppeListPairProvider(){ return Stream.of(Arrays.asList(pair, pair2, pair3, pair4)); }

  static Stream<PairedXsamRecord> ppdPairProvider() {
    return Stream.of(dPair);
  }

  static Stream<PairedXsamRecord> xxPairProvider(){
    return Stream.of(xxPair);
  }

  static Stream<PairedXsamRecord> gimmeOnOfEach(){
    return Stream.of(pair, dPair, xxPair);
  }
}
