package com.sbl.providers;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public final class SampleXsamFileCreator {

  public static File writeSmolXsamFile(File destination){
    if(destination == null) return null;
    if(!destination.exists()){
      try{
        destination.createNewFile();
      }catch (IOException ioe){
        return null;
      }
    }

    try(PrintWriter pw = new PrintWriter(new FileWriter(destination))){

      pw.println("@HD\tVN:1.0\tSO:unsorted\n" +
          "@PG\tID:novoalign_1\tPN:novoalign\tVN:V3.08.01\tCL:novoalign -d \"//mnt/htsd/reference sequences/indexed reference sequences/novoindex/current references/mm_SBL_v6.nix\" -f XSMJR314b_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.1.fastq XSMJR314b_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.2.fastq -F STDFQ -r random -o sam\n" +
          "@PG\tID:novoalign_2\tPN:novoalign\tVN:V3.08.01\tCL:novoalign -d \"//mnt/htsd/reference sequences/indexed reference sequences/novoindex/current references/mm_SBL_v6.nix\" -f XSMJR316a_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.1.fastq XSMJR316a_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.2.fastq -F STDFQ -r random -o sam\n" +
          "@PG\tID:novoalign_3\tPN:novoalign\tVN:V3.08.01\tCL:novoalign -d \"//mnt/htsd/reference sequences/indexed reference sequences/novoindex/current references/mm_SBL_v6.nix\" -f XSMJR272_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.1.fastq XSMJR272_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.2.fastq -F STDFQ -r random -o sam\n" +
          "@PG\tID:novoalign_4\tPN:novoalign\tVN:V3.08.01\tCL:novoalign -d \"//mnt/htsd/reference sequences/indexed reference sequences/novoindex/current references/mm_SBL_v6.nix\" -f XSMJR280_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.1.fastq XSMJR280_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.2.fastq -F STDFQ -r random -o sam\n" +
          "@PG\tID:novoalign_5\tPN:novoalign\tVN:V3.08.01\tCL:novoalign -d \"//mnt/htsd/reference sequences/indexed reference sequences/novoindex/current references/mm_SBL_v6.nix\" -f XSMJR315a_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.1.fastq XSMJR315a_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.2.fastq -F STDFQ -r random -o sam\n" +
          "@PG\tID:novoalign_6\tPN:novoalign\tVN:V3.08.01\tCL:novoalign -d \"//mnt/htsd/reference sequences/indexed reference sequences/novoindex/current references/mm_SBL_v6.nix\" -f XSMJR329_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.1.fastq XSMJR329_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.2.fastq -F STDFQ -r random -o sam\n" +
          "@PG\tID:novoalign_7\tPN:novoalign\tVN:V3.08.01\tCL:novoalign -d \"//mnt/htsd/reference sequences/indexed reference sequences/novoindex/current references/mm_SBL_v6.nix\" -f XSMJR316b_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.1.fastq XSMJR316b_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.2.fastq -F STDFQ -r random -o sam\n" +
          "@PG\tID:novoalign_8\tPN:novoalign\tVN:V3.08.01\tCL:novoalign -d \"//mnt/htsd/reference sequences/indexed reference sequences/novoindex/current references/mm_SBL_v6.nix\" -f XSMJR276_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.1.fastq XSMJR276_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.2.fastq -F STDFQ -r random -o sam\n" +
          "@PG\tID:novoalign_9\tPN:novoalign\tVN:V3.08.01\tCL:novoalign -d \"//mnt/htsd/reference sequences/indexed reference sequences/novoindex/current references/mm_SBL_v6.nix\" -f XSMJR271_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.1.fastq XSMJR271_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.2.fastq -F STDFQ -r random -o sam\n" +
          "@PG\tID:novoalign_10\tPN:novoalign\tVN:V3.08.01\tCL:novoalign -d \"//mnt/htsd/reference sequences/indexed reference sequences/novoindex/current references/mm_SBL_v6.nix\" -f XSMJR315b_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.1.fastq XSMJR315b_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.2.fastq -F STDFQ -r random -o sam\n" +
          "@PG\tID:novoalign_11\tPN:novoalign\tVN:V3.08.01\tCL:novoalign -d \"//mnt/htsd/reference sequences/indexed reference sequences/novoindex/current references/mm_SBL_v6.nix\" -f XSMJR314a_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.1.fastq XSMJR314a_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.2.fastq -F STDFQ -r random -o sam\n" +
          "@PG\tID:novoalign_12\tPN:novoalign\tVN:V3.08.01\tCL:novoalign -d \"//mnt/htsd/reference sequences/indexed reference sequences/novoindex/current references/mm_SBL_v6.nix\" -f XSMJR279_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.1.fastq XSMJR279_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.2.fastq -F STDFQ -r random -o sam\n" +
          "@PG\tID:novoalign_13\tPN:novoalign\tVN:V3.08.01\tCL:novoalign -d \"//mnt/htsd/reference sequences/indexed reference sequences/novoindex/current references/mm_SBL_v6.nix\" -f XSMJR275_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.1.fastq XSMJR275_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.2.fastq -F STDFQ -r random -o sam\n" +
          "@PG\tID:novoalign_14\tPN:novoalign\tVN:V3.08.01\tCL:novoalign -d \"//mnt/htsd/reference sequences/indexed reference sequences/novoindex/current references/mm_SBL_v6.nix\" -f XSMJR267_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.1.fastq XSMJR267_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.2.fastq -F STDFQ -r random -o sam\n" +
          "@PG\tID:novoalign_15\tPN:novoalign\tVN:V3.08.01\tCL:novoalign -d \"//mnt/htsd/reference sequences/indexed reference sequences/novoindex/current references/mm_SBL_v6.nix\" -f XSMJR268_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.1.fastq XSMJR268_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.2.fastq -F STDFQ -r random -o sam\n" +
          "@SQ\tSN:mm10\tLN:130694993\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:mm11\tLN:122082543\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:mm12\tLN:120129022\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:mm13\tLN:120421639\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:mm14\tLN:124902244\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:mm15\tLN:104043685\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:mm16\tLN:98207768\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:mm17\tLN:94987271\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:mm18\tLN:90702639\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:mm19\tLN:61431566\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:mm01\tLN:195471971\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:mm02\tLN:182113224\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:mm03\tLN:160039680\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:mm04\tLN:156508116\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:mm05\tLN:151834684\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:mm06\tLN:149736546\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:mm07\tLN:145441459\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:mm08\tLN:129401213\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:mm09\tLN:124595110\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:mmMT\tLN:16299\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:mmX\tLN:171031299\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:mmY\tLN:91744698\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:JH584299.1\tLN:953012\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456233.1\tLN:336933\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:JH584301.1\tLN:259875\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456211.1\tLN:241735\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456350.1\tLN:227966\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:JH584293.1\tLN:207968\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456221.1\tLN:206961\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:JH584297.1\tLN:205776\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:JH584296.1\tLN:199368\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456354.1\tLN:195993\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:JH584294.1\tLN:191905\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:JH584298.1\tLN:184189\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:JH584300.1\tLN:182347\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456219.1\tLN:175968\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456210.1\tLN:169725\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:JH584303.1\tLN:158099\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:JH584302.1\tLN:155838\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456212.1\tLN:153618\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:JH584304.1\tLN:114452\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456379.1\tLN:72385\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456216.1\tLN:66673\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456393.1\tLN:55711\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456366.1\tLN:47073\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456367.1\tLN:42057\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456239.1\tLN:40056\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456213.1\tLN:39340\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456383.1\tLN:38659\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456385.1\tLN:35240\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456360.1\tLN:31704\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456378.1\tLN:31602\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456389.1\tLN:28772\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456372.1\tLN:28664\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456370.1\tLN:26764\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456381.1\tLN:25871\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456387.1\tLN:24685\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456390.1\tLN:24668\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456394.1\tLN:24323\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456392.1\tLN:23629\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456382.1\tLN:23158\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456359.1\tLN:22974\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456396.1\tLN:21240\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456368.1\tLN:20208\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:JH584292.1\tLN:14945\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:JH584295.1\tLN:1976\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:SBL_Con_5B3_minilocus\tLN:18131\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:SBL_Con_A_baumannii_Transposase\tLN:3936291\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:SBL_Con_Acidovorax\tLN:4448856\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:SBL_Con_Ecoli_MG1655\tLN:4639675\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:SBL_Con_E\tLN:3190\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:SBL_Con_eGFP_cds\tLN:720\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:SBL_Con_hLine1\tLN:6050\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:SBL_Con_hmGFP\tLN:4707\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:SBL_Con_Illumina_Adapter_PCR1\tLN:58\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:SBL_Con_Illumina_Adapter_PCR2\tLN:61\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:SBL_Con_MG1655_masked_LacZ\tLN:4639675\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:SBL_Con_pBR322\tLN:4361\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:SBL_Con_pGEM\tLN:3221\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:PGM/Tag_P1_Adapter_PE_1\tLN:50\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:PGM_A_Adapter_1\tLN:32\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:SBL_Con_Phage_Lambda\tLN:48502\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:SBL_Con_Phagemid_f1\tLN:6407\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:SBL_Con_PhiX\tLN:5386\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:SBL_Con_Plasmid_BRA100\tLN:56265\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:SBL_Con_Plasmid_pBVIE02\tLN:265616\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:SBL_Con_Plasmid_pTINT01\tLN:45943\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:SBL_Con_Plasmid_SYK-6\tLN:148801\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:SBL_Con_Propionbacterium.acnes\tLN:2519002\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:SBL_Con_T4_PNK\tLN:906\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:SBL_Con_T4_Pol_cds\tLN:2697\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:SBL_Con_Topo_PCR2.1\tLN:3929\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@CO\tXsam:F:Generated at 2018/12/04 13:31:05\n" +
          "@CO\tXsam:F:Generated by Xsam version 2.061\n" +
          "@CO\tXsam:F:type is Paired-End\n" +
          "@CO\tXsam:S:P1:2000\n" +
          "@CO\tXsam:S:P2:0\n" +
          "@CO\tXsam:S:P3:0\n" +
          "@CO\tXsam:P:mm01:1:6:4479:0\n" +
          "@CO\tXsam:P:mm01:2:0:0:4479\n" +
          "@CO\tXsam:P:mm01:3:0:0:4479\n" +
          "@CO\tXsam:P:mm01:4:0:0:4479\n" +
          "SBL_XSMJR267_ID:1884132_BC1:CGCAG_\t81\tmm01\t24684050\t30\t39M\t=\t24684188\t175\tTGGTGGTGGTTATAGACCCCAGGTAGTGGTGGTTACAGN\tIIIIIIIIIIIIIIIHIIIIIIIIIIIIIIIIIIIIII~\txl:i:24684050\txr:i:24684088\txs:i:39\txd:A:r\txm:A:u\txa:A:\"\"\t\txL:i:24684050\txR:i:24684224\txLseq:i:CAGG\txRseq:i:GACA\txS:i:175\txW:i:99\txP:i:0\txQ:i:0\txC:A:\"\"\txD:A:\"\"\tPG:Z:novoalign\tAS:i:6\tUQ:i:6\tNM:i:1\tMD:Z:38A0\t\n" +
          "SBL_XSMJR267_ID:1884132_BC1:CGCAG_\t161\tmm01\t24684188\t29\t2S37M2S\t=\t24684050\t-175\tNNCAGGTAGTGGTGGTTACAGACCTCAGGTAGTGGTGGTTA\t~~IIHIHIIIIIIIIIIIIIIIIIIIIIHGHIIIIIIIIII\txl:i:24684188\txr:i:24684224\txs:i:37\txd:A:f\txm:A:u\txa:A:\"\"\t\txL:i:24684050\txR:i:24684224\txLseq:i:CAGG\txRseq:i:GACA\txS:i:175\txW:i:99\txP:i:0\txQ:i:0\txC:A:\"\"\txD:A:\"\"\tPG:Z:novoalign\tAS:i:42\tUQ:i:42\tNM:i:0\tMD:Z:37\t\n" +
          "SBL_XSMJR271_ID:2569046_BC1:TGCAG_\t83\tmm01\t24684186\t70\t39M\t=\t24684188\t39\tCTCAGGTAGTGGTGGTTACAGACCTCAGGTAGTGGTGGN\tIIGIIIIIIIIIIIIIIIIIIIIIIIIIIIIHIIIIII~\txl:i:24684186\txr:i:24684224\txs:i:39\txd:A:r\txm:A:u\txa:A:\"\"\t\txL:i:24684186\txR:i:24684224\txLseq:i:AGAC\txRseq:i:GACA\txS:i:39\txW:i:-37\txP:i:0\txQ:i:0\txC:A:\"\"\txD:A:\"\"\tPG:Z:novoalign\tAS:i:6\tUQ:i:6\tNM:i:1\tMD:Z:38T0\tPQ:i:78\tSM:i:70\tAM:i:24\t\n" +
          "SBL_XSMJR271_ID:2569046_BC1:TGCAG_\t163\tmm01\t24684188\t28\t2S37M2S\t=\t24684186\t-39\tNNCAGGTAGTGGTGGTTACAGACCTCAGGTAGTGGTGGTTA\t~~IIIIIIIIIIIIIIIHIIIIIIIIHIGHIIHIIIIIEHF\txl:i:24684188\txr:i:24684224\txs:i:37\txd:A:f\txm:A:u\txa:A:\"\"\t\txL:i:24684186\txR:i:24684224\txLseq:i:AGAC\txRseq:i:GACA\txS:i:39\txW:i:-37\txP:i:0\txQ:i:0\txC:A:\"\"\txD:A:\"\"\tPG:Z:novoalign\tAS:i:42\tUQ:i:42\tNM:i:0\tMD:Z:37\tPQ:i:78\tSM:i:24\tAM:i:24\t\n" +
          "SBL_XSMJR314a_ID:mb34vlhd\t81\tmm01\t69021869\t70\t41M\t=\t69021956\t125\tTACTTCACATCCAAAAGACAAATGAACAAAATAAAAAAANN\tGGGGGIGIGIIIIIIGGIIIIIIIIIIIIIIIIIIIIII~~\txl:i:69021869\txr:i:69021909\txs:i:41\txd:A:r\txm:A:u\txa:A:\"\"\txL:i:69021869\txR:i:69021993\txLseq:i:TCAA\txRseq:i:CACT\txS:i:125\txW:i:46\txP:i:0\txQ:i:0\txC:A:\"\"\txD:A:\"\"\tPG:Z:novoalign\tAS:i:12\tUQ:i:12\tNM:i:2\tMD:Z:39A0G0\n" +
          "SBL_XSMJR314a_ID:mb34vlhd\t161\tmm01\t69021956\t70\t1S38M\t=\t69021869\t-125\tNAAAGACATTTCCAAATACAAATGACTTAACACTGTAAA\t~GGGGGGGGGGGGIGAGGGGGGGGIGGGIIIGGAGGGGG\txl:i:69021956\txr:i:69021993\txs:i:38\txd:A:f\txm:A:u\txa:A:\"\"\txL:i:69021869\txR:i:69021993\txLseq:i:TCAA\txRseq:i:CACT\txS:i:125\txW:i:46\txP:i:0\txQ:i:0\txC:A:\"\"\txD:A:\"\"\tPG:Z:novoalign\tAS:i:6\tUQ:i:6\tNM:i:0\tMD:Z:38\n" +
          "SBL_XSMJR268_ID:2363106_BC1:CTCAG_\t81\tmm01\t90846371\t70\t39M\t=\t90847662\t1320\tAGACCTGAATGGGCAGAGGGTGGGGAGGGGGTTTCCTCN\tEF<HGIHHHIHGIIHHEG=IHIIHHHIIHEIGIGIIIH~\txl:i:90846371\txr:i:90846409\txs:i:39\txd:A:r\txm:A:u\txa:A:\"\"\t\txL:i:90846371\txR:i:90847690\txLseq:i:ATGC\txRseq:i:GCTT\txS:i:1320\txW:i:1252\txP:i:0\txQ:i:0\txC:A:\"\"\txD:A:\"\"\tPG:Z:novoalign\tAS:i:6\tUQ:i:6\tNM:i:1\tMD:Z:38A0\t\n" +
          "SBL_XSMJR268_ID:2363106_BC1:CTCAG_\t161\tmm01\t90847662\t29\t2S29M10S\t=\t90846371\t-1320\tNNTTTATTTGTCCCATGAGCTTATTAAGTCACCTCCTTCTG\t~~IHIIIIIIIGIIIHHEH?HHIHIIIIIIIIHIIIIIIII\txl:i:90847662\txr:i:90847690\txs:i:29\txd:A:f\txm:A:u\txa:A:\"\"\t\txL:i:90846371\txR:i:90847690\txLseq:i:ATGC\txRseq:i:GCTT\txS:i:1320\txW:i:1252\txP:i:0\txQ:i:0\txC:A:\"\"\txD:A:\"\"\tPG:Z:novoalign\tAS:i:106\tUQ:i:106\tNM:i:0\tMD:Z:29\t\n" +
          "SBL_XSMJR314b_ID:262321\t81\tmm01\t150703551\t70\t39M\t=\t150704097\t580\tGAACCAATGAAAGGTATTGTTTGTATTCTTTCAAAACAN\tEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE~\txl:i:150703551\txr:i:150703589\txs:i:39\txd:A:r\txm:A:u\txa:A:\"\"\txL:i:150703551\txR:i:150704130\txLseq:i:AATT\txRseq:i:ACAT\txS:i:580\txW:i:507\txP:i:0\txQ:i:0\txC:A:\"\"\txD:A:\"\"\tPG:Z:novoalign\tAS:i:6\tUQ:i:6\tNM:i:1\tMD:Z:38T0\n" +
          "SBL_XSMJR314b_ID:262321\t161\tmm01\t150704097\t70\t2S34M5S\t=\t150703551\t-580\tNNCATTACATGTTCATCAGCTGCAGAGGCAGTGTAACCATA\t~~EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE\txl:i:150704097\txr:i:150704130\txs:i:34\txd:A:f\txm:A:u\txa:A:\"\"\txL:i:150703551\txR:i:150704130\txLseq:i:AATT\txRseq:i:ACAT\txS:i:580\txW:i:507\txP:i:0\txQ:i:0\txC:A:\"\"\txD:A:\"\"\tPG:Z:novoalign\tAS:i:42\tUQ:i:42\tNM:i:0\tMD:Z:34\n" +
          "SBL_XSMJR271_ID:212165_BC1:TGCAG_\t83\tmm01\t189915571\t70\t3S36M\t=\t189915571\t-36\tAGGTCTGAGAACACAGGGGCTAGGCTAGTAACTAAAGCN\tHGIIHIIHIIIIIHIIIGHFIIIIHIHHHIIHHIHGHE~\txl:i:189915571\txr:i:189915606\txs:i:36\txd:A:r\txm:A:u\txa:A:\"\"\t\txL:i:189915571\txR:i:189915605\txLseq:i:ACTC\txRseq:i:CCCT\txS:i:35\txW:i:-36\txP:i:0\txQ:i:0\txC:A:\"\"\txD:A:\"\"\tPG:Z:novoalign\tAS:i:64\tUQ:i:64\tNM:i:1\tMD:Z:35C0\tPQ:i:173\tSM:i:70\tAM:i:53\t\n" +
          "SBL_XSMJR271_ID:212165_BC1:TGCAG_\t163\tmm01\t189915571\t70\t5S35M1S\t=\t189915571\t36\tNNAGGTCTGAGAACACAGGGGCTAGGCTAGTAACTAAAGCA\t~~IHHHIIIIHIIIIIIIIIIHIIHIIIIIHHIIIIIIIII\txl:i:189915571\txr:i:189915605\txs:i:35\txd:A:f\txm:A:u\txa:A:\"\"\t\txL:i:189915571\txR:i:189915605\txLseq:i:ACTC\txRseq:i:CCCT\txS:i:35\txW:i:-36\txP:i:0\txQ:i:0\txC:A:\"\"\txD:A:\"\"\tPG:Z:novoalign\tAS:i:100\tUQ:i:100\tNM:i:0\tMD:Z:35\tPQ:i:173\tSM:i:53\tAM:i:53\t");

        pw.flush();


    }catch (IOException ioe){
      return null;
    }

    return destination;
  }

  public static File writeSmolSamFile(File destination){
    if(destination == null) return null;
    if(!destination.exists()){
      try{
        destination.createNewFile();
      }catch (IOException ioe){
        return null;
      }
    }

    try(PrintWriter pw = new PrintWriter(new FileWriter(destination))){

      pw.println("@HD\tVN:1.0\tSO:unsorted\n" +
              "@PG\tID:novoalign_1\tPN:novoalign\tVN:V3.08.01\tCL:novoalign -d \"//mnt/htsd/reference sequences/indexed reference sequences/novoindex/current references/mm_SBL_v6.nix\" -f XSMJR314b_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.1.fastq XSMJR314b_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.2.fastq -F STDFQ -r random -o sam\n" +
              "@PG\tID:novoalign_2\tPN:novoalign\tVN:V3.08.01\tCL:novoalign -d \"//mnt/htsd/reference sequences/indexed reference sequences/novoindex/current references/mm_SBL_v6.nix\" -f XSMJR316a_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.1.fastq XSMJR316a_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.2.fastq -F STDFQ -r random -o sam\n" +
              "@PG\tID:novoalign_3\tPN:novoalign\tVN:V3.08.01\tCL:novoalign -d \"//mnt/htsd/reference sequences/indexed reference sequences/novoindex/current references/mm_SBL_v6.nix\" -f XSMJR272_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.1.fastq XSMJR272_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.2.fastq -F STDFQ -r random -o sam\n" +
              "@PG\tID:novoalign_4\tPN:novoalign\tVN:V3.08.01\tCL:novoalign -d \"//mnt/htsd/reference sequences/indexed reference sequences/novoindex/current references/mm_SBL_v6.nix\" -f XSMJR280_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.1.fastq XSMJR280_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.2.fastq -F STDFQ -r random -o sam\n" +
              "@PG\tID:novoalign_5\tPN:novoalign\tVN:V3.08.01\tCL:novoalign -d \"//mnt/htsd/reference sequences/indexed reference sequences/novoindex/current references/mm_SBL_v6.nix\" -f XSMJR315a_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.1.fastq XSMJR315a_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.2.fastq -F STDFQ -r random -o sam\n" +
              "@PG\tID:novoalign_6\tPN:novoalign\tVN:V3.08.01\tCL:novoalign -d \"//mnt/htsd/reference sequences/indexed reference sequences/novoindex/current references/mm_SBL_v6.nix\" -f XSMJR329_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.1.fastq XSMJR329_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.2.fastq -F STDFQ -r random -o sam\n" +
              "@PG\tID:novoalign_7\tPN:novoalign\tVN:V3.08.01\tCL:novoalign -d \"//mnt/htsd/reference sequences/indexed reference sequences/novoindex/current references/mm_SBL_v6.nix\" -f XSMJR316b_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.1.fastq XSMJR316b_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.2.fastq -F STDFQ -r random -o sam\n" +
              "@PG\tID:novoalign_8\tPN:novoalign\tVN:V3.08.01\tCL:novoalign -d \"//mnt/htsd/reference sequences/indexed reference sequences/novoindex/current references/mm_SBL_v6.nix\" -f XSMJR276_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.1.fastq XSMJR276_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.2.fastq -F STDFQ -r random -o sam\n" +
              "@PG\tID:novoalign_9\tPN:novoalign\tVN:V3.08.01\tCL:novoalign -d \"//mnt/htsd/reference sequences/indexed reference sequences/novoindex/current references/mm_SBL_v6.nix\" -f XSMJR271_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.1.fastq XSMJR271_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.2.fastq -F STDFQ -r random -o sam\n" +
              "@PG\tID:novoalign_10\tPN:novoalign\tVN:V3.08.01\tCL:novoalign -d \"//mnt/htsd/reference sequences/indexed reference sequences/novoindex/current references/mm_SBL_v6.nix\" -f XSMJR315b_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.1.fastq XSMJR315b_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.2.fastq -F STDFQ -r random -o sam\n" +
              "@PG\tID:novoalign_11\tPN:novoalign\tVN:V3.08.01\tCL:novoalign -d \"//mnt/htsd/reference sequences/indexed reference sequences/novoindex/current references/mm_SBL_v6.nix\" -f XSMJR314a_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.1.fastq XSMJR314a_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.2.fastq -F STDFQ -r random -o sam\n" +
              "@PG\tID:novoalign_12\tPN:novoalign\tVN:V3.08.01\tCL:novoalign -d \"//mnt/htsd/reference sequences/indexed reference sequences/novoindex/current references/mm_SBL_v6.nix\" -f XSMJR279_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.1.fastq XSMJR279_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.2.fastq -F STDFQ -r random -o sam\n" +
              "@PG\tID:novoalign_13\tPN:novoalign\tVN:V3.08.01\tCL:novoalign -d \"//mnt/htsd/reference sequences/indexed reference sequences/novoindex/current references/mm_SBL_v6.nix\" -f XSMJR275_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.1.fastq XSMJR275_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.2.fastq -F STDFQ -r random -o sam\n" +
              "@PG\tID:novoalign_14\tPN:novoalign\tVN:V3.08.01\tCL:novoalign -d \"//mnt/htsd/reference sequences/indexed reference sequences/novoindex/current references/mm_SBL_v6.nix\" -f XSMJR267_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.1.fastq XSMJR267_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.2.fastq -F STDFQ -r random -o sam\n" +
              "@PG\tID:novoalign_15\tPN:novoalign\tVN:V3.08.01\tCL:novoalign -d \"//mnt/htsd/reference sequences/indexed reference sequences/novoindex/current references/mm_SBL_v6.nix\" -f XSMJR268_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.1.fastq XSMJR268_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.2.fastq -F STDFQ -r random -o sam\n" +
              "@SQ\tSN:mm10\tLN:130694993\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:mm11\tLN:122082543\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:mm12\tLN:120129022\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:mm13\tLN:120421639\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:mm14\tLN:124902244\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:mm15\tLN:104043685\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:mm16\tLN:98207768\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:mm17\tLN:94987271\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:mm18\tLN:90702639\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:mm19\tLN:61431566\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:mm01\tLN:195471971\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:mm02\tLN:182113224\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:mm03\tLN:160039680\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:mm04\tLN:156508116\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:mm05\tLN:151834684\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:mm06\tLN:149736546\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:mm07\tLN:145441459\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:mm08\tLN:129401213\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:mm09\tLN:124595110\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:mmMT\tLN:16299\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:mmX\tLN:171031299\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:mmY\tLN:91744698\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:JH584299.1\tLN:953012\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:GL456233.1\tLN:336933\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:JH584301.1\tLN:259875\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:GL456211.1\tLN:241735\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:GL456350.1\tLN:227966\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:JH584293.1\tLN:207968\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:GL456221.1\tLN:206961\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:JH584297.1\tLN:205776\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:JH584296.1\tLN:199368\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:GL456354.1\tLN:195993\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:JH584294.1\tLN:191905\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:JH584298.1\tLN:184189\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:JH584300.1\tLN:182347\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:GL456219.1\tLN:175968\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:GL456210.1\tLN:169725\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:JH584303.1\tLN:158099\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:JH584302.1\tLN:155838\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:GL456212.1\tLN:153618\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:JH584304.1\tLN:114452\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:GL456379.1\tLN:72385\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:GL456216.1\tLN:66673\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:GL456393.1\tLN:55711\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:GL456366.1\tLN:47073\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:GL456367.1\tLN:42057\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:GL456239.1\tLN:40056\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:GL456213.1\tLN:39340\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:GL456383.1\tLN:38659\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:GL456385.1\tLN:35240\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:GL456360.1\tLN:31704\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:GL456378.1\tLN:31602\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:GL456389.1\tLN:28772\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:GL456372.1\tLN:28664\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:GL456370.1\tLN:26764\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:GL456381.1\tLN:25871\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:GL456387.1\tLN:24685\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:GL456390.1\tLN:24668\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:GL456394.1\tLN:24323\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:GL456392.1\tLN:23629\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:GL456382.1\tLN:23158\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:GL456359.1\tLN:22974\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:GL456396.1\tLN:21240\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:GL456368.1\tLN:20208\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:JH584292.1\tLN:14945\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:JH584295.1\tLN:1976\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:SBL_Con_5B3_minilocus\tLN:18131\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:SBL_Con_A_baumannii_Transposase\tLN:3936291\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:SBL_Con_Acidovorax\tLN:4448856\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:SBL_Con_Ecoli_MG1655\tLN:4639675\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:SBL_Con_E\tLN:3190\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:SBL_Con_eGFP_cds\tLN:720\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:SBL_Con_hLine1\tLN:6050\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:SBL_Con_hmGFP\tLN:4707\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:SBL_Con_Illumina_Adapter_PCR1\tLN:58\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:SBL_Con_Illumina_Adapter_PCR2\tLN:61\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:SBL_Con_MG1655_masked_LacZ\tLN:4639675\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:SBL_Con_pBR322\tLN:4361\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:SBL_Con_pGEM\tLN:3221\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:PGM/Tag_P1_Adapter_PE_1\tLN:50\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:PGM_A_Adapter_1\tLN:32\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:SBL_Con_Phage_Lambda\tLN:48502\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:SBL_Con_Phagemid_f1\tLN:6407\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:SBL_Con_PhiX\tLN:5386\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:SBL_Con_Plasmid_BRA100\tLN:56265\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:SBL_Con_Plasmid_pBVIE02\tLN:265616\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:SBL_Con_Plasmid_pTINT01\tLN:45943\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:SBL_Con_Plasmid_SYK-6\tLN:148801\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:SBL_Con_Propionbacterium.acnes\tLN:2519002\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:SBL_Con_T4_PNK\tLN:906\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:SBL_Con_T4_Pol_cds\tLN:2697\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "@SQ\tSN:SBL_Con_Topo_PCR2.1\tLN:3929\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
              "SBL_XSMJR267_ID:1884132_BC1:CGCAG_\t81\tmm01\t24684050\t30\t39M\t=\t24684188\t175\tTGGTGGTGGTTATAGACCCCAGGTAGTGGTGGTTACAGN\tIIIIIIIIIIIIIIIHIIIIIIIIIIIIIIIIIIIIII~\tPG:Z:novoalign\tAS:i:6\tUQ:i:6\tNM:i:1\tMD:Z:38A0\t\n" +
              "SBL_XSMJR267_ID:1884132_BC1:CGCAG_\t161\tmm01\t24684188\t29\t2S37M2S\t=\t24684050\t-175\tNNCAGGTAGTGGTGGTTACAGACCTCAGGTAGTGGTGGTTA\t~~IIHIHIIIIIIIIIIIIIIIIIIIIIHGHIIIIIIIIII\tPG:Z:novoalign\tAS:i:42\tUQ:i:42\tNM:i:0\tMD:Z:37\t\n" +
              "SBL_XSMJR271_ID:2569046_BC1:TGCAG_\t83\tmm01\t24684186\t70\t39M\t=\t24684188\t39\tCTCAGGTAGTGGTGGTTACAGACCTCAGGTAGTGGTGGN\tIIGIIIIIIIIIIIIIIIIIIIIIIIIIIIIHIIIIII~\tPG:Z:novoalign\tAS:i:6\tUQ:i:6\tNM:i:1\tMD:Z:38T0\tPQ:i:78\tSM:i:70\tAM:i:24\t\n" +
              "SBL_XSMJR271_ID:2569046_BC1:TGCAG_\t163\tmm01\t24684188\t28\t2S37M2S\t=\t24684186\t-39\tNNCAGGTAGTGGTGGTTACAGACCTCAGGTAGTGGTGGTTA\t~~IIIIIIIIIIIIIIIHIIIIIIIIHIGHIIHIIIIIEHF\tPG:Z:novoalign\tAS:i:42\tUQ:i:42\tNM:i:0\tMD:Z:37\tPQ:i:78\tSM:i:24\tAM:i:24\t\n" +
              "SBL_XSMJR314a_ID:mb34vlhd\t81\tmm01\t69021869\t70\t41M\t=\t69021956\t125\tTACTTCACATCCAAAAGACAAATGAACAAAATAAAAAAANN\tGGGGGIGIGIIIIIIGGIIIIIIIIIIIIIIIIIIIIII~~\tPG:Z:novoalign\tAS:i:12\tUQ:i:12\tNM:i:2\tMD:Z:39A0G0\n" +
              "SBL_XSMJR314a_ID:mb34vlhd\t161\tmm01\t69021956\t70\t1S38M\t=\t69021869\t-125\tNAAAGACATTTCCAAATACAAATGACTTAACACTGTAAA\t~GGGGGGGGGGGGIGAGGGGGGGGIGGGIIIGGAGGGGG\tPG:Z:novoalign\tAS:i:6\tUQ:i:6\tNM:i:0\tMD:Z:38\n" +
              "SBL_XSMJR268_ID:2363106_BC1:CTCAG_\t81\tmm01\t90846371\t70\t39M\t=\t90847662\t1320\tAGACCTGAATGGGCAGAGGGTGGGGAGGGGGTTTCCTCN\tEF<HGIHHHIHGIIHHEG=IHIIHHHIIHEIGIGIIIH~\tPG:Z:novoalign\tAS:i:6\tUQ:i:6\tNM:i:1\tMD:Z:38A0\t\n" +
              "SBL_XSMJR268_ID:2363106_BC1:CTCAG_\t161\tmm01\t90847662\t29\t2S29M10S\t=\t90846371\t-1320\tNNTTTATTTGTCCCATGAGCTTATTAAGTCACCTCCTTCTG\t~~IHIIIIIIIGIIIHHEH?HHIHIIIIIIIIHIIIIIIII\tPG:Z:novoalign\tAS:i:106\tUQ:i:106\tNM:i:0\tMD:Z:29\t\n" +
              "SBL_XSMJR314b_ID:262321\t81\tmm01\t150703551\t70\t39M\t=\t150704097\t580\tGAACCAATGAAAGGTATTGTTTGTATTCTTTCAAAACAN\tEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE~\tPG:Z:novoalign\tAS:i:6\tUQ:i:6\tNM:i:1\tMD:Z:38T0\n" +
              "SBL_XSMJR314b_ID:262321\t161\tmm01\t150704097\t70\t2S34M5S\t=\t150703551\t-580\tNNCATTACATGTTCATCAGCTGCAGAGGCAGTGTAACCATA\t~~EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE\tPG:Z:novoalign\tAS:i:42\tUQ:i:42\tNM:i:0\tMD:Z:34\n" +
              "SBL_XSMJR271_ID:212165_BC1:TGCAG_\t83\tmm01\t189915571\t70\t3S36M\t=\t189915571\t-36\tAGGTCTGAGAACACAGGGGCTAGGCTAGTAACTAAAGCN\tHGIIHIIHIIIIIHIIIGHFIIIIHIHHHIIHHIHGHE~\tPG:Z:novoalign\tAS:i:64\tUQ:i:64\tNM:i:1\tMD:Z:35C0\tPQ:i:173\tSM:i:70\tAM:i:53\t\n" +
              "SBL_XSMJR271_ID:212165_BC1:TGCAG_\t163\tmm01\t189915571\t70\t5S35M1S\t=\t189915571\t36\tNNAGGTCTGAGAACACAGGGGCTAGGCTAGTAACTAAAGCA\t~~IHHHIIIIHIIIIIIIIIIHIIHIIIIIHHIIIIIIIII\tPG:Z:novoalign\tAS:i:100\tUQ:i:100\tNM:i:0\tMD:Z:35\tPQ:i:173\tSM:i:53\tAM:i:53\t");

      pw.flush();


    }catch (IOException ioe){
      return null;
    }

    return destination;
  }

  public static File createFakeXsamHeader(File destination) throws IOException{
    try(PrintWriter pw = new PrintWriter(new FileWriter(destination))) {
      pw.println("@HD\tVN:1.0\tSO:unsorted\n" +
          "@PG\tID:novoalign_1\tPN:novoalign\tVN:V3.08.01\tCL:novoalign -d \"//mnt/htsd/reference sequences/indexed reference sequences/novoindex/current references/mm_SBL_v6.nix\" -f XSMJR314b_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.1.fastq XSMJR314b_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.2.fastq -F STDFQ -r random -o sam\n" +
          "@PG\tID:novoalign_2\tPN:novoalign\tVN:V3.08.01\tCL:novoalign -d \"//mnt/htsd/reference sequences/indexed reference sequences/novoindex/current references/mm_SBL_v6.nix\" -f XSMJR316a_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.1.fastq XSMJR316a_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.2.fastq -F STDFQ -r random -o sam\n" +
          "@PG\tID:novoalign_3\tPN:novoalign\tVN:V3.08.01\tCL:novoalign -d \"//mnt/htsd/reference sequences/indexed reference sequences/novoindex/current references/mm_SBL_v6.nix\" -f XSMJR272_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.1.fastq XSMJR272_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.2.fastq -F STDFQ -r random -o sam\n" +
          "@PG\tID:novoalign_4\tPN:novoalign\tVN:V3.08.01\tCL:novoalign -d \"//mnt/htsd/reference sequences/indexed reference sequences/novoindex/current references/mm_SBL_v6.nix\" -f XSMJR280_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.1.fastq XSMJR280_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.2.fastq -F STDFQ -r random -o sam\n" +
          "@PG\tID:novoalign_5\tPN:novoalign\tVN:V3.08.01\tCL:novoalign -d \"//mnt/htsd/reference sequences/indexed reference sequences/novoindex/current references/mm_SBL_v6.nix\" -f XSMJR315a_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.1.fastq XSMJR315a_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.2.fastq -F STDFQ -r random -o sam\n" +
          "@PG\tID:novoalign_6\tPN:novoalign\tVN:V3.08.01\tCL:novoalign -d \"//mnt/htsd/reference sequences/indexed reference sequences/novoindex/current references/mm_SBL_v6.nix\" -f XSMJR329_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.1.fastq XSMJR329_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.2.fastq -F STDFQ -r random -o sam\n" +
          "@PG\tID:novoalign_7\tPN:novoalign\tVN:V3.08.01\tCL:novoalign -d \"//mnt/htsd/reference sequences/indexed reference sequences/novoindex/current references/mm_SBL_v6.nix\" -f XSMJR316b_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.1.fastq XSMJR316b_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.2.fastq -F STDFQ -r random -o sam\n" +
          "@PG\tID:novoalign_8\tPN:novoalign\tVN:V3.08.01\tCL:novoalign -d \"//mnt/htsd/reference sequences/indexed reference sequences/novoindex/current references/mm_SBL_v6.nix\" -f XSMJR276_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.1.fastq XSMJR276_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.2.fastq -F STDFQ -r random -o sam\n" +
          "@PG\tID:novoalign_9\tPN:novoalign\tVN:V3.08.01\tCL:novoalign -d \"//mnt/htsd/reference sequences/indexed reference sequences/novoindex/current references/mm_SBL_v6.nix\" -f XSMJR271_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.1.fastq XSMJR271_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.2.fastq -F STDFQ -r random -o sam\n" +
          "@PG\tID:novoalign_10\tPN:novoalign\tVN:V3.08.01\tCL:novoalign -d \"//mnt/htsd/reference sequences/indexed reference sequences/novoindex/current references/mm_SBL_v6.nix\" -f XSMJR315b_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.1.fastq XSMJR315b_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.2.fastq -F STDFQ -r random -o sam\n" +
          "@PG\tID:novoalign_11\tPN:novoalign\tVN:V3.08.01\tCL:novoalign -d \"//mnt/htsd/reference sequences/indexed reference sequences/novoindex/current references/mm_SBL_v6.nix\" -f XSMJR314a_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.1.fastq XSMJR314a_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.2.fastq -F STDFQ -r random -o sam\n" +
          "@PG\tID:novoalign_12\tPN:novoalign\tVN:V3.08.01\tCL:novoalign -d \"//mnt/htsd/reference sequences/indexed reference sequences/novoindex/current references/mm_SBL_v6.nix\" -f XSMJR279_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.1.fastq XSMJR279_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.2.fastq -F STDFQ -r random -o sam\n" +
          "@PG\tID:novoalign_13\tPN:novoalign\tVN:V3.08.01\tCL:novoalign -d \"//mnt/htsd/reference sequences/indexed reference sequences/novoindex/current references/mm_SBL_v6.nix\" -f XSMJR275_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.1.fastq XSMJR275_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.2.fastq -F STDFQ -r random -o sam\n" +
          "@PG\tID:novoalign_14\tPN:novoalign\tVN:V3.08.01\tCL:novoalign -d \"//mnt/htsd/reference sequences/indexed reference sequences/novoindex/current references/mm_SBL_v6.nix\" -f XSMJR267_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.1.fastq XSMJR267_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.2.fastq -F STDFQ -r random -o sam\n" +
          "@PG\tID:novoalign_15\tPN:novoalign\tVN:V3.08.01\tCL:novoalign -d \"//mnt/htsd/reference sequences/indexed reference sequences/novoindex/current references/mm_SBL_v6.nix\" -f XSMJR268_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.1.fastq XSMJR268_43bp.vPE.breakseq.Sticky_Paper_v3.not_sticky.2.fastq -F STDFQ -r random -o sam\n" +
          "@SQ\tSN:mm10\tLN:130694993\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:mm11\tLN:122082543\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:mm12\tLN:120129022\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:mm13\tLN:120421639\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:mm14\tLN:124902244\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:mm15\tLN:104043685\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:mm16\tLN:98207768\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:mm17\tLN:94987271\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:mm18\tLN:90702639\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:mm19\tLN:61431566\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:mm01\tLN:195471971\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:mm02\tLN:182113224\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:mm03\tLN:160039680\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:mm04\tLN:156508116\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:mm05\tLN:151834684\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:mm06\tLN:149736546\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:mm07\tLN:145441459\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:mm08\tLN:129401213\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:mm09\tLN:124595110\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:mmMT\tLN:16299\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:mmX\tLN:171031299\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:mmY\tLN:91744698\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:JH584299.1\tLN:953012\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456233.1\tLN:336933\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:JH584301.1\tLN:259875\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456211.1\tLN:241735\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456350.1\tLN:227966\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:JH584293.1\tLN:207968\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456221.1\tLN:206961\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:JH584297.1\tLN:205776\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:JH584296.1\tLN:199368\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456354.1\tLN:195993\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:JH584294.1\tLN:191905\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:JH584298.1\tLN:184189\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:JH584300.1\tLN:182347\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456219.1\tLN:175968\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456210.1\tLN:169725\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:JH584303.1\tLN:158099\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:JH584302.1\tLN:155838\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456212.1\tLN:153618\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:JH584304.1\tLN:114452\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456379.1\tLN:72385\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456216.1\tLN:66673\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456393.1\tLN:55711\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456366.1\tLN:47073\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456367.1\tLN:42057\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456239.1\tLN:40056\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456213.1\tLN:39340\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456383.1\tLN:38659\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456385.1\tLN:35240\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456360.1\tLN:31704\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456378.1\tLN:31602\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456389.1\tLN:28772\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456372.1\tLN:28664\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456370.1\tLN:26764\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456381.1\tLN:25871\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456387.1\tLN:24685\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456390.1\tLN:24668\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456394.1\tLN:24323\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456392.1\tLN:23629\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456382.1\tLN:23158\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456359.1\tLN:22974\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456396.1\tLN:21240\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:GL456368.1\tLN:20208\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:JH584292.1\tLN:14945\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:JH584295.1\tLN:1976\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:SBL_Con_5B3_minilocus\tLN:18131\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:SBL_Con_A_baumannii_Transposase\tLN:3936291\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:SBL_Con_Acidovorax\tLN:4448856\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:SBL_Con_Ecoli_MG1655\tLN:4639675\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:SBL_Con_E\tLN:3190\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:SBL_Con_eGFP_cds\tLN:720\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:SBL_Con_hLine1\tLN:6050\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:SBL_Con_hmGFP\tLN:4707\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:SBL_Con_Illumina_Adapter_PCR1\tLN:58\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:SBL_Con_Illumina_Adapter_PCR2\tLN:61\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:SBL_Con_MG1655_masked_LacZ\tLN:4639675\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:SBL_Con_pBR322\tLN:4361\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:SBL_Con_pGEM\tLN:3221\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:PGM/Tag_P1_Adapter_PE_1\tLN:50\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:PGM_A_Adapter_1\tLN:32\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:SBL_Con_Phage_Lambda\tLN:48502\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:SBL_Con_Phagemid_f1\tLN:6407\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:SBL_Con_PhiX\tLN:5386\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:SBL_Con_Plasmid_BRA100\tLN:56265\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:SBL_Con_Plasmid_pBVIE02\tLN:265616\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:SBL_Con_Plasmid_pTINT01\tLN:45943\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:SBL_Con_Plasmid_SYK-6\tLN:148801\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:SBL_Con_Propionbacterium.acnes\tLN:2519002\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:SBL_Con_T4_PNK\tLN:906\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:SBL_Con_T4_Pol_cds\tLN:2697\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@SQ\tSN:SBL_Con_Topo_PCR2.1\tLN:3929\tAS:novoindex_k11_s3_mm_38.90_dna_full_SBL_Cont_v6.nix\n" +
          "@CO\tXsam:F:Generated at 2018/11/30 10:25:01\n" +
          "@CO\tXsam:F:Generated by Xsam version 2.060\n" +
          "@CO\tXsam:F:type is Paired-End\n" +
          "@CO\tXsam:S:P1:2000\n" +
          "@CO\tXsam:S:P2:100000\n" +
          "@CO\tXsam:S:P3:3000000\n" +
          "@CO\tXsam:P:GL456210.1:1:97:72480:0\n" +
          "@CO\tXsam:P:GL456210.1:2:0:0:72480\n" +
          "@CO\tXsam:P:GL456210.1:3:0:0:72480\n" +
          "@CO\tXsam:P:GL456210.1:4:0:0:72480\n" +
          "@CO\tXsam:P:GL456211.1:1:154:115570:72480\n" +
          "@CO\tXsam:P:GL456211.1:2:0:0:188050\n" +
          "@CO\tXsam:P:GL456211.1:3:0:0:188050\n" +
          "@CO\tXsam:P:GL456211.1:4:0:0:188050\n" +
          "@CO\tXsam:P:GL456212.1:1:98:73253:188050\n" +
          "@CO\tXsam:P:GL456212.1:2:0:0:261303\n" +
          "@CO\tXsam:P:GL456212.1:3:0:0:261303\n" +
          "@CO\tXsam:P:GL456212.1:4:0:0:261303\n" +
          "@CO\tXsam:P:GL456216.1:1:73:54202:261303\n" +
          "@CO\tXsam:P:GL456216.1:2:0:0:315505\n" +
          "@CO\tXsam:P:GL456216.1:3:0:0:315505\n" +
          "@CO\tXsam:P:GL456216.1:4:0:0:315505\n" +
          "@CO\tXsam:P:GL456219.1:1:1:760:315505\n" +
          "@CO\tXsam:P:GL456219.1:2:0:0:316265\n" +
          "@CO\tXsam:P:GL456219.1:3:0:0:316265\n" +
          "@CO\tXsam:P:GL456219.1:4:0:0:316265\n" +
          "@CO\tXsam:P:GL456221.1:1:85:63263:316265\n" +
          "@CO\tXsam:P:GL456221.1:2:0:0:379528\n" +
          "@CO\tXsam:P:GL456221.1:3:0:0:379528\n" +
          "@CO\tXsam:P:GL456221.1:4:0:0:379528\n" +
          "@CO\tXsam:P:GL456233.1:1:118:88551:379528\n" +
          "@CO\tXsam:P:GL456233.1:2:0:0:468079\n" +
          "@CO\tXsam:P:GL456233.1:3:0:0:468079\n" +
          "@CO\tXsam:P:GL456233.1:4:0:0:468079\n" +
          "@CO\tXsam:P:GL456239.1:1:157:115898:468079\n" +
          "@CO\tXsam:P:GL456239.1:2:0:0:583977\n" +
          "@CO\tXsam:P:GL456239.1:3:0:0:583977\n" +
          "@CO\tXsam:P:GL456239.1:4:0:0:583977\n" +
          "@CO\tXsam:P:GL456354.1:1:6:4506:583977\n" +
          "@CO\tXsam:P:GL456354.1:2:0:0:588483\n" +
          "@CO\tXsam:P:GL456354.1:3:0:0:588483\n" +
          "@CO\tXsam:P:GL456354.1:4:0:0:588483\n" +
          "@CO\tXsam:P:GL456359.1:1:32:23568:588483\n" +
          "@CO\tXsam:P:GL456359.1:2:0:0:612051\n" +
          "@CO\tXsam:P:GL456359.1:3:0:0:612051\n" +
          "@CO\tXsam:P:GL456359.1:4:0:0:612051\n" +
          "@CO\tXsam:P:GL456360.1:1:29:21450:612051\n" +
          "@CO\tXsam:P:GL456360.1:2:0:0:633501\n" +
          "@CO\tXsam:P:GL456360.1:3:0:0:633501\n" +
          "@CO\tXsam:P:GL456360.1:4:0:0:633501\n" +
          "@CO\tXsam:P:GL456366.1:1:45:33218:633501\n" +
          "@CO\tXsam:P:GL456366.1:2:0:0:666719\n" +
          "@CO\tXsam:P:GL456366.1:3:0:0:666719\n" +
          "@CO\tXsam:P:GL456366.1:4:0:0:666719\n" +
          "@CO\tXsam:P:GL456367.1:1:31:23007:666719\n" +
          "@CO\tXsam:P:GL456367.1:2:0:0:689726\n" +
          "@CO\tXsam:P:GL456367.1:3:0:0:689726\n" +
          "@CO\tXsam:P:GL456367.1:4:0:0:689726\n" +
          "@CO\tXsam:P:GL456368.1:1:12:8807:689726\n" +
          "@CO\tXsam:P:GL456368.1:2:0:0:698533\n" +
          "@CO\tXsam:P:GL456368.1:3:0:0:698533\n" +
          "@CO\tXsam:P:GL456368.1:4:0:0:698533\n" +
          "@CO\tXsam:P:GL456370.1:1:17:12540:698533\n" +
          "@CO\tXsam:P:GL456370.1:2:0:0:711073\n" +
          "@CO\tXsam:P:GL456370.1:3:0:0:711073\n" +
          "@CO\tXsam:P:GL456370.1:4:0:0:711073\n" +
          "@CO\tXsam:P:GL456372.1:1:23:16992:711073\n" +
          "@CO\tXsam:P:GL456372.1:2:0:0:728065\n" +
          "@CO\tXsam:P:GL456372.1:3:0:0:728065\n" +
          "@CO\tXsam:P:GL456372.1:4:0:0:728065\n" +
          "@CO\tXsam:P:GL456378.1:1:42:30751:728065\n" +
          "@CO\tXsam:P:GL456378.1:2:0:0:758816\n" +
          "@CO\tXsam:P:GL456378.1:3:0:0:758816\n" +
          "@CO\tXsam:P:GL456378.1:4:0:0:758816\n" +
          "@CO\tXsam:P:GL456379.1:1:39:28972:758816\n" +
          "@CO\tXsam:P:GL456379.1:2:0:0:787788\n" +
          "@CO\tXsam:P:GL456379.1:3:0:0:787788\n" +
          "@CO\tXsam:P:GL456379.1:4:0:0:787788\n" +
          "@CO\tXsam:P:GL456381.1:1:13:9627:787788\n" +
          "@CO\tXsam:P:GL456381.1:2:0:0:797415\n" +
          "@CO\tXsam:P:GL456381.1:3:0:0:797415\n" +
          "@CO\tXsam:P:GL456381.1:4:0:0:797415\n" +
          "@CO\tXsam:P:GL456382.1:1:5:3766:797415\n" +
          "@CO\tXsam:P:GL456382.1:2:0:0:801181\n" +
          "@CO\tXsam:P:GL456382.1:3:0:0:801181\n" +
          "@CO\tXsam:P:GL456382.1:4:0:0:801181\n" +
          "@CO\tXsam:P:GL456383.1:1:100:74559:801181\n" +
          "@CO\tXsam:P:GL456383.1:2:2:1402:875740\n" +
          "@CO\tXsam:P:GL456383.1:3:0:0:877142\n" +
          "@CO\tXsam:P:GL456383.1:4:0:0:877142\n" +
          "@CO\tXsam:P:GL456385.1:1:16:11764:877142\n" +
          "@CO\tXsam:P:GL456385.1:2:0:0:888906\n" +
          "@CO\tXsam:P:GL456385.1:3:0:0:888906\n" +
          "@CO\tXsam:P:GL456385.1:4:0:0:888906\n" +
          "@CO\tXsam:P:GL456387.1:1:67:49413:888906\n" +
          "@CO\tXsam:P:GL456387.1:2:0:0:938319\n" +
          "@CO\tXsam:P:GL456387.1:3:0:0:938319\n" +
          "@CO\tXsam:P:GL456387.1:4:0:0:938319\n" +
          "@CO\tXsam:P:GL456389.1:1:44:32674:938319\n" +
          "@CO\tXsam:P:GL456389.1:2:0:0:970993\n" +
          "@CO\tXsam:P:GL456389.1:3:0:0:970993\n" +
          "@CO\tXsam:P:GL456389.1:4:0:0:970993\n" +
          "@CO\tXsam:P:GL456390.1:1:127:94610:970993\n" +
          "@CO\tXsam:P:GL456390.1:2:3:2094:1065603\n" +
          "@CO\tXsam:P:GL456390.1:3:0:0:1067697\n" +
          "@CO\tXsam:P:GL456390.1:4:0:0:1067697\n" +
          "@CO\tXsam:P:GL456392.1:1:91:67107:1067697\n" +
          "@CO\tXsam:P:GL456392.1:2:0:0:1134804\n" +
          "@CO\tXsam:P:GL456392.1:3:0:0:1134804\n" +
          "@CO\tXsam:P:GL456392.1:4:0:0:1134804\n" +
          "@CO\tXsam:P:GL456393.1:1:130:96338:1134804\n" +
          "@CO\tXsam:P:GL456393.1:2:1:695:1231142\n" +
          "@CO\tXsam:P:GL456393.1:3:0:0:1231837\n" +
          "@CO\tXsam:P:GL456393.1:4:0:0:1231837\n" +
          "@CO\tXsam:P:GL456394.1:1:28:20838:1231837\n" +
          "@CO\tXsam:P:GL456394.1:2:0:0:1252675\n" +
          "@CO\tXsam:P:GL456394.1:3:0:0:1252675\n" +
          "@CO\tXsam:P:GL456394.1:4:0:0:1252675\n" +
          "@CO\tXsam:P:GL456396.1:1:112:83400:1252675\n" +
          "@CO\tXsam:P:GL456396.1:2:2:1423:1336075\n" +
          "@CO\tXsam:P:GL456396.1:3:0:0:1337498\n" +
          "@CO\tXsam:P:GL456396.1:4:0:0:1337498\n" +
          "@CO\tXsam:P:JH584292.1:1:26:19173:1337498\n" +
          "@CO\tXsam:P:JH584292.1:2:0:0:1356671\n" +
          "@CO\tXsam:P:JH584292.1:3:0:0:1356671\n" +
          "@CO\tXsam:P:JH584292.1:4:0:0:1356671\n" +
          "@CO\tXsam:P:JH584295.1:1:3:2186:1356671\n" +
          "@CO\tXsam:P:JH584295.1:2:0:0:1358857\n" +
          "@CO\tXsam:P:JH584295.1:3:0:0:1358857\n" +
          "@CO\tXsam:P:JH584295.1:4:0:0:1358857\n" +
          "@CO\tXsam:P:JH584299.1:1:51:38372:1358857\n" +
          "@CO\tXsam:P:JH584299.1:2:0:0:1397229\n" +
          "@CO\tXsam:P:JH584299.1:3:0:0:1397229\n" +
          "@CO\tXsam:P:JH584299.1:4:0:0:1397229\n" +
          "@CO\tXsam:P:JH584301.1:1:1:735:1397229\n" +
          "@CO\tXsam:P:JH584301.1:2:0:0:1397964\n" +
          "@CO\tXsam:P:JH584301.1:3:0:0:1397964\n" +
          "@CO\tXsam:P:JH584301.1:4:0:0:1397964\n" +
          "@CO\tXsam:P:JH584304.1:1:2118:1569113:1397964\n" +
          "@CO\tXsam:P:JH584304.1:2:0:0:2967077\n" +
          "@CO\tXsam:P:JH584304.1:3:0:0:2967077\n" +
          "@CO\tXsam:P:JH584304.1:4:0:0:2967077\n" +
          "@CO\tXsam:P:SBL_Con_Propionbacterium.acnes:1:40:32015:2967077\n" +
          "@CO\tXsam:P:SBL_Con_Propionbacterium.acnes:2:0:0:2999092\n" +
          "@CO\tXsam:P:SBL_Con_Propionbacterium.acnes:3:0:0:2999092\n" +
          "@CO\tXsam:P:SBL_Con_Propionbacterium.acnes:4:0:0:2999092\n" +
          "@CO\tXsam:P:SBL_Con_hLine1:1:3:2240:2999092\n" +
          "@CO\tXsam:P:SBL_Con_hLine1:2:0:0:3001332\n" +
          "@CO\tXsam:P:SBL_Con_hLine1:3:0:0:3001332\n" +
          "@CO\tXsam:P:SBL_Con_hLine1:4:0:0:3001332\n" +
          "@CO\tXsam:P:mm01:1:227920:175919733:3001332\n" +
          "@CO\tXsam:P:mm01:2:11:8105:178921065\n" +
          "@CO\tXsam:P:mm01:3:31:23179:178929170\n" +
          "@CO\tXsam:P:mm01:4:749:566302:178952349\n" +
          "@CO\tXsam:P:mm02:1:231492:178654905:179518651\n" +
          "@CO\tXsam:P:mm02:2:10:7261:358173556\n" +
          "@CO\tXsam:P:mm02:3:42:31387:358180817\n" +
          "@CO\tXsam:P:mm02:4:1004:757634:358212204\n" +
          "@CO\tXsam:P:mm03:1:175682:135318911:358969838\n" +
          "@CO\tXsam:P:mm03:2:5:3658:494288749\n" +
          "@CO\tXsam:P:mm03:3:31:23067:494292407\n" +
          "@CO\tXsam:P:mm03:4:435:327409:494315474\n" +
          "@CO\tXsam:P:mm04:1:190685:147118472:494642883\n" +
          "@CO\tXsam:P:mm04:2:14:10273:641761355\n" +
          "@CO\tXsam:P:mm04:3:37:27814:641771628\n" +
          "@CO\tXsam:P:mm04:4:474:358135:641799442\n" +
          "@CO\tXsam:P:mm05:1:192487:148504657:642157577\n" +
          "@CO\tXsam:P:mm05:2:7:5057:790662234\n" +
          "@CO\tXsam:P:mm05:3:46:34517:790667291\n" +
          "@CO\tXsam:P:mm05:4:473:357933:790701808\n" +
          "@CO\tXsam:P:mm06:1:177370:136556168:791059741\n" +
          "@CO\tXsam:P:mm06:2:10:7338:927615909\n" +
          "@CO\tXsam:P:mm06:3:28:20913:927623247\n" +
          "@CO\tXsam:P:mm06:4:454:341811:927644160\n" +
          "@CO\tXsam:P:mm07:1:176368:135971584:927985971\n" +
          "@CO\tXsam:P:mm07:2:7:5127:1063957555\n" +
          "@CO\tXsam:P:mm07:3:19:14164:1063962682\n" +
          "@CO\tXsam:P:mm07:4:431:325482:1063976846\n" +
          "@CO\tXsam:P:mm08:1:161526:124262486:1064302328\n" +
          "@CO\tXsam:P:mm08:2:4:2933:1188564814\n" +
          "@CO\tXsam:P:mm08:3:30:22377:1188567747\n" +
          "@CO\tXsam:P:mm08:4:368:277286:1188590124\n" +
          "@CO\tXsam:P:mm09:1:165094:126935685:1188867410\n" +
          "@CO\tXsam:P:mm09:2:3:2194:1315803095\n" +
          "@CO\tXsam:P:mm09:3:16:11871:1315805289\n" +
          "@CO\tXsam:P:mm09:4:379:284845:1315817160\n" +
          "@CO\tXsam:P:mm10:1:155427:119412514:1316102005\n" +
          "@CO\tXsam:P:mm10:2:8:5810:1435514519\n" +
          "@CO\tXsam:P:mm10:3:26:19280:1435520329\n" +
          "@CO\tXsam:P:mm10:4:368:276564:1435539609\n" +
          "@CO\tXsam:P:mm11:1:175230:134676361:1435816173\n" +
          "@CO\tXsam:P:mm11:2:7:5160:1570492534\n" +
          "@CO\tXsam:P:mm11:3:30:22365:1570497694\n" +
          "@CO\tXsam:P:mm11:4:370:278112:1570520059\n" +
          "@CO\tXsam:P:mm12:1:142139:109169883:1570798171\n" +
          "@CO\tXsam:P:mm12:2:5:3691:1679968054\n" +
          "@CO\tXsam:P:mm12:3:19:14256:1679971745\n" +
          "@CO\tXsam:P:mm12:4:283:212628:1679986001\n" +
          "@CO\tXsam:P:mm13:1:144781:111142316:1680198629\n" +
          "@CO\tXsam:P:mm13:2:7:5075:1791340945\n" +
          "@CO\tXsam:P:mm13:3:24:17823:1791346020\n" +
          "@CO\tXsam:P:mm13:4:249:186692:1791363843\n" +
          "@CO\tXsam:P:mm14:1:136537:104870919:1791550535\n" +
          "@CO\tXsam:P:mm14:2:6:4340:1896421454\n" +
          "@CO\tXsam:P:mm14:3:18:13459:1896425794\n" +
          "@CO\tXsam:P:mm14:4:247:185372:1896439253\n" +
          "@CO\tXsam:P:mm15:1:125250:95961301:1896624625\n" +
          "@CO\tXsam:P:mm15:2:7:5040:1992585926\n" +
          "@CO\tXsam:P:mm15:3:17:12610:1992590966\n" +
          "@CO\tXsam:P:mm15:4:208:155872:1992603576\n" +
          "@CO\tXsam:P:mm16:1:114231:87385620:1992759448\n" +
          "@CO\tXsam:P:mm16:2:3:2159:2080145068\n" +
          "@CO\tXsam:P:mm16:3:17:12604:2080147227\n" +
          "@CO\tXsam:P:mm16:4:177:132321:2080159831\n" +
          "@CO\tXsam:P:mm17:1:119789:91682143:2080292152\n" +
          "@CO\tXsam:P:mm17:2:7:5121:2171974295\n" +
          "@CO\tXsam:P:mm17:3:23:17074:2171979416\n" +
          "@CO\tXsam:P:mm17:4:192:143611:2171996490\n" +
          "@CO\tXsam:P:mm18:1:109457:83766235:2172140101\n" +
          "@CO\tXsam:P:mm18:2:5:3616:2255906336\n" +
          "@CO\tXsam:P:mm18:3:15:11149:2255909952\n" +
          "@CO\tXsam:P:mm18:4:163:122016:2255921101\n" +
          "@CO\tXsam:P:mm19:1:79961:61162433:2256043117\n" +
          "@CO\tXsam:P:mm19:2:3:2182:2317205550\n" +
          "@CO\tXsam:P:mm19:3:11:8103:2317207732\n" +
          "@CO\tXsam:P:mm19:4:77:57538:2317215835\n" +
          "@CO\tXsam:P:mmX:1:87267:67248583:2317273373\n" +
          "@CO\tXsam:P:mmX:2:2:1494:2384521956\n" +
          "@CO\tXsam:P:mmX:3:9:6729:2384523450\n" +
          "@CO\tXsam:P:mmX:4:110:83149:2384530179\n" +
          "@CO\tXsam:P:mmY:1:1846:1380736:2384613328\n" +
          "@CO\tXsam:P:mmY:2:5:3614:2385994064\n" +
          "@CO\tXsam:P:mmY:3:0:0:2385997678\n" +
          "@CO\tXsam:P:mmY:4:0:0:2385997678\n");

      pw.flush();

    }catch (IOException ioe){
      return null;
    }

    return destination;
  }

}
