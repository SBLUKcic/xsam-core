package com.sbl.benchmarking;


import com.sbl.conversion.XsamConverter;
import com.sbl.conversion.XsamConverterFactory;
import com.sbl.filtering.model.Range;
import com.sbl.filtering.predicates.XsamRecordPredicates;
import com.sbl.model.SamRecord;
import com.sbl.model.XsamRecord;
import com.sbl.model.footprints.XsamRecordFootprint;
import com.sbl.model.xsamtypes.XsamRecordMappingType;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.MICROSECONDS)
@State(Scope.Benchmark)
public class FilteringBenchmarking {

    //GOOD ARTICLE: https://dzone.com/articles/iteration-over-java-collections-with-high-performa

    /*
    Results for 10000 records:
    Benchmark                                            Mode  Cnt    Score    Error  Units
    FilteringBenchmarking.benchmarkForEachFiltering      avgt   20  355.788 ± 30.867  us/op
    FilteringBenchmarking.benchmarkForLoopFiltering      avgt   20  341.263 ± 31.429  us/op
    FilteringBenchmarking.benchmarkSimpleLoopStyle       avgt   20  336.481 ± 27.974  us/op
    FilteringBenchmarking.benchmarkStreamBasedFiltering  avgt   20  341.907 ± 22.670  us/op
    */

    @State(Scope.Thread)
    public static class MyState {

        @Setup(Level.Trial)
        public void doSetup() {
            records = randomLongListOfRecords();

        }

        private Predicate<XsamRecord> pred1 = XsamRecordPredicates.createFootprintInRangePredicate(XsamRecordFootprint.Lb, new Range(0, 100));
        private Predicate<XsamRecord> pred2 = XsamRecordPredicates.createMappingTypePredicate(XsamRecordMappingType.P);
        private Predicate<XsamRecord> pred3 = XsamRecordPredicates.createSpanInRangePredicate(new Range(0, 500));

        public List<XsamRecord> records;

        public Predicate<XsamRecord> finalPredicate = Stream.of(pred1, pred2, pred3).reduce(Predicate::or).orElse(x->false);
    }

    @Benchmark
    public List<XsamRecord> benchmarkStreamBasedFiltering(MyState state) {

        return state.records.stream().filter(state.finalPredicate).collect(Collectors.toList());

    }

    @Benchmark
    public List<XsamRecord> benchmarkForLoopFiltering(MyState state){

        List<XsamRecord> passedRecords = new ArrayList<>();

        for(XsamRecord record : state.records){
            if(state.finalPredicate.test(record)){
                passedRecords.add(record);
            }
        }
        return passedRecords;
    }

    @Benchmark
    public List<XsamRecord> benchmarkForEachFiltering(MyState state) {

        List<XsamRecord> passedRecords = new ArrayList<>();

        state.records.forEach(e -> {
            if(state.finalPredicate.test(e)){
                passedRecords.add(e);
            }
        });

        return passedRecords;
    }

    @Benchmark
    public List<XsamRecord> benchmarkSimpleLoopStyle(MyState state) {

        List<XsamRecord> passedRecords = new ArrayList<>();

        for(int i = 0; i < state.records.size(); i++){
            XsamRecord record = state.records.get(i); //hold ref
            if(state.finalPredicate.test(record)){
                passedRecords.add(record);
            }
        }

        return passedRecords;
    }

    public static void main(String... args) throws RunnerException {
        Options opts = new OptionsBuilder()
                .include(".*")
                .warmupIterations(5)
                .measurementIterations(10)
                .jvmArgs("-Xms2g", "-Xmx2g")
                .shouldDoGC(true)
                .forks(2)
                .build();

        new Runner(opts).run();
    }

    private static List<XsamRecord> randomLongListOfRecords(){

        List<XsamRecord> records = new ArrayList<>();

        Random r = new Random();

        String[] rnames = new String[]{"mm01", "mm02", "mm03", "mm04"};

        XsamConverterFactory f = new XsamConverterFactory();
        XsamConverter converter = f.getXsamConverter(1);

        //10000 records per thread is reasonable.
        for(int i = 0; i < 10000; i++){
            StringJoiner sb = new StringJoiner("\t");
            sb.add(String.format("SBL_XSNP077_ID:%s", r.nextInt()));
            sb.add(String.valueOf(r.nextInt(4095)));
            sb.add(rnames[r.nextInt(3)]);
            int pos = r.nextInt();
            if(pos < 0){
                pos = Math.abs(pos);
            }
            sb.add(String.valueOf(pos));
            sb.add("70");
            sb.add("39M");
            sb.add("=");
            sb.add("pos");
            sb.add("90");
            sb.add("NGCTGTCTGTGTGTATATATCTCACACATGT");
            sb.add("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            sb.add("PG:Novoalign");
            sb.add("AS:i:12");

            XsamRecord record = converter.convertSingleRecord(new SamRecord(sb.toString()));
            records.add(record);
        }

        return records;

    }

}
