package com.sbl.sorting;

import com.sbl.model.XsamRecord;
import com.sbl.utils.XsamReadQueries;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class LRSorterTest {

    @ParameterizedTest
    @MethodSource("com.sbl.providers.XsamRecordProviders#listOfRecords")
    void givenListOfRecords_whenSorted_checkSortingCorrect(List<XsamRecord> list){

        //assert out of order
        assertTrue(list.get(2).getXl() < list.get(1).getXl());

        Collections.sort(list, SortingMethods.createDefaultRecordComparator());


        assertTrue(list.get(0).getXl() == list.get(1).getXl());
        assertTrue(list.get(1).getXl() == list.get(2).getXl());
        assertTrue(list.get(2).getXl() == list.get(3).getXl());
        assertTrue(list.get(3).getXl() < list.get(4).getXl());

    }

    @ParameterizedTest
    @MethodSource("com.sbl.providers.XsamRecordProviders#listOfRecords")
    void givenListOfRecordsInStrings_whenSorted_checkSortingCorrect(List<XsamRecord> list){

        List<String> stringList = new ArrayList<>();
        for(XsamRecord r : list){
            stringList.add(r.toString());
        }

        //assert out of order
        assertTrue(XsamReadQueries.findxlField(stringList.get(2)) < XsamReadQueries.findxlField(stringList.get(1)));

        Collections.sort(stringList, SortingMethods.createDefaultRecordStringComparator());


        assertTrue(XsamReadQueries.findxlField(stringList.get(0)) == XsamReadQueries.findxlField(stringList.get(1)));
        assertTrue(XsamReadQueries.findxlField(stringList.get(1)) == XsamReadQueries.findxlField(stringList.get(2)));
        assertTrue(XsamReadQueries.findxlField(stringList.get(2)) == XsamReadQueries.findxlField(stringList.get(3)));
        assertTrue(XsamReadQueries.findxlField(stringList.get(3)) < XsamReadQueries.findxlField(stringList.get(4)));

    }

}