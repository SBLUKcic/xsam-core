package com.sbl.sorting;

import com.sbl.io.XsamChunk;
import com.sbl.io.header.XsamSection;
import com.sbl.model.PairedXsamRecord;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class LRPairedSorterTest {



    @ParameterizedTest
    @MethodSource("com.sbl.providers.XsamPairProviders#ppeListPairProvider")
    void givenOutOfOrderListOfPPEPairs_whenSortedLandR_checkSortingCorrect(List<PairedXsamRecord> pairedXsamRecords){


        //check input from provider correct
        assertEquals(4, pairedXsamRecords.size());

        //assert out of order
        //assert 2nd and 3rd elements are the same position
        assertEquals(pairedXsamRecords.get(1).getxL(), pairedXsamRecords.get(2).getxL());
        //assert the first element is greater than the second
        assertTrue(pairedXsamRecords.get(0).getxL() > pairedXsamRecords.get(1).getxL());

        //assert the R of the 4th pair is greater than the R of the 2nd
        assertTrue(pairedXsamRecords.get(3).getxR() > pairedXsamRecords.get(1).getxR());

        Collections.sort(pairedXsamRecords, SortingMethods.createDefaultPairComparator());

        //assert 1st and 2nd elements are now equal
        assertEquals(pairedXsamRecords.get(0).getxL(), pairedXsamRecords.get(1).getxL());
        assertEquals(pairedXsamRecords.get(1).getxL(), pairedXsamRecords.get(2).getxL());

        //assert 2nd and 3rd L's are equal, but xR is greater on 3rd
        assertEquals(pairedXsamRecords.get(1).getxL(), pairedXsamRecords.get(2).getxL());
        assertTrue(pairedXsamRecords.get(1).getxR() < pairedXsamRecords.get(2).getxR());

        //assert 3rd now has the highest xL
        assertTrue(pairedXsamRecords.get(2).getxL() < pairedXsamRecords.get(3).getxL()) ;

    }

}