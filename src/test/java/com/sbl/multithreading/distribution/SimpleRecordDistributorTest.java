package com.sbl.multithreading.distribution;

import com.sbl.io.XsamChunk;
import com.sbl.io.header.XsamSection;
import com.sbl.model.XsamRecord;
import com.sbl.model.factories.RecordFactory;
import com.sbl.multithreading.threadfactories.RecordThreadFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.*;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.internal.verification.VerificationModeFactory.times;

class SimpleRecordDistributorTest {

    @Mock
    private ExecutorService executorService;

    @Mock
    private RecordThreadFactory<XsamRecord> threadFactory;

    @Mock
    private Runnable runnable;

    private RecordFactory<XsamRecord> xsamRecordFactory;

    @BeforeEach
    void setUp()  throws InterruptedException {
        MockitoAnnotations.initMocks(this);

        when(threadFactory.createRecordProcessingThread(any())).thenReturn(runnable);
        doNothing().when(executorService).shutdown();

        when(executorService.awaitTermination(1, TimeUnit.DAYS)).thenReturn(false).thenReturn(true);
        when(executorService.isTerminated()).thenReturn(true);

        xsamRecordFactory = XsamRecord::new;

    }

    @ParameterizedTest
    @MethodSource("com.sbl.providers.XsamRecordProviders#randomLongListOfRecords")
    void givenValidChunkFile_whenDistributed_checkCorrectDistribution(List<XsamRecord> records) {

        File sampleFile = new File(System.getProperty("user.dir") + File.separator + "sampleXsamList" + System.nanoTime());

        SimpleRecordDistributor<XsamRecord> distributor = new SimpleRecordDistributor<>(executorService, 10, threadFactory, xsamRecordFactory);

        int expectedDistributions = records.size()/10;


        try(PrintWriter writer = new PrintWriter(new FileWriter(sampleFile))){

            if(!sampleFile.createNewFile() && !sampleFile.exists()){
                fail();
            }

            for(XsamRecord record : records){
                writer.println(record);
            }

            writer.flush();
            writer.close();

            XsamChunk chunk = new XsamChunk(records.size(), sampleFile, 100, XsamSection.P, "mm01");

            distributor.distribute(chunk);

            verify(threadFactory, times(expectedDistributions)).createRecordProcessingThread(any());
            verify(executorService, times(expectedDistributions)).execute(any(Runnable.class));


        }catch (Exception ioe){

            fail();

        }finally {

            distributor.shutdown();

            if(!sampleFile.delete()){
                sampleFile.deleteOnExit();
            }

        }


    }

    @ParameterizedTest
    @MethodSource("com.sbl.providers.XsamRecordProviders#randomLongListOfRecords")
    void givenValidListOfChunks_whenDistributed_checkCorrectDistribution(List<XsamRecord> records) {

        File sampleFile = new File(System.getProperty("user.dir") + File.separator + "sampleXsamListMulti1" + System.nanoTime());


        SimpleRecordDistributor<XsamRecord> distributor = new SimpleRecordDistributor<>(executorService, 10, threadFactory, xsamRecordFactory);

        int expectedDistributions = records.size()/10;


        try(PrintWriter writer = new PrintWriter(new FileWriter(sampleFile))){


            if(!sampleFile.createNewFile() && !sampleFile.exists()){
                fail();
            }

            for(XsamRecord record : records){
                writer.println(record);
            }

            writer.flush();
            writer.close();

            XsamChunk chunk = new XsamChunk(records.size(), sampleFile, 100, XsamSection.P, "mm01");
            XsamChunk chunk2 = new XsamChunk(records.size(), sampleFile, 100, XsamSection.P, "mm01");

            distributor.distribute(Arrays.asList(chunk, chunk2));

            verify(threadFactory, times(2*expectedDistributions)).createRecordProcessingThread(any());
            verify(executorService, times(2*expectedDistributions)).execute(any(Runnable.class));


        }catch (Exception ioe){

            fail();

        }finally {

            distributor.shutdown();

            if(!sampleFile.delete()){
                sampleFile.deleteOnExit();
            }

        }


    }

    @ParameterizedTest
    @MethodSource("com.sbl.providers.XsamRecordProviders#randomLongListOfRecords")
    void givenDistributorCreatedUsingMoreDescriptiveConstructor_whenDistributed_checkCorrectDistribution(List<XsamRecord> records) {

        File sampleFile = new File(System.getProperty("user.dir") + File.separator + "sampleXsamList" + System.nanoTime());

        SimpleRecordDistributor<XsamRecord> distributor = new SimpleRecordDistributor<>(2, 10, 10, threadFactory, xsamRecordFactory);

        int expectedDistributions = records.size()/10;


        try(PrintWriter writer = new PrintWriter(new FileWriter(sampleFile))){

            if(!sampleFile.createNewFile() && !sampleFile.exists()){
                fail();
            }

            for(XsamRecord record : records){
                writer.println(record);
            }

            writer.flush();
            writer.close();

            XsamChunk chunk = new XsamChunk(records.size(), sampleFile, 100, XsamSection.P, "mm01");

            distributor.distribute(chunk);

            verify(threadFactory, times(expectedDistributions)).createRecordProcessingThread(any());

        }catch (Exception ioe){

            fail();

        }finally {

            distributor.shutdown();

            if(!sampleFile.delete()){
                sampleFile.deleteOnExit();
            }

        }


    }

    @ParameterizedTest
    @MethodSource("com.sbl.providers.XsamRecordProviders#randomLongListOfRecords")
    void givenValidChunkFile_whenDistributedWithUnevenSize_checkCorrectDistribution(List<XsamRecord> records) {



        File sampleFile = new File(System.getProperty("user.dir") + File.separator + "sampleXsamList" + System.nanoTime());

        SimpleRecordDistributor<XsamRecord> distributor = new SimpleRecordDistributor<>(executorService, 7, threadFactory, xsamRecordFactory);

        int expectedDistributions = 1 + records.size()/7;


        try(PrintWriter writer = new PrintWriter(new FileWriter(sampleFile))){

            if(!sampleFile.createNewFile() && !sampleFile.exists()){
                fail();
            }

            for(XsamRecord record : records){
                writer.println(record);
            }

            writer.flush();
            writer.close();

            XsamChunk chunk = new XsamChunk(records.size(), sampleFile, 100, XsamSection.P, "mm01");

            distributor.distribute(chunk);

            verify(threadFactory, times(expectedDistributions)).createRecordProcessingThread(any());
            verify(executorService, times(expectedDistributions)).execute(any(Runnable.class));



        }catch (Exception ioe){

            fail();

        }finally {

            distributor.shutdown();

            if(!sampleFile.delete()){
                sampleFile.deleteOnExit();
            }

        }


    }
}