package com.sbl.multithreading.distribution;

import com.sbl.io.XsamChunk;
import com.sbl.io.header.XsamSection;
import com.sbl.model.PairedXsamRecord;
import com.sbl.model.factories.PairedRecordFactory;
import com.sbl.multithreading.threadfactories.RecordThreadFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verify;
import static org.mockito.internal.verification.VerificationModeFactory.times;

class SimplePairedRecordDistributorTest {

    @Mock
    private ExecutorService executorService;

    @Mock
    private RecordThreadFactory<PairedXsamRecord> threadFactory;

    @Mock
    private Runnable runnable;

    private PairedRecordFactory<PairedXsamRecord> recordFactory;

    @BeforeEach
    void setUp()  throws InterruptedException {
        MockitoAnnotations.initMocks(this);

        when(threadFactory.createRecordProcessingThread(any())).thenReturn(runnable);
        doNothing().when(executorService).shutdown();

        when(executorService.awaitTermination(1, TimeUnit.DAYS)).thenReturn(false).thenReturn(true);
        when(executorService.isTerminated()).thenReturn(true);

        recordFactory = PairedXsamRecord::new;
    }

    @ParameterizedTest
    @MethodSource("com.sbl.providers.XsamPairProviders#pairedXsamRecordProvider")
    void givenValidChunkFile_whenDistributed_checkCorrectDistribution(List<PairedXsamRecord> records) {

        File sampleFile = new File(System.getProperty("user.dir") + File.separator + "sampleXsamList" + System.nanoTime());

        SimplePairedRecordDistributor distributor = new SimplePairedRecordDistributor<>(executorService, 10, threadFactory, recordFactory);

        int expectedDistributions = records.size()/10;


        try(PrintWriter writer = new PrintWriter(new FileWriter(sampleFile))){

            if(!sampleFile.createNewFile() && !sampleFile.exists()){
                fail();
            }

            for(PairedXsamRecord record : records){
                writer.println(record.toString());
            }

            writer.flush();
            writer.close();

            XsamChunk chunk = new XsamChunk(records.size(), sampleFile, 100, XsamSection.P, "mm01");

            distributor.distribute(chunk);

            verify(threadFactory, times(expectedDistributions)).createRecordProcessingThread(any());
            verify(executorService, times(expectedDistributions)).execute(any(Runnable.class));


        }catch (Exception ioe){

            fail();

        }finally {

            distributor.shutdown();

            if(!sampleFile.delete()){
                sampleFile.deleteOnExit();
            }

        }


    }

    @ParameterizedTest
    @MethodSource("com.sbl.providers.XsamPairProviders#pairedXsamRecordProvider")
    void givenValidMultipleChunks_whenDistributed_checkCorrectDistribution(List<PairedXsamRecord> records) {

        File sampleFile = new File(System.getProperty("user.dir") + File.separator + "sampleXsamList" + System.nanoTime());

        SimplePairedRecordDistributor distributor = new SimplePairedRecordDistributor<>(executorService, 10, threadFactory, recordFactory);

        int expectedDistributions = 2*(records.size()/10);


        try(PrintWriter writer = new PrintWriter(new FileWriter(sampleFile))){

            if(!sampleFile.createNewFile() && !sampleFile.exists()){
                fail();
            }

            for(PairedXsamRecord record : records){
                writer.println(record.toString());
            }

            writer.flush();
            writer.close();

            XsamChunk chunk = new XsamChunk(records.size(), sampleFile, 100, XsamSection.P, "mm01");
            XsamChunk chunk2 = new XsamChunk(records.size(), sampleFile, 100, XsamSection.P, "mm01");

            distributor.distribute(Arrays.asList(chunk, chunk2));

            verify(threadFactory, times(expectedDistributions)).createRecordProcessingThread(any());
            verify(executorService, times(expectedDistributions)).execute(any(Runnable.class));


        }catch (Exception ioe){

            fail();

        }finally {

            distributor.shutdown();

            if(!sampleFile.delete()){
                sampleFile.deleteOnExit();
            }

        }


    }

    @ParameterizedTest
    @MethodSource("com.sbl.providers.XsamPairProviders#pairedXsamRecordProvider")
    void givenConstructorWithMoreArgs_whenDistributed_checkCorrectDistribution(List<PairedXsamRecord> records) {

        File sampleFile = new File(System.getProperty("user.dir") + File.separator + "sampleXsamList" + System.nanoTime());

        SimplePairedRecordDistributor distributor = new SimplePairedRecordDistributor<>(10,10, 10, threadFactory, recordFactory);

        int expectedDistributions = records.size()/10;


        try(PrintWriter writer = new PrintWriter(new FileWriter(sampleFile))){

            if(!sampleFile.createNewFile() && !sampleFile.exists()){
                fail();
            }

            for(PairedXsamRecord record : records){
                writer.println(record.toString());
            }

            writer.flush();
            writer.close();

            XsamChunk chunk = new XsamChunk(records.size(), sampleFile, 100, XsamSection.P, "mm01");

            distributor.distribute(chunk);

            verify(threadFactory, times(expectedDistributions)).createRecordProcessingThread(any());

        }catch (Exception ioe){

            fail();

        }finally {

            distributor.shutdown();

            if(!sampleFile.delete()){
                sampleFile.deleteOnExit();
            }

        }


    }

}