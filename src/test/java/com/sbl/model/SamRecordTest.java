package com.sbl.model;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class SamRecordTest {
  
  private SamRecord record;

  @Test
  void givenValidSamString_whenElementsParsed_checkCorrectStringsAndIntsReturned() {

    record = new SamRecord("SBL_XSNP077_ID:10_BC1:CTATTTCAG\t99\tSAT_234bp_Repeat_Mus_musculus\t3542\t2\t1S38M\t=\t3921\t415\tNCTCATTTTCCGTGATTTTCAGTTTACTCGCCATATTCC\t~@FHIFHHHEHHHIIIIIEHHFIHIIHHIIIHIIHHIII\tPG:Z:novoalign\tAS:i:36\tUQ:i:36\tNM:i:1\tMD:Z:24T13\tPQ:i:157\tSM:i:0\tAM:i:0\tZS:Z:R\tNH:i:2\tHI:i:1\tIH:i:1");

    assertEquals("SBL_XSNP077_ID:10_BC1:CTATTTCAG", record.getId());
    assertEquals(99, record.getFlag());
    assertEquals("SAT_234bp_Repeat_Mus_musculus", record.getReferenceName());
    assertEquals(3542, record.getPos());
    assertEquals(2, record.getMappingQuality());
    assertEquals("1S38M", record.getCigar());
    assertEquals("=", record.getMateReferenceName());
    assertEquals(3921, record.getMatePosition());
    assertEquals(415, record.getTemplateLength());
    assertEquals("NCTCATTTTCCGTGATTTTCAGTTTACTCGCCATATTCC", record.getSequence());
    assertEquals("~@FHIFHHHEHHHIIIIIEHHFIHIIHHIIIHIIHHIII", record.getQuality());
    assertTrue(record.isRepeat());
    assertTrue(record.isMapped());
    assertEquals("PG:Z:novoalign\tAS:i:36\tUQ:i:36\tNM:i:1\tMD:Z:24T13\tPQ:i:157\tSM:i:0\tAM:i:0\tZS:Z:R\tNH:i:2\tHI:i:1\tIH:i:1", String.join("\t", record.getVariableTerms()));

  }

  @Test
  void whenEqualityTested_checkCorrect() {

    SamRecord record1 = new SamRecord("SBL_XSNP077_ID:10_BC1:CTATTTCAG\t99\tSAT_234bp_Repeat_Mus_musculus\t3542\t2\t1S38M\t=\t3921\t415\tNCTCATTTTCCGTGATTTTCAGTTTACTCGCCATATTCC\t~@FHIFHHHEHHHIIIIIEHHFIHIIHHIIIHIIHHIII\tPG:Z:novoalign\tAS:i:36\tUQ:i:36\tNM:i:1\tMD:Z:24T13\tPQ:i:157\tSM:i:0\tAM:i:0\tZS:Z:R\tNH:i:2\tHI:i:1\tIH:i:1");
    SamRecord record2 = new SamRecord("SBL_XSNP077_ID:10_BC1:CTATTTCAG\t99\tSAT_234bp_Repeat_Mus_musculus\t3542\t2\t1S38M\t=\t3921\t415\tNCTCATTTTCCGTGATTTTCAGTTTACTCGCCATATTCC\t~@FHIFHHHEHHHIIIIIEHHFIHIIHHIIIHIIHHIII\tPG:Z:novoalign\tAS:i:36\tUQ:i:36\tNM:i:1\tMD:Z:24T13\tPQ:i:157\tSM:i:0\tAM:i:0\tZS:Z:R\tNH:i:2\tHI:i:1\tIH:i:1");

    assertEquals(record1, record2);

  }

  @Test
  void whenConvertedToFastQCheckCorrect() {

    SamRecord record1 = new SamRecord("SBL_XSNP077_ID:10_BC1:CTATTTCAG\t99\tSAT_234bp_Repeat_Mus_musculus\t3542\t2\t1S38M\t=\t3921\t415\tNCTCATTTTCCGTGATTTTCAGTTTACTCGCCATATTCC\t~@FHIFHHHEHHHIIIIIEHHFIHIIHHIIIHIIHHIII\tPG:Z:novoalign\tAS:i:36\tUQ:i:36\tNM:i:1\tMD:Z:24T13\tPQ:i:157\tSM:i:0\tAM:i:0\tZS:Z:R\tNH:i:2\tHI:i:1\tIH:i:1");

    FastQRecord fastq = record1.convertToFastQ(1);

    assertEquals("@SBL_XSNP077_ID:10_BC1:CTATTTCAG/1", fastq.getId());
    assertEquals("NCTCATTTTCCGTGATTTTCAGTTTACTCGCCATATTCC", fastq.getSequence());
    assertEquals("~@FHIFHHHEHHHIIIIIEHHFIHIIHHIIIHIIHHIII", fastq.getQuality());


  }

  @Test
  void whenHashCodesTested_checkEqual() {

    SamRecord record1 = new SamRecord("SBL_XSNP077_ID:10_BC1:CTATTTCAG\t99\tSAT_234bp_Repeat_Mus_musculus\t3542\t2\t1S38M\t=\t3921\t415\tNCTCATTTTCCGTGATTTTCAGTTTACTCGCCATATTCC\t~@FHIFHHHEHHHIIIIIEHHFIHIIHHIIIHIIHHIII\tPG:Z:novoalign\tAS:i:36\tUQ:i:36\tNM:i:1\tMD:Z:24T13\tPQ:i:157\tSM:i:0\tAM:i:0\tZS:Z:R\tNH:i:2\tHI:i:1\tIH:i:1");
    SamRecord record2 = new SamRecord("SBL_XSNP077_ID:10_BC1:CTATTTCAG\t99\tSAT_234bp_Repeat_Mus_musculus\t3542\t2\t1S38M\t=\t3921\t415\tNCTCATTTTCCGTGATTTTCAGTTTACTCGCCATATTCC\t~@FHIFHHHEHHHIIIIIEHHFIHIIHHIIIHIIHHIII\tPG:Z:novoalign\tAS:i:36\tUQ:i:36\tNM:i:1\tMD:Z:24T13\tPQ:i:157\tSM:i:0\tAM:i:0\tZS:Z:R\tNH:i:2\tHI:i:1\tIH:i:1");

    assertEquals(record1.hashCode(), record2.hashCode());

  }
}