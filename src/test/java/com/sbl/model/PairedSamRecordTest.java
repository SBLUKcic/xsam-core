package com.sbl.model;

import com.sbl.model.xsamtypes.PairedXsamRecordMappingCombination;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PairedSamRecordTest {

  private SamRecord qcRecord1 = new SamRecord("SBL_XSNP077_ID:184296_BC1:CTATTTCAG_\t99\tmm02\t188710023\t70\t2S39M\t*\t188710074\t90\tNNGAATAATAATAGTGGGGGATCCACTAGGGCTGTGGCTCC\t~~IIIIIIIIIIIIIIIIIIIIIIIIIIHHHIIIIIIIII\tPG:Z:novoalign\tAS:i:12\tUQ:i:12\tNM:i:0\tMD:Z:39\tPQ:i:20\tSM:i:70\tAM:i:70\tZS:Z:QC");
  private SamRecord nmRecord2 = new SamRecord("SBL_XSNP077_ID:184296_BC1:CTATTTCAG_\t147\t*\t188710074\t70\t39M\tmm01\t188710023\t-90\tTCCTTGGTCAGCTGTGTTTTAACTTGAGGAGGCCTTCCN\tIHIIIIIIIIIIIIIIIIIHIHIIIIIIIIIIIIIIII~\tPG:Z:novoalign\tAS:i:6\tUQ:i:6\tNM:i:1\tMD:Z:38A0\tPQ:i:20\tSM:i:70\tAM:i:70\tZS:Z:NM");

  private PairedSamRecord xxPair = new PairedSamRecord(qcRecord1, nmRecord2);

  @Test
  void givenNMAndQCSamPair_whenMappingTypeChecked_checkXXReturned() {

    assertEquals(PairedXsamRecordMappingCombination.XX, xxPair.getMappingCombination());

  }

  @Test
  void givenValidSamPair_whenConvertedToFastQ_checkCorrect() {

    FastQRecord one = xxPair.getFastQRecord1();

    assertEquals("@SBL_XSNP077_ID:184296_BC1:CTATTTCAG_/1", one.getId());
    assertEquals("NNGAATAATAATAGTGGGGGATCCACTAGGGCTGTGGCTCC", one.getSequence());
    assertEquals("~~IIIIIIIIIIIIIIIIIIIIIIIIIIHHHIIIIIIIII", one.getQuality());

    FastQRecord two = xxPair.getFastQRecord2();

    assertEquals("@SBL_XSNP077_ID:184296_BC1:CTATTTCAG_/2", two.getId());
    assertEquals("TCCTTGGTCAGCTGTGTTTTAACTTGAGGAGGCCTTCCN", two.getSequence());
    assertEquals("IHIIIIIIIIIIIIIIIIIHIHIIIIIIIIIIIIIIII~", two.getQuality());

  }
}