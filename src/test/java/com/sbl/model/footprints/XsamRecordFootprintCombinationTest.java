package com.sbl.model.footprints;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class XsamRecordFootprintCombinationTest {

    @Test
    void givenValidFootprintCombination_whenChecked_checkValidListReturned() {

        Optional<List<XsamRecordFootprint>> opt = XsamRecordFootprintCombination.isValidsamRecordFootprintCombination("Ab");

        assertTrue(opt.isPresent());
        assertEquals(2, opt.get().size());
    }

    @Test
    void givenInValidFootprintCombination_whenChecked_checkOptionalIsEmpty() {

        Optional<List<XsamRecordFootprint>> opt = XsamRecordFootprintCombination.isValidsamRecordFootprintCombination("NOT CORRECT");

        assertFalse(opt.isPresent());

    }
}

