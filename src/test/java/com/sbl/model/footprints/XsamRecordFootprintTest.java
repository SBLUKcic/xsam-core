package com.sbl.model.footprints;

import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class XsamRecordFootprintTest {

    @Test
    void givenValidFootprint_whenChecked_checkOptionalContainsFootprint() {

        Optional<XsamRecordFootprint> footprintOptional = XsamRecordFootprint.isValidXsamRecordFootprint("Lb");

        assertTrue(footprintOptional.isPresent());
        assertEquals(XsamRecordFootprint.Lb, footprintOptional.get());

    }

    @Test
    void givenWrongFootprint_checkOptionalEmpty() {

        Optional<XsamRecordFootprint> footprintOptionalEmpty = XsamRecordFootprint.isValidXsamRecordFootprint("Left Footprint");

        assertFalse(footprintOptionalEmpty.isPresent());
    }
}