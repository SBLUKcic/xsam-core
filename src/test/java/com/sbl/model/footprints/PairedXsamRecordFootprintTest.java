package com.sbl.model.footprints;

import com.sbl.model.PairedXsamRecord;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class PairedXsamRecordFootprintTest {

    @Test
    void givenValidFootprint_whenChecked_checkOptionalContainsFootprint() {

        Optional<PairedXsamRecordFootprint> footprintOptional = PairedXsamRecordFootprint.isValidXsamRecordFootprint("LE");

        assertTrue(footprintOptional.isPresent());
        assertEquals(PairedXsamRecordFootprint.LE, footprintOptional.get());

    }

    @Test
    void givenWrongFootprint_checkOptionalEmpty() {

        Optional<PairedXsamRecordFootprint> footprintOptionalEmpty = PairedXsamRecordFootprint.isValidXsamRecordFootprint("Left Footprint");

        assertFalse(footprintOptionalEmpty.isPresent());
    }

}