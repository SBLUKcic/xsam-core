package com.sbl.model.footprints;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class PairedXsamRecordFootprintCombinationTest {

    @Test
    void givenValidPairedFootprintCombination_whenChecked_checkValidListReturned() {

        Optional<List<PairedXsamRecordFootprint>> opt = PairedXsamRecordFootprintCombination.isValidPairedXsamRecordFootprintCombination("AE");

        assertTrue(opt.isPresent());
        assertEquals(2, opt.get().size());
    }

    @Test
    void givenInValidPairedFootprintCombination_whenChecked_checkOptionalIsEmpty() {

        Optional<List<PairedXsamRecordFootprint>> opt = PairedXsamRecordFootprintCombination.isValidPairedXsamRecordFootprintCombination("I AM NOT A FOOTPRINT");

        assertFalse(opt.isPresent());
    }
}