package com.sbl.model;

import com.sbl.model.xsamtypes.PairedXsamRecordMappingCombination;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;

import static org.junit.jupiter.api.Assertions.*;

class PairedXsamRecordTest {

  @ParameterizedTest
  @CsvSource("SBL_XSNP077_ID:184296_BC1:CTATTTCAG_\t99\tmm01\t188710023\t70\t2S39M\t=\t188710074\t90\tNNGAATAATAATAGTGGGGGATCCACTAGGGCTGTGGCTCC\t~~IIIIIIIIIIIIIIIIIIIIIIIIIIHHHIIIIIIIIII\txl:i:188710023\txr:i:188710061\txs:i:39\txd:A:f\txm:A:u\txa:A:\"\"\txL:i:188710023\txR:i:188710112\txS:i:90\txW:i:12\txP:i:0\txQ:i:0\txC:A:\"\"\txD:A:\"\"\tPG:Z:novoalign\tAS:i:12\tUQ:i:12\tNM:i:0\tMD:Z:39\tPQ:i:20\tSM:i:70\tAM:i:70, SBL_XSNP077_ID:184296_BC1:CTATTTCAG_\t147\tmm01\t188710074\t70\t39M\t=\t188710023\t-90\tTCCTTGGTCAGCTGTGTTTTAACTTGAGGAGGCCTTCCN\tIHIIIIIIIIIIIIIIIIIHIHIIIIIIIIIIIIIIII~\txl:i:188710074\txr:i:188710112\txs:i:39\txd:A:r\txm:A:u\txa:A:\"\"txL:i:188710023\txR:i:188710112\txS:i:90\txW:i:12\txP:i:0\txQ:i:0\txC:A:\"\"\txD:A:\"\"\tPG:Z:novoalign\tAS:i:6\tUQ:i:6\tNM:i:1\tMD:Z:38A0\tPQ:i:20\tSM:i:70\tAM:i:70")
  void givenValidXsamString_whenPairedElementsChecked_checkCorrectValuesReturned(String record1, String record2) {

    PairedXsamRecord pair = new PairedXsamRecord(record1, record2);

    assertAll(
        () ->  assertEquals(188710023, pair.getxL()),
        () -> assertEquals(188710112, pair.getxR()),
        () -> assertEquals(90, pair.getxS()),
        () -> assertEquals(12, pair.getxW()),
        () -> assertEquals(0, pair.getxQ()),
        () -> assertEquals(0, pair.getxP()),
        () -> assertEquals("\"\"", pair.getxC()),
        () -> assertEquals("\"\"", pair.getxD())
    );
  }

  @ParameterizedTest
  @MethodSource("com.sbl.providers.XsamPairProviders#ppePairProvider")
  void givenValidXsamPairOnSameRName_whenReferenceMatchingChecked_checkTrue(PairedXsamRecord pair) {

    assertTrue(pair.areReferenceNamesMatching());

  }

  @ParameterizedTest
  @MethodSource("com.sbl.providers.XsamPairProviders#ppePairProvider")
  void given2CopiesOfSamePair_whenEqualityCheck_checkCorrect(PairedXsamRecord pair) {

    PairedXsamRecord record1 = new PairedXsamRecord(pair.getRecord1(), pair.getRecord2());
    PairedXsamRecord record2 = new PairedXsamRecord(pair.getRecord1(), pair.getRecord2());

    assertEquals(record1, record2);

  }

  @ParameterizedTest
  @MethodSource("com.sbl.providers.XsamPairProviders#ppdPairProvider")
  void givenValidXsamPairOnDifferenceRNames_whenReferenceMatchingChecked_checkFalse(PairedXsamRecord pair) {

    assertFalse(pair.areReferenceNamesMatching());

  }

  @Test
  void givenInvalidIDs_whenPairCreated_checkExceptionThrown() {

    assertThrows(IllegalArgumentException.class, () -> new PairedXsamRecord(new XsamRecord("asdsa\txl:i:1\txL:i:100"), new XsamRecord("asdsad\txl:i:1\txL:i:100")),"sadsad");

  }

  @Test
  void givenInvalidIDsButPairedFieldsMissing_whenPairCreated_checkExceptionThrown() {


    assertThrows(IllegalArgumentException.class, () -> new PairedXsamRecord(new XsamRecord("asdsa\txl:i:1\t"), new XsamRecord("asdsa\txl:i:1\t")),"Paired Xsam fields are missing");

  }

  @ParameterizedTest
  @MethodSource("com.sbl.providers.XsamPairProviders#ppePairProvider")
  void givenRecord1Null_whenPairCreated_checkExceptionThrown(PairedXsamRecord pair) {

    assertThrows(IllegalArgumentException.class, () -> new PairedXsamRecord(null, pair.getRecord2()),"Record 1 cannot be null");

  }

  @ParameterizedTest
  @MethodSource("com.sbl.providers.XsamPairProviders#ppePairProvider")
  void givenRecord2Null_whenPairCreated_checkExceptionThrown(PairedXsamRecord pair) {

    assertThrows(IllegalArgumentException.class, () -> new PairedXsamRecord(pair.getRecord1(), null),"Record 1 cannot be null");

  }

  @ParameterizedTest
  @MethodSource("com.sbl.providers.XsamPairProviders#xxPairProvider")
  void givenOneRecordIsQCAndAnotherIsNM_whenNonMappedStatusChecked_checkNonMapped(PairedXsamRecord xxPair) {

    assertEquals(PairedXsamRecordMappingCombination.XX, xxPair.getMappingCombination());

  }
}