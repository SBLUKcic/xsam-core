package com.sbl.model.xsamstrands;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PairedXsamRecordStrandCombinationTest {

    @Test
    void givenValidStrandCombo_whenQueried_checkCorrect() {

        assertEquals(7, PairedXsamRecordStrandCombination.aaa.getSubTypes().size());
        assertEquals(PairedXsamRecordStrand.ff, PairedXsamRecordStrandCombination.ff.getSubTypes().get(0));
    }
}