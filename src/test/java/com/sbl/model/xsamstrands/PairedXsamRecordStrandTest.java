package com.sbl.model.xsamstrands;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PairedXsamRecordStrandTest {

    @Test
    void givenRecordStrandEnum_whenQueired_checkCorrect() {
        assertEquals("ff", PairedXsamRecordStrand.ff.getAcceptedStrands().get(0));
    }
}