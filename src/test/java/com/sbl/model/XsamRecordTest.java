package com.sbl.model;


import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import static org.junit.jupiter.api.Assertions.*;

class XsamRecordTest {

  String samRec = "SBL_XSNP077_ID:10_BC1:CTATTTCAG\t99\tSAT_234bp_Repeat_Mus_musculus\t3542\t2\t1S38M\t=\t3921\t415\tNCTCATTTTCCGTGATTTTCAGTTTACTCGCCATATTCC\t~@FHIFHHHEHHHIIIIIEHHFIHIIHHIIIHIIHHIII\tPG:Z:novoalign\tAS:i:36\tUQ:i:36\tNM:i:1\tMD:Z:24T13\tPQ:i:157\tSM:i:0\tAM:i:0\tZS:Z:R\tNH:i:2\tHI:i:1\tIH:i:1";


  @Test
  void givenReadOneSingleIsNull_whenXsamRecordCreated_checkIllegalArgumentExceptionIsThrown() {

    assertThrows(IllegalArgumentException.class, () -> new XsamRecord(null), "Input read cannot be null");

  }

  private XsamRecord record;

  @Test
  void whenSamRecordPassedToXsamObject_checkIllegalArgExcepThrown() {

    assertThrows(IllegalArgumentException.class, () -> new XsamRecord(samRec), "Input read must not be a .sam record, expected Xsam fields - xl:i:...");

  }

  @Test
  void givenValidSamString_whenElementsParsed_checkCorrectStringsAndIntsReturned() {

    record = new XsamRecord("SBL_XSNP077_ID:184296_BC1:CTATTTCAG_\t147\tmm01\t188710074\t70\t39M\t=\t188710023\t-90\tTCCTTGGTCAGCTGTGTTTTAACTTGAGGAGGCCTTCCN\tIHIIIIIIIIIIIIIIIIIHIHIIIIIIIIIIIIIIII~\txl:i:188710074\txr:i:188710112\txs:i:39\txd:A:r\txm:A:u\txa:A:\"\"\txL:i:188710023\txR:i:188710112\txS:i:90\txW:i:12\txP:i:0\txQ:i:0\txC:A:\"\"\txD:A:\"\"\tPG:Z:novoalign\tAS:i:6\tUQ:i:6\tNM:i:1\tMD:Z:38A0\tPQ:i:20\tSM:i:70\tAM:i:70");

    assertEquals("SBL_XSNP077_ID:184296_BC1:CTATTTCAG_", record.getId());
    assertEquals(147, record.getFlag());
    assertEquals("mm01", record.getReferenceName());
    assertEquals(188710074, record.getPos());
    assertEquals(70, record.getMappingQuality());
    assertEquals("39M", record.getCigar());
    assertEquals("=", record.getMateReferenceName());
    assertEquals(188710023, record.getMatePosition());
    assertEquals(-90, record.getTemplateLength());
    assertEquals("TCCTTGGTCAGCTGTGTTTTAACTTGAGGAGGCCTTCCN", record.getSequence());
    assertEquals("IHIIIIIIIIIIIIIIIIIHIHIIIIIIIIIIIIIIII~", record.getQuality());
    assertFalse(record.isRepeat());
    assertTrue(record.isMapped());

    assertEquals("PG:Z:novoalign\tAS:i:6\tUQ:i:6\tNM:i:1\tMD:Z:38A0\tPQ:i:20\tSM:i:70\tAM:i:70", String.join("\t", record.getVariableTerms()));

  }

  @Test
  void givenValidXSamString_whenConversionToSamCalled_checkConversionCorrect() {

    record = new XsamRecord("SBL_XSNP077_ID:184296_BC1:CTATTTCAG_\t147\tmm01\t188710074\t70\t39M\t=\t188710023\t-90\tTCCTTGGTCAGCTGTGTTTTAACTTGAGGAGGCCTTCCN\tIHIIIIIIIIIIIIIIIIIHIHIIIIIIIIIIIIIIII~\txl:i:188710074\txr:i:188710112\txs:i:39\txd:A:r\txm:A:u\txa:A:\"\"\t\txL:i:188710023\txR:i:188710112\txS:i:90\txW:i:12\txP:i:0\txQ:i:0\txC:A:\"\"\txD:A:\"\"\tPG:Z:novoalign\tAS:i:6\tUQ:i:6\tNM:i:1\tMD:Z:38A0\tPQ:i:20\tSM:i:70\tAM:i:70");

    assertEquals("SBL_XSNP077_ID:184296_BC1:CTATTTCAG_\t147\tmm01\t188710074\t70\t39M\t=\t188710023\t-90\tTCCTTGGTCAGCTGTGTTTTAACTTGAGGAGGCCTTCCN\tIHIIIIIIIIIIIIIIIIIHIHIIIIIIIIIIIIIIII~\tPG:Z:novoalign\tAS:i:6\tUQ:i:6\tNM:i:1\tMD:Z:38A0\tPQ:i:20\tSM:i:70\tAM:i:70", record.convertToSam().toString());

  }

  @Test
  void givenValidXsamString_whenCommonElementsChecked_checkCorrectValuesReturned() {

    record = new XsamRecord("SBL_XSNP077_ID:184296_BC1:CTATTTCAG_\t147\tmm01\t188710074\t70\t39M\t=\t188710023\t-90\tTCCTTGGTCAGCTGTGTTTTAACTTGAGGAGGCCTTCCN\tIHIIIIIIIIIIIIIIIIIHIHIIIIIIIIIIIIIIII~\txl:i:188710074\txr:i:188710112\txs:i:39\txd:A:r\txm:A:u\txa:A:\"\"\t\txL:i:188710023\txR:i:188710112\txS:i:90\txW:i:12\txP:i:0\txQ:i:0\txC:A:\"\"\txD:A:\"\"\tPG:Z:novoalign\tAS:i:6\tUQ:i:6\tNM:i:1\tMD:Z:38A0\tPQ:i:20\tSM:i:70\tAM:i:70");

    assertEquals(188710074, record.getXl());
    assertEquals(188710112, record.getXr());
    assertEquals(39, record.getXs());
    assertEquals('r', record.getXd());
    assertEquals('u', record.getXm());
    assertEquals("", record.getXa());
  }

  @Test
  void givenQCFailedXsamRead_whenQueried_checkTrueReturned() {

    XsamRecord qcRecord1 = new XsamRecord("SBL_XSNP077_ID:184296_BC1:CTATTTCAG_\t99\tmm02\t188710023\t70\t2S39M\t*\t188710074\t90\tNNGAATAATAATAGTGGGGGATCCACTAGGGCTGTGGCTCC\t~~IIIIIIIIIIIIIIIIIIIIIIIIIIHHHIIIIIIIIII\txl:i:188710023\txr:i:188710061\txs:i:39\txd:A:f\txm:A:u\txa:A:\"\"\t\txL:i:188710023\txR:i:188710112\txS:i:90\txW:i:12\txP:i:0\txQ:i:0\txC:A:\"\"\txD:A:\"\"\tPG:Z:novoalign\tAS:i:12\tUQ:i:12\tNM:i:0\tMD:Z:39\tPQ:i:20\tSM:i:70\tAM:i:70\tZS:Z:QC");

    assertTrue(qcRecord1.isQualityFailed());

  }

  @ParameterizedTest
  @MethodSource("com.sbl.providers.XsamPairProviders#ppePairProvider")
  void givenNoFlankExtensionFields_whenOptionalChecked_checkEmpty(PairedXsamRecord pair) {

      assertFalse(pair.getxLSeq().isPresent());
      assertFalse(pair.getxRSeq().isPresent());


  }

  @ParameterizedTest
  @MethodSource("com.sbl.providers.XsamPairProviders#flankExtPairProvider")
  void givenlankExtensionFields_whenOptionalChecked_checkValidStrings(PairedXsamRecord pair) {

    assertTrue(pair.getxLSeq().isPresent());
    assertTrue(pair.getxRSeq().isPresent());
  }
}