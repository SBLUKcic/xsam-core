package com.sbl.utils;

import com.sbl.model.SamRecord;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class XsamReadQueriesTest {

  String read1 = "SBL_XSMJR275_ID:1108283_BC1:ACACTTCAG_\t99\tmm07\t59176172\t30\t2S39M\t=\t59176371\t235\tNNACAGGAAACCCACTCAGATCTACCTCTCATGGGCCAAAT\t~~IIIIIIIIIHGIIIHIIIIIIIIIIIHIIIIIIIIIIII\txl:i:59176172\txr:i:59176210\txs:i:39\txd:A:f\txm:A:u\txa:A:\"\"\t\txL:i:59176172\txR:i:59176406\txLseq:i:AGCT\txRseq:i:TGTG\txS:i:235\txW:i:160\txP:i:0\txQ:i:0\txC:A:\"\"\txD:A:\"\"\tPG:Z:novoalign\tAS:i:12\tUQ:i:12\tNM:i:0\tMD:Z:39\tPQ:i:49\tSM:i:23\tAM:i:0\t";
  String read2 = "SBL_XSMJR275_ID:1108283_BC1:ACACTTCAG_\t147\tmm07\t59176371\t30\t36M3S\t=\t59176172\t-235\tGTGTGTGTGTGTGAGTGTGTGTGTGTGTGTGTGTGTATN\tIHIIIIIIIIIHG?HCIIIIIIIHIIIIIIIIIIIIII~\txl:i:59176371\txr:i:59176406\txs:i:36\txd:A:r\txm:A:u\txa:A:\"\"\t\txL:i:59176172\txR:i:59176406\txLseq:i:AGCT\txRseq:i:TGTG\txS:i:235\txW:i:160\txP:i:0\txQ:i:0\txC:A:\"\"\txD:A:\"\"\tPG:Z:novoalign\tAS:i:36\tUQ:i:36\tNM:i:0\tMD:Z:36\tPQ:i:49\tSM:i:0\tAM:i:0";

  String exampleBCR = "SBL_XSMJR275_ID:1108283_:BCR1:ATGTGA:BCR2:TGTGCA\t147\tmm07\t59176371\t30\t36M3S\t=\t59176172\t-235\tGTGTGTGTGTGTGAGTGTGTGTGTGTGTGTGTGTGTATN\tIHIIIIIIIIIHG?HCIIIIIIIHIIIIIIIIIIIIII~\txl:i:59176371\txr:i:59176406\txs:i:36\txd:A:r\txm:A:u\txa:A:\"\"\t\txL:i:59176172\txR:i:59176406\txLseq:i:AGCT\txRseq:i:TGTG\txS:i:235\txW:i:160\txP:i:0\txQ:i:0\txC:A:\"\"\txD:A:\"\"\tPG:Z:novoalign\tAS:i:36\tUQ:i:36\tNM:i:0\tMD:Z:36\tPQ:i:49\tSM:i:0\tAM:i:0";

  String samRead1 = "SBL_XSMJR275_ID:1108283_BC1:ACACTTCAG_\t147\tmm07\t59176371\t30\t36M3S\t=\t59176172\t-235\tGTGTGTGTGTGTGAGTGTGTGTGTGTGTGTGTGTGTATN\tIHIIIIIIIIIHG?HCIIIIIIIHIIIIIIIIIIIIII~\tPG:Z:novoalign\tAS:i:36\tUQ:i:36\tNM:i:0\tMD:Z:36\tPQ:i:49\tSM:i:0\tAM:i:0";



  @Test
  void givenValidReadWithSBLID_whenLibraryNameSearched_checkCorrectLibraryReturned() {

    String lib = XsamReadQueries.findLibraryName(read1);

    assertEquals("XSMJR275", lib);
  }

  @Test
  void givenValidReadWithInvalidLibraryName_whenLibraryNameSearched_checkWholeIDReturned() {

    String lib = XsamReadQueries.findLibraryName("SBLXSMJR275D:1108283BC1:ACACTTCAG_");

    assertEquals("SBLXSMJR275D:1108283BC1:ACACTTCAG_", lib);
  }

  @Test
  void givenValidSamRead_whenIndexesSearchedFor_checkCorrect() {

    int[] indexes = XsamReadQueries.findTabIndexes(samRead1);

    assertEquals(18, indexes.length);
    assertEquals(indexes[0], 38);

  }

  @Test
  void givenOptionalField_whenChecked_checkCorrect() {

    assertTrue(SamRecord.OptionalField.isFieldOptional("NM:i:0"));
    assertFalse(SamRecord.OptionalField.isFieldOptional("I am not a field"));

  }

  @Test
  void givenValidReadWithoutSBLID_whenLibraryNameSearchedFromID_checkFullIDReturned() {

    String lib = XsamReadQueries.findLibraryName("@23425:213123:213123123:213213123");

    assertEquals("", lib);

  }

  @Test
  void givenValidReadWithoutSBLID_whenLibraryNameSearchedFromFullRead_checkFullIDReturned() {

    String lib = XsamReadQueries.findLibraryName("@23425:213123:213123123:213213123\tTheRestOfTHeString");

    assertEquals("", lib);

  }



  @Test
  void givenRead_whenIDReturned_checkIDCorrect() {

    String id = XsamReadQueries.findID(exampleBCR);

    assertEquals("SBL_XSMJR275_ID:1108283_:BCR1:ATGTGA:BCR2:TGTGCA", id);

  }

  @Test
  void givenRead_whenFlagReturned_checkCorrect() {

    String flag = XsamReadQueries.findFlag(exampleBCR);

    assertEquals("147", flag);

  }

  @Test
  void givenRead_whenPositionReturned_checkCorrect() {

    String pos = XsamReadQueries.findPosition(exampleBCR);

    assertEquals("59176371", pos);

  }

  @Test
  void givenRead_whenMappingQualityReturned_checkCorrect() {

    String mapQual = XsamReadQueries.findMappingQuality(exampleBCR);

    assertEquals("30", mapQual);

  }

  @Test
  void givenRead_whenMateReferenceNameReturned_checkCorrect() {

    String mateRef = XsamReadQueries.findMateReferenceName(exampleBCR);

    assertEquals("=", mateRef);

  }

  @Test
  void givenRead_whenMatePosReturned_checkCorrect() {

    String matePos = XsamReadQueries.findMatePosition(exampleBCR);

    assertEquals("59176172", matePos);

  }

  @Test
  void givenRead_whenTemplateLengthReturned_checkCorrect() {

    String templateLength = XsamReadQueries.findTemplateLength(exampleBCR);

    assertEquals("-235", templateLength);

  }

  @Test
  void givenRead_whenPhredReturned_checkPhredCorrect() {

    String phred = XsamReadQueries.findPhred(exampleBCR);

    assertEquals("IHIIIIIIIIIHG?HCIIIIIIIHIIIIIIIIIIIIII~", phred);

  }



  @Test
  void givenValidSamRecord_whenVariableRegionSearchedFor_checkCorrectStringReturned() {

    String sam = "SBL_XSNP077_ID:10_BC1:CTATTTCAG\t99\tSAT_234bp_Repeat_Mus_musculus\t3542\t2\t1S38M\t=\t3921\t415\tNCTCATTTTCCGTGATTTTCAGTTTACTCGCCATATTCC\t~@FHIFHHHEHHHIIIIIEHHFIHIIHHIIIHIIHHIII\tPG:Z:novoalign\tAS:i:36\tUQ:i:36\tNM:i:1\tMD:Z:24T13\tPQ:i:157\tSM:i:0\tAM:i:0\tZS:Z:R\tNH:i:2\tHI:i:1\tIH:i:1";

    assertEquals("PG:Z:novoalign\tAS:i:36\tUQ:i:36\tNM:i:1\tMD:Z:24T13\tPQ:i:157\tSM:i:0\tAM:i:0\tZS:Z:R\tNH:i:2\tHI:i:1\tIH:i:1", String.join("\t", XsamReadQueries.findVariableRegions(sam, 10)));


  }

  @Test
  void givenValidXSamRecord_whenVariableRegionSearchedFor_checkCorrectStringReturned() {

    String Xsam = "SBL_XSNP077_ID:184296_BC1:CTATTTCAG_\t147\tmm01\t188710074\t70\t39M\t=\t188710023\t-90\tTCCTTGGTCAGCTGTGTTTTAACTTGAGGAGGCCTTCCN\tIHIIIIIIIIIIIIIIIIIHIHIIIIIIIIIIIIIIII~\txl:i:188710074\txr:i:188710112\txs:i:39\txd:A:r\txm:A:u\txa:A:\"\"\txL:i:188710023\txR:i:188710112\txS:i:90\txW:i:12\txP:i:0\txQ:i:0\txC:A:\"\"\txD:A:\"\"\tPG:Z:novoalign\tAS:i:6\tUQ:i:6\tNM:i:1\tMD:Z:38A0\tPQ:i:20\tSM:i:70\tAM:i:70";

    assertEquals("PG:Z:novoalign\tAS:i:6\tUQ:i:6\tNM:i:1\tMD:Z:38A0\tPQ:i:20\tSM:i:70\tAM:i:70", String.join("\t", XsamReadQueries.findVariableRegions(Xsam, 10)));


  }

  @Test
  void givenValidXSamRecordWithNoVariableRegion_whenVariableRegionSearchedFor_checkEmpty() {

    String Xsam = "SBL_XSNP077_ID:184296_BC1:CTATTTCAG_\t147\tmm01\t188710074\t70\t39M\t=\t188710023\t-90\tTCCTTGGTCAGCTGTGTTTTAACTTGAGGAGGCCTTCCN\tIHIIIIIIIIIIIIIIIIIHIHIIIIIIIIIIIIIIII~\txl:i:188710074\txr:i:188710112\txs:i:39\txd:A:r\txm:A:u\txa:A:\"\"\txL:i:188710023\txR:i:188710112\txS:i:90\txW:i:12\txP:i:0\txQ:i:0\txC:A:\"\"\txD:A:\"\"\t";

    assertEquals("", String.join("\t", XsamReadQueries.findVariableRegions(Xsam, 10)));


  }

  @Test
  void givenValidSamRecord_whenStartOfVariableRegionSearchedFor_checkCorrectIntReturned() {

    String sam = "SBL_XSNP077_ID:10_BC1:CTATTTCAG\t99\tSAT_234bp_Repeat_Mus_musculus\t3542\t2\t1S38M\t=\t3921\t415\tNCTCATTTTCCGTGATTTTCAGTTTACTCGCCATATTCC\t~@FHIFHHHEHHHIIIIIEHHFIHIIHHIIIHIIHHIII\tPG:Z:novoalign\tAS:i:36\tUQ:i:36\tNM:i:1\tMD:Z:24T13\tPQ:i:157\tSM:i:0\tAM:i:0\tZS:Z:R\tNH:i:2\tHI:i:1\tIH:i:1";

    assertEquals(169, XsamReadQueries.findVariableRegionStart(sam));


  }
}