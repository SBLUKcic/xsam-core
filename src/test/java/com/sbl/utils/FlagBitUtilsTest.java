package com.sbl.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class FlagBitUtilsTest {

  @Test
  void givenValidXsamFlag_whenQueried_checkBitInfoIsReturnedCorrectly() {

    int flag = 113;

    Assertions.assertAll(
        () -> assertTrue(FlagBitUtils.isBitSet(flag, 0)),
        () -> assertFalse(FlagBitUtils.isBitSet(flag, 1)),
        () -> assertFalse(FlagBitUtils.isBitSet(flag, 2)),
        () -> assertFalse(FlagBitUtils.isBitSet(flag, 3)),
        () -> assertTrue(FlagBitUtils.isBitSet(flag, 4)),
        () -> assertTrue(FlagBitUtils.isBitSet(flag, 5)),
        () -> assertTrue(FlagBitUtils.isBitSet(flag, 6)),
        () -> assertFalse(FlagBitUtils.isBitSet(flag, 7)),
        () -> assertFalse(FlagBitUtils.isBitSet(flag, 8)),
        () -> assertFalse(FlagBitUtils.isBitSet(flag, 9)),
        () -> assertFalse(FlagBitUtils.isBitSet(flag, 10)),
        () -> assertFalse(FlagBitUtils.isBitSet(flag, 11))
    );


  }

  @Test
  void givenValidXsamFlag_whenBitToggleD_checkBitCorrect() {

    int flag = 113;

    //set read unmapped

    int readUnmappedFlag = FlagBitUtils.toggleBit(flag, 2);

    assertEquals(117, readUnmappedFlag);

  }

  @Test
  void givenValidXsamFlag_whenBitToggledOff_checkBitCorrect() {

    int flag = 117;

    //set read unmapped

    int readUnmappedFlag = FlagBitUtils.toggleBit(flag, 2);

    assertEquals(113, readUnmappedFlag);

  }

  @Test
  void givenValidXsamFlag_whenQueriedUsingAbstractedMethods_checkBitInfoIsReturnedCorrectly() {

    int flag = 113;

    Assertions.assertAll(
        () ->   assertTrue(FlagBitUtils.isReadPaired(flag)),
        () -> assertFalse(FlagBitUtils.isReadMappedInProperPair(flag)),
        () -> assertFalse(FlagBitUtils.isReadUnmapped(flag)),
        () -> assertFalse(FlagBitUtils.isMateUnmapped(flag)),
        () -> assertTrue(FlagBitUtils.isReadReverseStand(flag)),
        () -> assertTrue(FlagBitUtils.isMateReverseStrand(flag)),
        () -> assertTrue(FlagBitUtils.isFirstInPair(flag)),
        () -> assertFalse(FlagBitUtils.isSecondInPair(flag)),
        () -> assertFalse(FlagBitUtils.isNotPrimaryAlignment(flag)),
        () -> assertFalse(FlagBitUtils.isReadQualityCheckFailed(flag)),
        () -> assertFalse(FlagBitUtils.isReadPCROrOpticalDuplicate(flag)),
        () -> assertFalse(FlagBitUtils.isReadSupplementaryAlignment(flag))
    );


  }

  @Test
  void giveFirstInPairFlag_whenBitSwapped_checkBitSwappedCorrectly() {

    assertEquals(177, FlagBitUtils.changePositionInPair(113));

  }

  @Test
  void giveSecondInPairFlag_whenBitSwapped_checkBitSwappedCorrectly() {

    assertEquals(113, FlagBitUtils.changePositionInPair(177));

  }
}