package com.sbl.utils;

import com.sbl.filtering.predicates.XsamRecordPredicates;
import com.sbl.model.XsamRecord;
import com.sbl.providers.SampleXsamFileCreator;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.File;
import java.io.IOException;
import java.util.function.Predicate;

import static org.junit.jupiter.api.Assertions.*;


class XsamUtilsTest {

  String read1 = "SBL_XSMJR275_ID:1108283_BC1:ACACTTCAG_\t99\tmm07\t59176172\t30\t2S39M\t=\t59176371\t235\tNNACAGGAAACCCACTCAGATCTACCTCTCATGGGCCAAAT\t~~IIIIIIIIIHGIIIHIIIIIIIIIIIHIIIIIIIIIIII\txl:i:59176172\txr:i:59176210\txs:i:39\txd:A:f\txm:A:u\txa:A:\"\"\t\txL:i:59176172\txR:i:59176406\txLseq:i:AGCT\txRseq:i:TGTG\txS:i:235\txW:i:160\txP:i:0\txQ:i:0\txC:A:\"\"\txD:A:\"\"\tPG:Z:novoalign\tAS:i:12\tUQ:i:12\tNM:i:0\tMD:Z:39\tPQ:i:49\tSM:i:23\tAM:i:0\t";
  String read2 = "SBL_XSMJR275_ID:1108283_BC1:ACACTTCAG_\t147\tmm07\t59176371\t30\t36M3S\t=\t59176172\t-235\tGTGTGTGTGTGTGAGTGTGTGTGTGTGTGTGTGTGTATN\tIHIIIIIIIIIHG?HCIIIIIIIHIIIIIIIIIIIIII~\txl:i:59176371\txr:i:59176406\txs:i:36\txd:A:r\txm:A:u\txa:A:\"\"\t\txL:i:59176172\txR:i:59176406\txLseq:i:AGCT\txRseq:i:TGTG\txS:i:235\txW:i:160\txP:i:0\txQ:i:0\txC:A:\"\"\txD:A:\"\"\tPG:Z:novoalign\tAS:i:36\tUQ:i:36\tNM:i:0\tMD:Z:36\tPQ:i:49\tSM:i:0\tAM:i:0";

  private static File inputFile = new File(System.getProperty("user.dir") + File.separator + "myFake.Xsam");

  @BeforeAll
  static void setUp() throws Exception {
    if(!inputFile.createNewFile()){
      fail();
    }

    inputFile = SampleXsamFileCreator.writeSmolXsamFile(inputFile);
  }

  @Test
  void givenValidPhred_whenAverageCalculated_checkAverageCorrect() {

    int av = XsamUtils.calculateAveragePHRED(XsamReadQueries.findPhred(read1));

    assertEquals(75, av);

  }

  @Test
  void givenPHREDValueWithOneBaseBelowThreshold_whenChecked_checkFailed() {

    String phred = "##############################!################";

    int threshold = 34;

    assertFalse(XsamUtils.areAllPHREDValuesAboveThreshold(phred, threshold));

  }

  @Test
  void givenPHREDValueWithAllAbove_whenChecked_checkPassed() {

    String phred = "#######ABCDFEFEFDQDSDAFFWF################";

    int threshold = 34;

    assertTrue(XsamUtils.areAllPHREDValuesAboveThreshold(phred, threshold));

  }

  @Test
  void givenTwoValidPhreds_whenAvCalculated_checkAverageCorrect() {

    int av = XsamUtils.calculateAveragePHRED(XsamReadQueries.findPhred(read1), XsamReadQueries.findPhred(read2));

    assertEquals(74, av);

  }

  @Test
  void givenValidPairedSmolXsamFile_whenSearchedForPairedReads_checkTrueReturned() throws IOException {


    assertTrue(XsamUtils.isFilePaired(inputFile));

  }

  @Test
  void givenValidString_whenRC_checkCorrect() {

    String sequence = "ATCGN";

    assertEquals("NCGAT", XsamUtils.reverseCompliment(sequence));

  }

  @Test
  void givenFirstReadWithNEnd_whenTrimmed_checkCorrectStringReturned() {

    String basesOfRead1 = XsamReadQueries.findBaseSequence(read1);

    assertEquals("NNACAGGAAACCCACTCAGATCTACCTCTCATGGGCCAAAT", basesOfRead1);

    String basesOfRead1NRemoved = XsamUtils.removeNs(basesOfRead1);

    assertEquals("ACAGGAAACCCACTCAGATCTACCTCTCATGGGCCAAAT", basesOfRead1NRemoved);


  }

  @Test
  void givenSecondReadWithNEnd_whenTrimmed_checkCorrectStringReturned() {

    String basesOfRead2 = XsamReadQueries.findBaseSequence(read2);

    assertEquals("GTGTGTGTGTGTGAGTGTGTGTGTGTGTGTGTGTGTATN", basesOfRead2);

    String basesOfRead2NRemoved = XsamUtils.removeNs(basesOfRead2);

    assertEquals("GTGTGTGTGTGTGAGTGTGTGTGTGTGTGTGTGTGTAT", basesOfRead2NRemoved);

  }

  @Test
  void givenSecondString_whenSoftTrimmed_checkLast3BasesRemoved() {

    String basesOfRead2 = XsamReadQueries.findBaseSequence(read2);

    assertEquals("GTGTGTGTGTGTGAGTGTGTGTGTGTGTGTGTGTGTATN", basesOfRead2);

    String cigar = XsamReadQueries.findCigar(read2);

    assertEquals("36M3S", cigar);

    String softTrimmed = XsamUtils.removeSoftTrimmedBases(basesOfRead2, cigar);

    assertEquals("GTGTGTGTGTGTGAGTGTGTGTGTGTGTGTGTGTGT", softTrimmed);


  }

  @Test
  void givenFirstString_whenSoftTrimmed_checkFirst2BasesRemoved() {

    String basesOfRead1 = XsamReadQueries.findBaseSequence(read1);

    assertEquals("NNACAGGAAACCCACTCAGATCTACCTCTCATGGGCCAAAT", basesOfRead1);

    String cigar = XsamReadQueries.findCigar(read1);

    assertEquals("2S39M", cigar);

    String softTrimmed = XsamUtils.removeSoftTrimmedBases(basesOfRead1, cigar);

    assertEquals("ACAGGAAACCCACTCAGATCTACCTCTCATGGGCCAAAT", softTrimmed);

  }

  @Test
  void givenFirstString_whenSoftTrimmedWithMadeUpCigar_checkBasesRemoved() {

    String basesOfRead1 = XsamReadQueries.findBaseSequence(read1);

    assertEquals("NNACAGGAAACCCACTCAGATCTACCTCTCATGGGCCAAAT", basesOfRead1);

    String cigar = "2M2S37M";

    String softTrimmed = XsamUtils.removeSoftTrimmedBases(basesOfRead1, cigar);

    assertEquals("NNAGGAAACCCACTCAGATCTACCTCTCATGGGCCAAAT", softTrimmed);

  }

  @Test
  void BUGFIX_stringOutOfBoundException_startGreaterTHanLength() {

    String sequence = "NNCATTTGCATATGAACTGATGCGGGTTCCAGGCTTTCCCC";

    String cigar = "2S38M1S";

    assertEquals("CATTTGCATATGAACTGATGCGGGTTCCAGGCTTTCCC", XsamUtils.removeSoftTrimmedBases(sequence, cigar));

  }

  @Test
  void BUGFIX2_stringOutOfBoundExcpetion_startGreaterThanLength2() {

    String sequence = "AGAAACTCACATGGTAGGACATGAAACATGGCAAGAAGCNN";

    String cigar = "18M4D20M3S";

    assertEquals("AGAAACTCACATGGTAGGACATGAAACATGGCAAGAAG", XsamUtils.removeSoftTrimmedBases(sequence, cigar));

  }

  @ParameterizedTest
  @CsvSource({"2S39M, 2", "5SM, 5", "39M, -1", "50S, 50"})
  void givenValidStrings_whenStartingSoftBasesCheckedFor_returnValidAnswer(String s, int expectedValue) {


      assertEquals(expectedValue, XsamUtils.findStartingSoftBases(s));


  }


  @ParameterizedTest
  @CsvSource({"2S39M36S, 36", "5S40M34S, 34", "39M, -1", "50S, 50"})
  void givenValidStrings_whenEndingSoftBasesCheckedFor_returnValidAnswer(String s, int expectedValue) {


      assertEquals(expectedValue, XsamUtils.findEndingSoftBases(s));

  }

  @ParameterizedTest
  @MethodSource({"com.sbl.providers.XsamRecordProviders#positionedRecordStream", "com.sbl.providers.XsamRecordProviders#nonMatchRecordStream"})
  void givenAverageQualityPredicated_whenAllEqualToThreshold_checkTrue(XsamRecord record) {

    int avQual = XsamUtils.calculateAveragePHRED(record.getQuality());

    Predicate<XsamRecord> avQualCheck = XsamRecordPredicates.createAverageQualityAboveThresholdPredicate(avQual);

    assertTrue(avQualCheck.test(record));

  }

  @ParameterizedTest
  @MethodSource({"com.sbl.providers.XsamRecordProviders#positionedRecordStream", "com.sbl.providers.XsamRecordProviders#nonMatchRecordStream"})
  void givenAverageQualityPredicated_whenAllAboveThreshold_checkTrue(XsamRecord record) {

    int avQual = XsamUtils.calculateAveragePHRED(record.getQuality());

    Predicate<XsamRecord> avQualCheck = XsamRecordPredicates.createAverageQualityAboveThresholdPredicate(avQual-1);

    assertTrue(avQualCheck.test(record));

  }

  @ParameterizedTest
  @MethodSource({"com.sbl.providers.XsamRecordProviders#positionedRecordStream", "com.sbl.providers.XsamRecordProviders#nonMatchRecordStream"})
  void givenAverageQualityPredicated_whenAllBelowThreshold_checkTrue(XsamRecord record) {

    int avQual = XsamUtils.calculateAveragePHRED(record.getQuality());

    Predicate<XsamRecord> avQualCheck = XsamRecordPredicates.createAverageQualityAboveThresholdPredicate(avQual+1);

    assertFalse(avQualCheck.test(record));

  }

  @AfterAll
  static void tearDown() throws Exception {
    if(inputFile.exists()){
      if(!inputFile.delete()){
        fail();
      }
    }
  }
}