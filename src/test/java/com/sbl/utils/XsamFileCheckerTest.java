package com.sbl.utils;

import com.sbl.providers.SampleXsamFileCreator;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;

import static org.junit.jupiter.api.Assertions.*;

class XsamFileCheckerTest {


    public File folder = new File(System.getProperty("user.dir") + File.separator + "mySampleFolderXsamfilecheckertest");

    private File tempXsamFile;
    private File headerOnly;
    private File tempSamfile;

    @BeforeEach
    void setUp() throws  Exception{
        folder.mkdirs();

        tempXsamFile = new File(folder + File.separator + "myFile.Xsam");
        headerOnly = SampleXsamFileCreator.createFakeXsamHeader(new File(folder + File.separator + "myNewHeaderOnly.Xsam"));
        tempSamfile = SampleXsamFileCreator.writeSmolSamFile(new File(folder + File.separator + "tempSam.sam"));

        tempXsamFile = SampleXsamFileCreator.writeSmolXsamFile(tempXsamFile);
    }

    @Test
    void givenValidXsamFile_whenChecked_checkXsamTypeReturned() throws Exception{

        FileType t = XsamFileChecker.findFileType(tempXsamFile);

        assertEquals(FileType.xsam, t);

    }

    @Test
    void givenValidXsamFile_whenChecked_checkTrue() throws Exception{

        assertTrue(XsamFileChecker.isXsamFileValid(tempXsamFile));

    }

    @Test
    void givenValidsamFile_whenChecked_checkFalse() throws Exception{

        assertFalse(XsamFileChecker.isXsamFileValid(tempSamfile));

    }

    @Test
    void givenValidsamFile_whenChecked_checksamTypeReturned() throws Exception{

        FileType t = XsamFileChecker.findFileType(tempSamfile);

        assertEquals(FileType.sam, t);

    }

    @Test
    void givenHeaderOnly_whenChecked_checkOtherReturned() throws Exception {

        FileType t = XsamFileChecker.findFileType(headerOnly);

        assertEquals(FileType.other, t);
    }

    @AfterEach
    void tearDown() {
        for(File f : folder.listFiles()){
            f.delete();
        }

        folder.delete();
    }
}