package com.sbl.conversion;

import com.sbl.conversion.model.XsamReturn;
import com.sbl.model.PairedSamRecord;
import com.sbl.model.PairedXsamRecord;
import com.sbl.model.SamRecord;
import com.sbl.model.XsamRecord;
import com.sbl.model.xsamtypes.PairedXsamRecordMappingCombination;
import com.sbl.model.xsamtypes.XsamRecordMappingType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import static org.junit.jupiter.api.Assertions.*;

class XsamConverterOneTest {

  private XsamConverterOne converterOne;

  private SamRecord nonRepeatSamRecord = new SamRecord("SBL_XSNP076_ID:12_BC1:TCGGATCAG\t113\tL1MdF_V_L1_Mus_musculus\t5878\t2\t34M5S\t=\t6336\t499\tCACATGATATGTACTCACTGATAAGTGGATATTAACCCN\tIIIIIIIIIIIIHHHIIIIIIIHIIIIHIIIIIIIIHI~\tPG:Z:novoalign\tAS:i:36\tUQ:i:36\tNM:i:0\tMD:Z:34\tNH:i:3\tHI:i:1\tIH:i:1");

  @BeforeEach
  void setUp() {
    converterOne = new XsamConverterOne();
  }


  @Test
  void givenValidSingleSamRead_whenConvertedToXsamWithRepeatMode1_checkXsamReadCorrect() throws Exception {

    XsamRecord record = converterOne.convertSingleRecord(nonRepeatSamRecord);

    assertEquals("SBL_XSNP076_ID:12_BC1:TCGGATCAG\t113\tL1MdF_V_L1_Mus_musculus\t5878\t2\t34M5S\t=\t6336\t499\tCACATGATATGTACTCACTGATAAGTGGATATTAACCCN\tIIIIIIIIIIIIHHHIIIIIIIHIIIIHIIIIIIIIHI~\txl:i:5878\txr:i:5911\txs:i:34\txd:A:r\txm:A:u\txa:A:\"\"\txx:i:0\tPG:Z:novoalign\tAS:i:36\tUQ:i:36\tNM:i:0\tMD:Z:34\tNH:i:3\tHI:i:1\tIH:i:1", record.toString());

  }

  @Test
  void givenValidRepeatRead_whenConverted_checkConversionToX() {

    String repeatSamRecord = "SBL_XSBF432_ID:825046\t129\tmm03\t119533673\t0\t41M\tmm13\t3164256\t0\tAACAACAGATCCTGAAGAAATCCAAAACACCATTAGATCNN\tGGGGGGGAAAGGGGGGGGIGGIIGGAAAAGGIIGGGGII~~\tPG:Z:novoalign\tAS:i:12\tUQ:i:12\tNM:i:2\tMD:Z:39C0T0\tZS:Z:R\tNH:i:63\tHI:i:1\tIH:i:1";

    SamRecord record = new SamRecord(repeatSamRecord);

    assertEquals(true, record.isRepeat());

    String expectedOutput = "SBL_XSNP076_ID:12_BC1:TCGGATCAG\t133\t*\t0\t0\t*\t*\t0\t0\tCACATGATATGTACTCACTGATAAGTGGATATTAACCCN\tIIIIIIIIIIIIHHHIIIIIIIHIIIIHIIIIIIIIHI~\txl:i:0\txr:i:0\txs:i:0\txd:A:x\txm:A:x\txa:A:\"\"\txx:i:1\tPG:Z:novoalign\tZS:Z:NM";

    XsamRecord xRecord = converterOne.convertSingleRecord(record);

    assertEquals(false, xRecord.isMapped());


  }

  @ParameterizedTest
  @MethodSource("com.sbl.providers.SamPairProviders#urPairProvider")
  void givenURRead_whenConverted_checkConversionToUXCorrect(PairedSamRecord record) {

    XsamReturn returner = converterOne.convertRecordPair(record);

    PairedXsamRecord paired = returner.getPair();

    assertEquals("SBL_XSNP077_ID:184296_BC1:CTATTTCAG_\t133\t*\t0\t0\t*\t*\t0\t0\tTCCTTGGTCAGCTGTGTTTTAACTTGAGGAGGCCTTCCN\tIHIIIIIIIIIIIIIIIIIHIHIIIIIIIIIIIIIIII~\txl:i:0\txr:i:0\txs:i:0\txd:A:x\txm:A:x\txa:A:x\txx:i:1\txL:i:188710023\txR:i:188710061\txS:i:39\txW:i:0\txP:i:0\txQ:i:0\txC:A:\"\"\txD:A:\"\"\tPG:Z:novoalign\tZS:Z:NM", paired.getRecord2().toString());



  }

  private String testRecord1 = "SBL_XPBF613_ID:525944/1:vpe	113	hs07	142358614	70	53M	hs14	22423156	0	GCCTGCCTCTCCCACAGCCCCCATGGAGGCAGGGATCAGCCAGATACCAAGAT	B@CCC@CBBB>DDBB@@1CCCD@A>BA=?BB<BBBCBCB?BB@@AA?A?DAAB	PG:Z:novoalign	AS:i:0	UQ:i:0	NM:i:0	MD:Z:53";
  private String testRecord2 = "SBL_XPBF613_ID:525944/1:vpe	177	hs14	22423156	3	51M2S	hs07	142358614	0	GGAGATCGAGACCATCCTGGCTAACACGGTGAAACCCCGTCTCTACTAAAATT	A?BBBDCCBABB?C@A>AC>BBB>BBBC>B@?>3;=;2>AC@@@>?7878(8:	PG:Z:novoalign	AS:i:88	UQ:i:88	NM:i:2	MD:Z:24T16C9	ZS:Z:R	NH:i:2	HI:i:1	IH:i:12";

  @Test
  void bugFixTest1() {

    PairedSamRecord record = new PairedSamRecord(new SamRecord(testRecord1), new SamRecord(testRecord2));

    XsamReturn xsamReturn = converterOne.convertRecordPair(record);

    //should be a PX read.
    PairedXsamRecord recordReturn = xsamReturn.getPair();

    assertEquals(PairedXsamRecordMappingCombination.PX, recordReturn.getMappingCombination());
    assertEquals(142358614, recordReturn.getxL());
    assertFalse(xsamReturn.getOptionalPair().isPresent());


  }
}