package com.sbl.conversion;

import com.sbl.conversion.model.XsamReturn;
import com.sbl.model.PairedSamRecord;
import com.sbl.model.SamRecord;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.jupiter.api.Assertions.*;

class XsamConverterFactoryTest {

  private XsamConverterFactory factory;

  @BeforeEach
  void setUp() {
    factory = new XsamConverterFactory();
  }

  @Test
  void givenRepeatModeRequestis1_whenFactoryCalled_checkInstanceReturnedCorrect() {

    XsamConverter converter = factory.getXsamConverter(1);

    assertTrue(converter instanceof XsamConverterOne);

  }

  @Test
  void givenRepeatModeRequestis0_whenFactoryCalled_checkInstanceReturnedCorrect() {

    XsamConverter converter = factory.getXsamConverter(0);

    assertTrue(converter instanceof XsamConverterZero);

  }

  @Test
  void givenRepeatModeRequestisInvalid_whenFactoryCalled_checkInstanceReturnedCorrect() {

    XsamConverter converter = factory.getXsamConverter(Integer.MAX_VALUE);

    assertTrue(converter instanceof XsamConverterOne);

  }

  @Test
  void givenSameAtomicIntegerPassed_whenFactoriesCalled_checkSameAtomicIntegerUsed() {

    SamRecord ppdSamRecord1 = new SamRecord("SBL_XSNP075_ID:11_BC1:CCAGATCAG\t83\tmm01\t4040\t6\t41M\tmm02\t3915\t-166\tCCTAGCCAGAGCAATTAGACAACAAAAGGAGATCAAGGGNN\tIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII~~\tPG:Z:novoalign\tAS:i:12\tUQ:i:12\tNM:i:2\tMD:Z:39G0A0\tPQ:i:19\tSM:i:1\tAM:i:1\tZS:Z:R\tNH:i:2\tHI:i:1\tIH:i:1");
    SamRecord ppdSamRecord2 = new SamRecord("SBL_XSNP075_ID:11_BC1:CCAGATCAG\t163\tmm02\t3915\t6\t1S38M\tmm01\t4040\t166\tNATATACAGCAAACCAGTAGCCAACATCAAACTAAATGG\t~IIIIIIIIIIIIIIIIIIIIIIIIIHIIIIIIIIIIII\tPG:Z:novoalign\tAS:i:6\tUQ:i:6\tNM:i:0\tMD:Z:38\tPQ:i:19\tSM:i:1\tAM:i:1\tZS:Z:R\tNH:i:2\tHI:i:1\tIH:i:1");


    AtomicInteger atomicInteger = new AtomicInteger(1);

    XsamConverter con1 = factory.getXsamConverter(0, atomicInteger);
    XsamConverter con2 = factory.getXsamConverter(0, atomicInteger);

    XsamReturn one = con1.convertRecordPair(new PairedSamRecord(ppdSamRecord1, ppdSamRecord2));
    XsamReturn two = con2.convertRecordPair(new PairedSamRecord(ppdSamRecord1, ppdSamRecord2));

    assertEquals(one.getPair().getxP(), 1);
    assertEquals(two.getPair().getxP(), 2);

    assertTrue(one.getOptionalPair().isPresent());
    assertTrue(two.getOptionalPair().isPresent());



  }
}