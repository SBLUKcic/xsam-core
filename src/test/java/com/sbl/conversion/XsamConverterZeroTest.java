package com.sbl.conversion;

import com.sbl.conversion.model.XsamReturn;
import com.sbl.model.*;
import com.sbl.model.PairedXsamRecord;
import com.sbl.model.PairedSamRecord;
import com.sbl.utils.FlagBitUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.*;

class XsamConverterZeroTest {

  private SamRecord samRecord = new SamRecord("SBL_XSNP076_ID:12_BC1:TCGGATCAG\t113\tL1MdF_V_L1_Mus_musculus\t5878\t2\t34M5S\t=\t6336\t499\tCACATGATATGTACTCACTGATAAGTGGATATTAACCCN\tIIIIIIIIIIIIHHHIIIIIIIHIIIIHIIIIIIIIHI~\tPG:Z:novoalign\tAS:i:36\tUQ:i:36\tNM:i:0\tMD:Z:34\tZS:Z:R\tNH:i:3\tHI:i:1\tIH:i:1");

  private SamRecord ppeSamRecord1 = new SamRecord("SBL_XSNP075_ID:11_BC1:CCAGATCAG\t83\tL1MdFanc_II_L1_Mus_musculus\t4040\t6\t41M\t=\t3915\t-166\tCCTAGCCAGAGCAATTAGACAACAAAAGGAGATCAAGGGNN\tIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII~~\tPG:Z:novoalign\tAS:i:12\tUQ:i:12\tNM:i:2\tMD:Z:39G0A0\tPQ:i:19\tSM:i:1\tAM:i:1\tZS:Z:R\tNH:i:2\tHI:i:1\tIH:i:1");
  private SamRecord ppeSamRecord2 = new SamRecord("SBL_XSNP075_ID:11_BC1:CCAGATCAG\t163\tL1MdFanc_II_L1_Mus_musculus\t3915\t6\t1S38M\t=\t4040\t166\tNATATACAGCAAACCAGTAGCCAACATCAAACTAAATGG\t~IIIIIIIIIIIIIIIIIIIIIIIIIHIIIIIIIIIIII\tPG:Z:novoalign\tAS:i:6\tUQ:i:6\tNM:i:0\tMD:Z:38\tPQ:i:19\tSM:i:1\tAM:i:1\tZS:Z:R\tNH:i:2\tHI:i:1\tIH:i:1");

  private SamRecord ppdSamRecord1 = new SamRecord("SBL_XSNP075_ID:11_BC1:CCAGATCAG\t83\tmm01\t4040\t6\t41M\tmm02\t3915\t-166\tCCTAGCCAGAGCAATTAGACAACAAAAGGAGATCAAGGGNN\tIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII~~\tPG:Z:novoalign\tAS:i:12\tUQ:i:12\tNM:i:2\tMD:Z:39G0A0\tPQ:i:19\tSM:i:1\tAM:i:1\tZS:Z:R\tNH:i:2\tHI:i:1\tIH:i:1");
  private SamRecord ppdSamRecord2 = new SamRecord("SBL_XSNP075_ID:11_BC1:CCAGATCAG\t163\tmm02\t3915\t6\t1S38M\tmm01\t4040\t166\tNATATACAGCAAACCAGTAGCCAACATCAAACTAAATGG\t~IIIIIIIIIIIIIIIIIIIIIIIIIHIIIIIIIIIIII\tPG:Z:novoalign\tAS:i:6\tUQ:i:6\tNM:i:0\tMD:Z:38\tPQ:i:19\tSM:i:1\tAM:i:1\tZS:Z:R\tNH:i:2\tHI:i:1\tIH:i:1");

  private SamRecord pxSamRecord1PFirst = new SamRecord("SBL_XSNP075_ID:78_BC1:CCAGATCAG\t89\tL1MdF_V_L1_Mus_musculus\t4654\t2\t41M\t=\t4654\t0\tTAGCAAAACCTCTTCTCAATGATAAAAGAACCTCTGGTGNN\tIIIIIIHGIIIIIIIIIIIIIIIIIIIIIIIIIIIIHII~~\tPG:Z:novoalign\tAS:i:45\tUQ:i:45\tNM:i:4\tMD:Z:8A10K19G0A0\tZS:Z:R\tNH:i:2\tHI:i:1\tIH:i:1\n");
  private SamRecord pxSamRecord2PFirst = new SamRecord("SBL_XSNP075_ID:78_BC1:CCAGATCAG\t165\tL1MdF_V_L1_Mus_musculus\t4654\t0\t*\t=\t4654\t0\tNGATGTAAACGAGAATTAAGAAGCTATGACACGTGTATG\t~IIIIIIHHHIIIIIHIIIIHIIIIHIIIIIIIIIIIII\tPG:Z:novoalign\tZS:Z:NM\n");

  private SamRecord pxSamRecord1XFirst = new SamRecord("SBL_XSNP075_ID:78_BC1:CCAGATCAG\t101\tL1MdF_V_L1_Mus_musculus\t4654\t0\t*\t=\t4654\t0\tNGATGTAAACGAGAATTAAGAAGCTATGACACGTGTATG\t~IIIIIIHHHIIIIIHIIIIHIIIIHIIIIIIIIIIIII\tPG:Z:novoalign\tZS:Z:NM\n");
  private SamRecord pxSamRecord2XFirst = new SamRecord("SBL_XSNP075_ID:78_BC1:CCAGATCAG\t153\tL1MdF_V_L1_Mus_musculus\t4654\t2\t41M\t=\t4654\t0\tTAGCAAAACCTCTTCTCAATGATAAAAGAACCTCTGGTGNN\tIIIIIIHGIIIIIIIIIIIIIIIIIIIIIIIIIIIIHII~~\tPG:Z:novoalign\tAS:i:45\tUQ:i:45\tNM:i:4\tMD:Z:8A10K19G0A0\tZS:Z:R\tNH:i:2\tHI:i:1\tIH:i:1\n");

  private SamRecord xxRecord1 = new SamRecord("SBL_XSNP075_ID:284_BC1:CCAGATCAG\t77\t*\t0\t0\t*\t*\t0\t0\tNATGTACATTTTACCATTATCCACACAGACAACGTTTCT\t~IIIIIIHIIIIIIHIIIIIHIIIHIIIIIIHIDHHIHI\tPG:Z:novoalign\tZS:Z:NM");
  private SamRecord xxRecord2 = new SamRecord("SBL_XSNP075_ID:284_BC1:CCAGATCAG\t141\t*\t0\t0\t*\t*\t0\t0\tNNCTCAGGACACTCTTTGTATTCTTTAAATGAAACCATAAC\t~~IIIIIIIIIHIIIIIIIIIIIIIIIIIIIIIIIIIIEHI\tPG:Z:novoalign\tZS:Z:NM");

  private XsamConverterZero converterZero;

  @BeforeEach
  void setUp() {
    converterZero = new XsamConverterZero();
  }

  @Test
  void givenValidSingleSamRead_whenConvertedToXsamWithRepeatMode0_checkXsamReadCorrect() throws Exception {



    XsamRecord record = converterZero.convertSingleRecord(samRecord);

    assertEquals("SBL_XSNP076_ID:12_BC1:TCGGATCAG\t113\tL1MdF_V_L1_Mus_musculus\t5878\t2\t34M5S\t=\t6336\t499\tCACATGATATGTACTCACTGATAAGTGGATATTAACCCN\tIIIIIIIIIIIIHHHIIIIIIIHIIIIHIIIIIIIIHI~\txl:i:5878\txr:i:5911\txs:i:34\txd:A:r\txm:A:r\txa:A:\"\"\txx:i:0\tPG:Z:novoalign\tAS:i:36\tUQ:i:36\tNM:i:0\tMD:Z:34\tZS:Z:R\tNH:i:3\tHI:i:1\tIH:i:1", record.toString());

  }

  @Test
  void givenPPESamRecordPair_whenConvertedWithRepeatMode0_checkXsamReadCorrect() {

    PairedXsamRecord pair = converterZero.convertRecordPair(new PairedSamRecord(ppeSamRecord1, ppeSamRecord2)).getPair();

    assertEquals(3915, pair.getxL());
    assertEquals(4080, pair.getxR());


    assertEquals(4080 - 3915 + 1, pair.getxS());
    assertEquals(0, pair.getxQ());

    assertFalse(pair.getRecord1().getRead().contains("\t\t"));
    assertFalse(pair.getRecord2().getRead().contains("\t\t"));

  }

  @Test
  void givenPPDSamRecordPair_whenConvertedWithRepeatMode0_checkXsamReadCorrect() {

    XsamReturn pairs = converterZero.convertRecordPair(new PairedSamRecord(ppdSamRecord1, ppdSamRecord2));

    assertTrue(pairs.getOptionalPair().isPresent());

    assertEquals(4040, pairs.getPair().getxL());
    assertEquals(3915, pairs.getOptionalPair().get().getxL());


    assertTrue(FlagBitUtils.isFirstInPair(pairs.getPair().getRecord1().getFlag()));
    assertTrue(FlagBitUtils.isSecondInPair(pairs.getPair().getRecord2().getFlag()));
    assertTrue(FlagBitUtils.isFirstInPair(pairs.getOptionalPair().get().getRecord1().getFlag()));
    assertTrue(FlagBitUtils.isSecondInPair(pairs.getOptionalPair().get().getRecord2().getFlag()));

    assertFalse(pairs.getPair().getRecord1().getRead().contains("\t\t"));
    assertFalse(pairs.getOptionalPair().get().getRecord2().getRead().contains("\t\t"));


  }

  @Test
  void givenPXPairWithPFirst_whenConvertedWithRepeatMode0_checkPairedFieldsCorrect() {

    PairedXsamRecord pair = converterZero.convertRecordPair(new PairedSamRecord(pxSamRecord1PFirst, pxSamRecord2PFirst)).getPair();

    assertEquals(0, pair.getRecord2().getXl());
    assertEquals(4654, pair.getRecord1().getXl());

    assertEquals(4654, pair.getxL());

    assertEquals(4694, pair.getxR());
    assertEquals(41, pair.getxS());

    assertFalse(pair.getRecord1().getRead().contains("\t\t"));
    assertFalse(pair.getRecord2().getRead().contains("\t\t"));

  }


  @Test
  void givenPXPairWithXFirst_whenConvertedWithRepeatMode0_checkPairedFieldsFlippedPX() {

    PairedXsamRecord pair = converterZero.convertRecordPair(new PairedSamRecord(pxSamRecord1XFirst, pxSamRecord2XFirst)).getPair();

    assertEquals(0, pair.getRecord2().getXl());
    assertEquals(4654, pair.getRecord1().getXl());

    assertEquals(4654, pair.getxL());

    assertEquals(4694, pair.getxR());
    assertEquals(41, pair.getxS());

    assertFalse(pair.getRecord1().getRead().contains("\t\t"));
    assertFalse(pair.getRecord2().getRead().contains("\t\t"));

  }

  @Test
  void givenXXPair_whenConvertedWithRepeatMode0_checkPairedFieldsCorrect() {

    PairedXsamRecord xxPair = converterZero.convertRecordPair(new PairedSamRecord(xxRecord1, xxRecord2)).getPair();

    assertEquals(0, xxPair.getRecord1().getXl());
    assertEquals(0, xxPair.getRecord2().getXl());

    assertEquals(0, xxPair.getxL());

    assertFalse(xxPair.getRecord1().getRead().contains("\t\t"));
    assertFalse(xxPair.getRecord2().getRead().contains("\t\t"));

  }

  String rec1BugFix = "SBL_XPBF613_ID:323612/1:vpe\t113\ths14\t22423106\t4\t1S52M\ths07\t142358682\t0\tGCCTGTAATCCCAGCACTTTGGGAGGCCGAGGCGGGCGGATCACGAGGTCAGG\tA?BBBB@CC=CCC@@@B=DA<A@@?A?AB><998D??<BB@@ABBB@BBBB>B\tPG:Z:novoalign\tAS:i:88\tUQ:i:88\tNM:i:2\tMD:Z:31T3T16\tZS:Z:R\tNH:i:2\tHI:i:1\tIH:i:1";
  String rec2BugFix = "SBL_XPBF613_ID:323612/1:vpe\t177\ths07\t142358682\t70\t50M3S\ths14\t22423106\t0\tGGAAAAAGATCATCCTGAAATATGCTCAGATTAGGAACCATTATTCAGTGATT\tA+9999(999AABC@AABE?BBCDCDDBCCDACC@D@C@BB@BB?BCCBCAD@\tPG:Z:novoalign\tAS:i:77\tUQ:i:77\tNM:i:1\tMD:Z:21T28";

  @Test
  void bugFixedStringOutOfBoundsExceptionPPETest() {

    XsamReturn xsamRecord = converterZero.convertRecordPair(new PairedSamRecord(new SamRecord(rec1BugFix), new SamRecord(rec2BugFix)));

    assertTrue(xsamRecord.getOptionalPair().isPresent());

  }

  //ppdsamrecord1


  /*@Test
  void givenRepeatRead_whenConvertenWithRepeatMode0_checkConvertedToXCorrectly() {

    XsamRecord record = XsamConverterZero.convertSingleRecord(ppdSamRecord1, 1);

    String expected = "SBL_XSNP075_ID:11_BC1:CCAGATCAG\t83\tmm01\t4040\t6\t41M\tmm02\t3915\t-166\tCCTAGCCAGAGCAATTAGACAACAAAAGGAGATCAAGGGNN\tIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII~~\tPG:Z:novoalign\tAS:i:12\tUQ:i:12\tNM:i:2\tMD:Z:39G0A0\tPQ:i:19\tSM:i:1\tAM:i:1\tZS:Z:R\tNH:i:2\tHI:i:1\tIH:i:1";

    assertNotNull(record);



  }*/
}