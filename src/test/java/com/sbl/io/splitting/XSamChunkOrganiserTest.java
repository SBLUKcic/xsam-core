package com.sbl.io.splitting;

import com.sbl.io.XsamChunk;
import com.sbl.model.PairedXsamRecord;
import com.sbl.model.XsamRecord;
import com.sbl.sorting.SortingMethods;
import com.sbl.sorting.XsamChunkSorter;
import com.sbl.utils.XSamChunkOrganiser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class XSamChunkOrganiserTest {

    private XsamChunkSorter sorter;

    @BeforeEach
    void setUp() {
        sorter = new XsamChunkSorter();
    }

    @ParameterizedTest
    @MethodSource({"com.sbl.providers.XsamRecordProviders#randomLongListOfRecords"})
    void givenALongListOfRecords_whenSplit_checkSplittingCorrect(List<XsamRecord> records) throws Exception{

        File file = new File(System.getProperty("user.dir") + File.separator + "inputRecords.Xsam");
        File tmpDir = new File(System.getProperty("user.dir") + File.separator + "tmpDir");

        if(!file.exists() && !file.createNewFile()){
            fail();
        }

        if(!tmpDir.mkdirs()){
            fail();
        }

        try(PrintWriter pw = new PrintWriter(new FileWriter(file))){

            for(XsamRecord r : records){
                pw.println(r);
            }

            pw.flush();
        }catch (IOException i){
            fail();
        }

        try {
            List<XsamChunk> chunks = XSamChunkOrganiser.split(file, tmpDir);
            assertTrue(chunks.size() > 0);


            int fileLengthCounter = 0;
            for(XsamChunk c : chunks){
                fileLengthCounter += c.getLocation().length();
                sorter.sort(SortingMethods.createDefaultPairStringComparator(), c, true);
            }

            assertEquals(file.length(), fileLengthCounter);

        }catch (IOException ioe){
            fail();
        }

        File[] f = tmpDir.listFiles();
        if (f != null) {
            for(File fi : f){
                if(!fi.delete()){
                    fail();
                }
            }
        }

        if(!tmpDir.delete()){
            fail();
        }


        if(!file.delete()){
            fail();
        }


    }


    @ParameterizedTest
    @MethodSource({"com.sbl.providers.XsamPairProviders#pairedXsamRecordProvider"})
    void givenALongListOfRecordPairs_whenSplit_checkSplittingCorrect(List<PairedXsamRecord> records) throws Exception{

        File file = new File(System.getProperty("user.dir") + File.separator + "inputRecords.Xsam");
        File tmpDir = new File(System.getProperty("user.dir") + File.separator + "tmpDir");

        if(!file.exists() && !file.createNewFile()){
            fail();
        }

        if(!tmpDir.mkdirs()){
            fail();
        }


        try(PrintWriter pw = new PrintWriter(new FileWriter(file))){

            for(PairedXsamRecord r : records){
                pw.println(r);
            }

            pw.flush();
        }catch (IOException i){
            fail();
        }

        try {
            List<XsamChunk> chunks = XSamChunkOrganiser.split(file, tmpDir);
            assertTrue(chunks.size() > 0);


            long fileLengthCounter = 0;
            for(XsamChunk c : chunks){
                fileLengthCounter += c.getLocation().length();
                sorter.sort(SortingMethods.createDefaultPairStringComparator(), c, false);
            }

            assertEquals(fileLengthCounter, file.length());


            System.out.println("here");
        }catch (IOException ioe){
            fail();
        }

        System.out.println("here");

        File[] f = tmpDir.listFiles();
        if (f != null) {
            for(File fi : f){
                if(!fi.delete()){
                    fail();
                }
            }
        }

        if(!tmpDir.delete()){
            fail();
        }


        if(!file.delete()){
            fail();
        }


    }

}