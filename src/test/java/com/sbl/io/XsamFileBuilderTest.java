package com.sbl.io;

import com.sbl.io.header.XsamHeaderLine;
import com.sbl.io.header.XsamSection;
import com.sbl.model.PairedXsamRecord;
import com.sbl.sorting.XsamChunkSorter;
import com.sbl.utils.XSamChunkOrganiser;
import com.sbl.model.XsamRecord;
import com.sbl.sorting.SortingMethods;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class XsamFileBuilderTest {

    File location = new File(System.getProperty("user.dir") + File.separator + "hello.chunk");
    private XsamChunkSorter sorter;

    @BeforeEach
    void setUp() {
        sorter = new XsamChunkSorter();
    }

    @ParameterizedTest
    @MethodSource({"com.sbl.providers.XsamRecordProviders#randomLongListOfRecords"})
    void givenALongListOfRecords_whenSplit_checkSplittingCorrect(List<XsamRecord> records) throws Exception{

        File file = new File(System.getProperty("user.dir") + File.separator + "inputRecords.Xsam");
        File tmpDir = new File(System.getProperty("user.dir") + File.separator + "tmpDir");

        if(!file.exists() && !file.createNewFile()){
            fail();
        }

        if(!tmpDir.mkdirs()){
            fail();
        }


        try(PrintWriter pw = new PrintWriter(new FileWriter(file))){

            for(XsamRecord r : records){
                pw.println(r);
            }

            pw.flush();
        }catch (IOException i){
            fail();
        }

        try {
            List<XsamChunk> chunks = XSamChunkOrganiser.split(file, tmpDir);
            assertTrue(chunks.size() > 0);

            int fileLengthCounter = 0;
            for(XsamChunk c : chunks){
                fileLengthCounter += c.getLocation().length();
                sorter.sort(SortingMethods.createDefaultRecordStringComparator(), c, true);
            }

            assertEquals(file.length(), fileLengthCounter);

            File out = new File(tmpDir + File.separator + "myOut.Xsam");

            if(!out.createNewFile()){
                fail();
            }

            XsamFileParameter param = new XsamFileParameter.XsamFileParameterBuilder("2.0").withDelete().build();
            File xf = XsamFileBuilder.buildFile(out, chunks, param);

            if(!xf.exists()){
                fail();
            }

            if(!out.delete()){
                fail();
            }

        }catch (IOException ioe){
            fail();
        }

        File[] f = tmpDir.listFiles();
        if (f != null) {
            for(File fi : f){
                if(!fi.delete()){
                    fail();
                }
            }
        }

        if(!tmpDir.delete()){
            fail();
        }


        if(!file.delete()){
            fail();
        }


    }

    @ParameterizedTest
    @MethodSource({"com.sbl.providers.XsamPairProviders#pairedXsamRecordProvider"})
    void givenALongListOfPairedRecords_whenSplit_checkSplittingCorrect(List<PairedXsamRecord> records) throws Exception{

        File file = new File(System.getProperty("user.dir") + File.separator + "inputRecordsPairs.Xsam");
        File tmpDir = new File(System.getProperty("user.dir") + File.separator + "tmpDirPairs");

        if(!file.exists() && !file.createNewFile()){
            fail();
        }

        if(!tmpDir.mkdirs()){
            fail();
        }


        try(PrintWriter pw = new PrintWriter(new FileWriter(file))){

            for(PairedXsamRecord r : records){
                pw.println(r);
            }

            pw.flush();
        }catch (IOException i){
            fail();
        }

        try {
            List<XsamChunk> chunks = XSamChunkOrganiser.split(file, tmpDir);
            assertTrue(chunks.size() > 0);

            int fileLengthCounter = 0;
            for(XsamChunk c : chunks){
                fileLengthCounter += c.getLocation().length();
                sorter.sort(SortingMethods.createDefaultRecordStringComparator(), c, false);
            }

            assertEquals(file.length(), fileLengthCounter);

            File out = new File(tmpDir + File.separator + "myOutPairs.Xsam");

            if(!out.createNewFile()){
                fail();
            }

            XsamFileParameter param = new XsamFileParameter.XsamFileParameterBuilder("2.0").withDelete().build();
            File xf = XsamFileBuilder.buildFile(out, chunks, param);

            if(!xf.exists()){
                fail();
            }

            if(!out.delete()){
                fail();
            }

        }catch (IOException ioe){
            fail();
        }

        File[] f = tmpDir.listFiles();
        if (f != null) {
            for(File fi : f){
                if(!fi.delete()){
                    fail();
                }
            }
        }

        if(!tmpDir.delete()){
            fail();
        }


        if(!file.delete()){
            fail();
        }


    }



    @Test
    void givenValidChunks_whenSortedInOrderOfrNameThenXsamSectionThenSpanSectionCheckSortingCorrect() throws IOException {

        if(!location.createNewFile()){
            fail();
        }

        List<XsamChunk> list = giveMeAnOutOfOrderAssortmentOfChunks();

        list.sort(XsamFileBuilder.createFileOrderComparator());

        int spanSectionPRedicator = 1;
        for(int i = 0; i < 8; i++){
            assertEquals(XsamSection.P, list.get(i).getSection());
            assertEquals(spanSectionPRedicator, list.get(i).getSpanSection());

            spanSectionPRedicator++;
            if(spanSectionPRedicator == 5){
                spanSectionPRedicator = 1;
            }

        }

        assertEquals(XsamSection.D, list.get(8).getSection());

    }

    @Test
    void givenMissingHeaderLines_whenFilled_checkFilledInCorrectly() {

        List<XsamHeaderLine> lines = new ArrayList<>();
        lines.add(new XsamHeaderLine(10, 20, 30, 1, XsamSection.P, "mm01"));
        lines.add(new XsamHeaderLine(10, 20, 30, 3, XsamSection.P, "mm01"));
        lines.add(new XsamHeaderLine(10, 20, 30, 4, XsamSection.P, "mm01"));

        List<XsamHeaderLine> filledIn = XsamFileBuilder.fillInMissingHeaderLines(lines);

        assertEquals(4, filledIn.size());


    }

    @Test
    void givenMissingHeaderLinesWithX_whenFilled_checkFilledInCorrectlyNoExtraXSections() {

        List<XsamHeaderLine> lines = new ArrayList<>();
        lines.add(new XsamHeaderLine(10, 20, 30, 1, XsamSection.P, "mm01"));
        lines.add(new XsamHeaderLine(10, 20, 30, 3, XsamSection.P, "mm01"));
        lines.add(new XsamHeaderLine(10, 20, 30, 4, XsamSection.P, "mm01"));
        lines.add(new XsamHeaderLine(10, 20, 30, 1, XsamSection.X, "*"));

        List<XsamHeaderLine> filledIn = XsamFileBuilder.fillInMissingHeaderLines(lines);

        assertEquals(5, filledIn.size());


    }

    private List<XsamChunk> giveMeAnOutOfOrderAssortmentOfChunks(){
        return Arrays.asList(
                new XsamChunk(100, location, 1, XsamSection.P, "mm01"),
                new XsamChunk(100, location, 2, XsamSection.P, "mm01"),
                new XsamChunk(100, location, 1, XsamSection.P, "mm02"),
                new XsamChunk(100, location, 2, XsamSection.P, "mm02"),
                new XsamChunk(100, location, 1, XsamSection.D, "mm01"),
                new XsamChunk(100, location, 2, XsamSection.D, "mm01"),
                new XsamChunk(100, location, 3, XsamSection.D, "mm01"),
                new XsamChunk(100, location, 4, XsamSection.D, "mm01"),
                new XsamChunk(100, location, 1, XsamSection.D, "mm02"),
                new XsamChunk(100, location, 2, XsamSection.D, "mm02"),
                new XsamChunk(100, location, 3, XsamSection.D, "mm02"),
                new XsamChunk(100, location, 4, XsamSection.D, "mm02"),
                new XsamChunk(100, location, 3, XsamSection.P, "mm01"),
                new XsamChunk(100, location, 4, XsamSection.P, "mm01"),
                new XsamChunk(100, location, 3, XsamSection.P, "mm02"),
                new XsamChunk(100, location, 4, XsamSection.P, "mm02")
        );
    }

    @AfterEach
    void tearDown() {
        if(location.exists() && !location.delete()){
            fail();
        }
    }
}