package com.sbl.io;

import com.sbl.io.writers.XsamChunkWriter;
import com.sbl.io.writers.XsamChunkWriterImpl;
import com.sbl.model.XsamRecord;
import com.sbl.providers.SampleXsamFileCreator;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;


class XsamFileTest {

  private static File xsamFile = new File(System.getProperty("user.dir") + File.separator + "myXsamFileTestFile.Xsam");
  private static File xsamSmolFile = new File(System.getProperty("user.dir") + File.separator + "myXsamFileTestFileSmol.Xsam");
  private static XsamFile fileRep;
  private static XsamFile smolFile;
  private static XsamChunkWriter writer;

  private static File dir = new File(System.getProperty("user.dir") + File.separator + "tempDirrrr");

  @BeforeAll
  static void setUp() throws Exception {

    if(!xsamFile.createNewFile()){
      fail();
    }

    if(!xsamSmolFile.createNewFile()){
      fail();
    }

    if(!dir.mkdirs()){
      fail();
    }

    try{
      SampleXsamFileCreator.createFakeXsamHeader(xsamFile);
      SampleXsamFileCreator.writeSmolXsamFile(xsamSmolFile);
    }catch (IOException ioe){
      fail();
    }

    writer = new XsamChunkWriterImpl();
    fileRep = new XsamFile(xsamFile, writer);

  }

  @Test
  void givenValidXsamHeader_whenXsamFileInstantiated_checkHeaderLinesFoundSuccessfully() throws Exception {



    assertEquals(228, fileRep.getpSection().size());
    assertEquals(0, fileRep.getdSection().size());
    assertNull( fileRep.getUnmapped());

  }

  @Test
  void whenNullFiledPassed_checkErrorThrown() {

    assertThrows(IllegalArgumentException.class, () -> new XsamFile(null, null), "Input file cannot be null");
  }

  @Test
  void whenNonExistantFiledPassed_checkErrorThrown() {

    assertThrows(IllegalArgumentException.class, () -> new XsamFile(new File("hello!!" + System.nanoTime()), null), "Input file must exist");
  }

  @Test
  void whenEmptyFilePassed_checkErrorThrown() {

    File myFile = new File(System.getProperty("user.dir") + File.separator + "emptyfileXsamFileTest" + System.nanoTime());

    try {

      if(!myFile.createNewFile() || !myFile.exists()){
        fail();
      }

      assertThrows(IllegalArgumentException.class, () -> new XsamFile(myFile, null), "Input file cannot be empty");

    }catch (IOException ioe){

      fail();

    }finally {
      if(!myFile.delete()){
        myFile.deleteOnExit();
      }
    }
  }


  @Test
  void givenValidXsamHeader_whenParsed_checkParsingOfSpanLimitsCorrect() throws Exception {

    XsamFile fileRep = new XsamFile(xsamFile, writer);

    assertEquals(2000, fileRep.getSection1Limit());
    assertEquals(100000, fileRep.getSection2Limit());
    assertEquals(3000000, fileRep.getSection3Limit());

  }

  @Test
  void givenValidXsamHeader_whenParsed_checkSamAndXsamHeaderLinesSplitCorrectly() {

    assertEquals(108, fileRep.getSamHeaderReads().size());
    assertEquals(234, fileRep.getxSamHeaderReads().size());

  }

  @Test
  void givenValidSmallXsamFile_whenParsedAndChunkWritten_checkChunkWrittenSuccessfully() throws Exception {

    smolFile = new XsamFile(xsamSmolFile, writer);

    smolFile.splitFiles(dir);

    int inCount = 0;
    for(XsamChunk x : smolFile.getChunks()){
      if(x != null){
        inCount ++;
        assertEquals(4479, x.getLocation().length());

        int i = 0;
        try(BufferedReader br = new BufferedReader(new FileReader(x.getLocation()))){

          String line;

          while((line = br.readLine()) != null){

            //check the first line
            if(i == 0){

              assertEquals("SBL_XSMJR267_ID:1884132_BC1:CGCAG_\t81\tmm01\t24684050\t30\t39M\t=\t24684188\t175\tTGGTGGTGGTTATAGACCCCAGGTAGTGGTGGTTACAGN\tIIIIIIIIIIIIIIIHIIIIIIIIIIIIIIIIIIIIII~\txl:i:24684050\txr:i:24684088\txs:i:39\txd:A:r\txm:A:u\txa:A:\"\"\t\txL:i:24684050\txR:i:24684224\txLseq:i:CAGG\txRseq:i:GACA\txS:i:175\txW:i:99\txP:i:0\txQ:i:0\txC:A:\"\"\txD:A:\"\"\tPG:Z:novoalign\tAS:i:6\tUQ:i:6\tNM:i:1\tMD:Z:38A0\t", line);

            }

            //last line
            if(i == 11){
              assertEquals("SBL_XSMJR271_ID:212165_BC1:TGCAG_\t163\tmm01\t189915571\t70\t5S35M1S\t=\t189915571\t36\tNNAGGTCTGAGAACACAGGGGCTAGGCTAGTAACTAAAGCA\t~~IHHHIIIIHIIIIIIIIIIHIIHIIIIIHHIIIIIIIII\txl:i:189915571\txr:i:189915605\txs:i:35\txd:A:f\txm:A:u\txa:A:\"\"\t\txL:i:189915571\txR:i:189915605\txLseq:i:ACTC\txRseq:i:CCCT\txS:i:35\txW:i:-36\txP:i:0\txQ:i:0\txC:A:\"\"\txD:A:\"\"\tPG:Z:novoalign\tAS:i:100\tUQ:i:100\tNM:i:0\tMD:Z:35\tPQ:i:173\tSM:i:53\tAM:i:53\t", line);
            }

            i++;
          }

        }

      }
    }

    //expected 1 of 4 io to contain data
    assertEquals(1, inCount);

  }

  @Test
  void givenValidSmallXsamFile_whenParsedAndChunkWrittenWithoutDirectorySpecified_checkChunkWrittenSuccessfully() throws Exception {

    smolFile = new XsamFile(xsamSmolFile, writer);

    smolFile.splitFiles();

    int inCount = 0;
    for(XsamChunk x : smolFile.getChunks()){
      if(x != null){
        inCount++;
        assertEquals(4479, x.getLocation().length());
        if(!x.getLocation().delete()){
          fail();
        }
      }
    }

    //expected 1 of 4 io to contain data
    assertEquals(1, inCount);

    if(smolFile.getSubFileDirectory() == null){
      fail();
    }

    File[] files = smolFile.getSubFileDirectory().listFiles();

    if(files == null){
      fail();
    }

    if(!smolFile.getSubFileDirectory().delete()){
      fail();
    }


  }

  @AfterAll
  static void tearDown() throws Exception {
    if(xsamFile.exists()){
      if(!xsamFile.delete()){
        fail();
      }
    }

    if(xsamSmolFile.exists()){
      if(!xsamSmolFile.delete()){
        fail();
      }
    }

    if(dir.exists()){

      File[] list = dir.listFiles();

      if(list != null){
        for(File f : list){
          if(!f.delete()){
            fail();
          }
        }
      }


      if(!dir.delete()){
        fail();
      }
    }
  }
}