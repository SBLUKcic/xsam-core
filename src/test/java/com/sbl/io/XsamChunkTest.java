package com.sbl.io;

import com.sbl.io.header.XsamSection;
import com.sbl.model.PairedXsamRecord;
import com.sbl.model.XsamRecord;
import com.sbl.sorting.SortingMethods;
import com.sbl.sorting.XsamChunkSorter;
import com.sbl.utils.XsamReadQueries;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class XsamChunkTest {

    private File tempDir = new File(System.getProperty("user.dir") + File.separator + "myFakeDir");
    private File location = new File(tempDir + File.separator + "myFakeXsamChunk");

    private XsamChunk xsamChunk;

    private List<String> stringList = new ArrayList<>();

    @BeforeEach
    void setUp() throws Exception{

        if(!tempDir.mkdirs() && !tempDir.exists()){
            fail();
        }

        if(!location.createNewFile()){
            fail();
        }

        stringList.add("SBL_XSMJR321b_ID:256386\t99\tmm01\t3070848\t70\t2S39M\t=\t3070964\t155\tNNCAAATCCAACCTGAAAGCAGCTAGTTACTTTATAATGGT\t~~EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEAEEEEE\txl:i:3070848\txr:i:3070886\txs:i:39\txd:A:f\txm:A:u\txa:A:\"\"\tPG:Z:novoalign\tAS:i:12\tUQ:i:12\tNM:i:0\tMD:Z:39\tPQ:i:19\tSM:i:70\tAM:i:70");
        stringList.add("SBL_XSMJR321b_ID:256383\t147\tmm01\t3070964\t70\t39M\t=\t3070848\t-155\tATCAGCAGTCTGCACAGCACCTTCCAGAGCTAGGAGTTN\tEEEEEEEEEEAAEEEEEEEEEEEEEEEEEEEEEEEEEE~\txl:i:3070964\txr:i:3071002\txs:i:39\txd:A:r\txm:A:u\txa:A:\"\"\tPG:Z:novoalign\tAS:i:6\tUQ:i:6\tNM:i:1\tMD:Z:38T0\tPQ:i:19\tSM:i:70\tAM:i:70");

        stringList.add("SBL_XSMJR322b_ID:3740\t99\tmm01\t3199893\t70\t1S38M\t=\t3200042\t190\tNTATGAAGTACTACTTTCCACTTCATTTCATCACAAATT\t~EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE\txl:i:3199893\txr:i:3199930\txs:i:38\txd:A:f\txm:A:u\txa:A:\"\"\tPG:Z:novoalign\tAS:i:6\tUQ:i:6\tNM:i:0\tMD:Z:38\tPQ:i:18\tSM:i:70\tAM:i:70");
        stringList.add("SBL_XSMJR322c_ID:3741\t147\tmm01\t3200042\t70\t41M\t=\t3199893\t-190\tAACCAAGAAAGACACACTTACAAGTTGAATTCTAAAGGANN\tEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE~~\txl:i:3200042\txr:i:3200082\txs:i:41\txd:A:r\txm:A:u\txa:A:\"\"\tPG:Z:novoalign\tAS:i:12\tUQ:i:12\tNM:i:2\tMD:Z:39A0G0\tPQ:i:18\tSM:i:70\tAM:i:70");

        stringList.add("SBL_XSMJR320b_ID:2293779\t65\tmm01\t3014444\t70\t2S39M\t=\t3014575\t170\tNNATTGTTCCTTTAAAAATTTTCTTTTGACCCCTTCTGTTC\t~~EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEAEE<EEEEE\txl:i:3014444\txr:i:3014482\txs:i:39\txd:A:f\txm:A:u\txa:A:\"\"\tPG:Z:novoalign\tAS:i:12\tUQ:i:12\tNM:i:0\tMD:Z:39");
        stringList.add("SBL_XSMJR320b_ID:2293778\t129\tmm01\t3014575\t57\t39M\t=\t3014444\t-170\tGAACACAACTTCTGTTCCAATCTAATCGTGCAAGACCGN\tEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE~\txl:i:3014575\txr:i:3014613\txs:i:39\txd:A:f\txm:A:u\txa:A:\"\"\tPG:Z:novoalign\tAS:i:6\tUQ:i:6\tNM:i:1\tMD:Z:38G0");

        try(PrintWriter pw = new PrintWriter(new FileWriter(location))){

            for(String s : stringList){
                pw.println(s);
            }

            pw.flush();
        }catch (IOException ioe){
            fail();
        }

        xsamChunk = new XsamChunk(6, location, 1, XsamSection.P, "mm01");
    }

    @Test
    void givenValidXsamChunk_whenSorted_checkOutputChunkSortedCorrectly() throws Exception {

        Collections.sort(stringList, SortingMethods.createDefaultRecordStringComparator());

        XsamChunkSorter.sort(SortingMethods.createDefaultRecordStringComparator(), xsamChunk, true);

        assertTrue(xsamChunk.getLocation().exists());

    }

    @ParameterizedTest
    @MethodSource({"com.sbl.providers.XsamRecordProviders#randomLongListOfRecords"})
    void givenALongListOfRecords_whenSortedByChunkSort_checkSortingCorrect(List<XsamRecord> records) throws Exception{

        File f = new File(tempDir + File.separator + "somethingSortTEst");

        if(!f.createNewFile()){
            fail();
        }

        try(PrintWriter printWriter = new PrintWriter(new FileWriter(f))){

            for(XsamRecord r : records){
                printWriter.println(r);
            }

            printWriter.flush();
        }


        XsamChunk chunk = new XsamChunk(records.size(), f, 1, XsamSection.P, "mm01");

        XsamChunkSorter.sort(SortingMethods.createDefaultRecordStringComparator(), chunk, true);

        List<String> sorted = new ArrayList<>();

        try(BufferedReader reader = new BufferedReader(new FileReader(chunk.getLocation()))){

            String line;

            while((line = reader.readLine()) != null ){
                sorted.add(line);
            }


        }


        for(int i = 0; i < sorted.size()-1; i++){
            assertTrue(XsamReadQueries.findxlField(sorted.get(i)) <= XsamReadQueries.findxlField(sorted.get(i+1)));
        }

        if(!f.delete()){
            fail();
        }

    }


    @ParameterizedTest
    @MethodSource("com.sbl.providers.XsamPairProviders#pairedXsamRecordProvider")
    void givenLongListOfRecordPairs_whenSorted_checkSortingCorrect(List<PairedXsamRecord> records) throws Exception{

        File f = new File(tempDir + File.separator + "somethingSortTEst2");

        if(!f.createNewFile()){
            fail();
        }

        try(PrintWriter printWriter = new PrintWriter(new FileWriter(f))){

            for(PairedXsamRecord r : records){
                printWriter.println(r);
            }

            printWriter.flush();
        }


        XsamChunk chunk = new XsamChunk(records.size(), f, 1, XsamSection.P, "mm01");

        XsamChunkSorter.sort(chunk, false);

        List<String> sorted = new ArrayList<>();

        try(BufferedReader reader = new BufferedReader(new FileReader(chunk.getLocation()))){

            String line;

            while((line = reader.readLine()) != null ){
                sorted.add(line);
            }


        }


        for(int i = 0; i < sorted.size()-3; i+=2){
            assertTrue(XsamReadQueries.findxLField(sorted.get(i)) <= XsamReadQueries.findxLField(sorted.get(i+2)));
            assertEquals(XsamReadQueries.findID(sorted.get(i)), XsamReadQueries.findID(sorted.get(i+1)));
            assertEquals(XsamReadQueries.findID(sorted.get(i+2)), XsamReadQueries.findID(sorted.get(i+3)));
        }

        if(!f.delete()){
            fail();
        }

    }

    @AfterEach
    void tearDown() {
        if(location.exists()){
            if(!location.delete()){
                fail();
            }
        }

        if(!tempDir.delete()){
            fail();
        }
    }


}