package com.sbl.io.header;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class XsamHeaderLineTest {

    @Test
    void givenOutOfOrderHeaderLines_whenSorted_checkSortingCorrect() {

        XsamHeaderLine line = new XsamHeaderLine(10, 20, 30, 1, XsamSection.P, "mm01");
        XsamHeaderLine line2 = new XsamHeaderLine(10, 20, 50, 3, XsamSection.P, "mm01");
        XsamHeaderLine line3 = new XsamHeaderLine(10, 20, 70, 2, XsamSection.P, "mm01");
        XsamHeaderLine linemm02 = new XsamHeaderLine(10, 20, 90, 1, XsamSection.P, "mm02");
        XsamHeaderLine line4 = new XsamHeaderLine(10, 20, 110, 3, XsamSection.D, "mm01");
        XsamHeaderLine line5 = new XsamHeaderLine(10, 20, 130, 1, XsamSection.D, "mm01");
        XsamHeaderLine line6 = new XsamHeaderLine(10, 20, 150, 2, XsamSection.D, "mm01");
        XsamHeaderLine line7 = new XsamHeaderLine(10, 20, 170, 1, XsamSection.X, "*");

        List<XsamHeaderLine> lines = Arrays.asList(line, line2, line3, line4, line5, line6, line7,  linemm02);

        Collections.sort(lines);

        assertEquals(lines.get(0), line);
        assertEquals(lines.get(1), line3);
        assertEquals(lines.get(2), line2);
        assertEquals(lines.get(3), linemm02);
        assertEquals(lines.get(4), line5);
        assertEquals(lines.get(5), line6);
        assertEquals(lines.get(6), line4);
        assertEquals(lines.get(7), line7);


    }

    @Test
    void givenXSectionHeaderLine_whenWrittenToString_checkRnameNOTIncluded() {

        XsamHeaderLine xone = new XsamHeaderLine(100, 200, 0, 1, XsamSection.X, "*");

        assertEquals("@CO\tXsam:X:100:200:0", xone.toString());

    }
}