package com.sbl.filtering.predicates;

import com.sbl.filtering.model.Range;
import com.sbl.model.footprints.XsamRecordFootprintCombination;
import com.sbl.model.xsamstrands.XsamRecordStrand;
import com.sbl.model.xsamtypes.XsamRecordMappingType;
import com.sbl.model.XsamRecord;
import com.sbl.model.footprints.XsamRecordFootprint;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.function.Predicate;

import static org.junit.jupiter.api.Assertions.*;

class XsamRecordPredicatesTest {

  @ParameterizedTest
  @MethodSource({"com.sbl.providers.XsamRecordProviders#positionedRecordStream", "com.sbl.providers.XsamRecordProviders#nonMatchRecordStream"})
  void givenSpanLimitPredicated_whenTested_checkTrue(XsamRecord record) {

    Predicate<XsamRecord> spanLimitPred = XsamRecordPredicates.createSpanInRangePredicate(new Range(38, 40));

    if(record.getXs() >= 38 && record.getXs() <= 40){
      assertTrue(spanLimitPred.test(record));
    }else{
      assertFalse(spanLimitPred.test(record));
    }


  }

  @ParameterizedTest
  @MethodSource({"com.sbl.providers.XsamRecordProviders#positionedRecordStream", "com.sbl.providers.XsamRecordProviders#nonMatchRecordStream"})
  void givenFootprintCheckLimitPredicated_whenTested_checkTrue(XsamRecord record) {

    Predicate<XsamRecord> LbCheck = XsamRecordPredicates.createFootprintInRangePredicate(XsamRecordFootprint.Lb, new Range(188710023, 188710040));

    if(record.getXl() >= 188710023 && record.getXl() <= 188710040){
      assertTrue(LbCheck.test(record));
    }else{
      assertFalse(LbCheck.test(record));
    }


    Predicate<XsamRecord> RbCheck = XsamRecordPredicates.createFootprintInRangePredicate(XsamRecordFootprint.Rb, new Range(188710112, 188710112));

    if(record.getXr() >= 188710112 && record.getXr() <= 188710112){
      assertTrue(RbCheck.test(record));
    }else{
      assertFalse(RbCheck.test(record));
    }

    Predicate<XsamRecord> prime5Check = XsamRecordPredicates.createFootprintInRangePredicate(XsamRecordFootprint.prime5, new Range(5, Integer.MAX_VALUE));

    assertTrue(prime5Check.test(record));

  }

  @ParameterizedTest
  @MethodSource({"com.sbl.providers.XsamRecordProviders#positionedRecordStream", "com.sbl.providers.XsamRecordProviders#nonMatchRecordStream"})
  void givenFootprintCombinationCheckLimitPredicated_whenTested_checkTrue(XsamRecord record) {

    Predicate<XsamRecord> AbCheck = XsamRecordPredicates.createFootprintInRangePredicate(XsamRecordFootprintCombination.Ab, new Range(188710023, 188710040));

    if((record.getXl() >= 188710023 && record.getXl() <= 188710040) || (record.getXr() >= 188710023 && record.getXr() <= 188710040)){
      assertTrue(AbCheck.test(record));
    }else{
      assertFalse(AbCheck.test(record));
    }

  }

  @ParameterizedTest
  @MethodSource({"com.sbl.providers.XsamRecordProviders#positionedRecordStream", "com.sbl.providers.XsamRecordProviders#nonMatchRecordStream"})
  void givenTypeCheckPredicated_whenTested_checkTrue(XsamRecord record) {

    Predicate<XsamRecord> typeCheck = XsamRecordPredicates.createMappingTypePredicate(XsamRecordMappingType.P);

    if(record.getXm() == 'u' || record.getXm() == 'r'){
      assertTrue(typeCheck.test(record));
    }else{
      assertFalse(typeCheck.test(record));
    }

    Predicate<XsamRecord> typeCheck2 = XsamRecordPredicates.createMappingTypePredicate(XsamRecordMappingType.Z);

    if(record.getXm() == 'x' || record.getXm() == 'r'){
      assertTrue(typeCheck2.test(record));
    }else{
      assertFalse(typeCheck2.test(record));
    }

  }

  @ParameterizedTest
  @MethodSource({"com.sbl.providers.XsamRecordProviders#positionedRecordStream", "com.sbl.providers.XsamRecordProviders#nonMatchRecordStream"})
  void givenStrandCheckPredicated_whenTested_checkTrue(XsamRecord record) {

    Predicate<XsamRecord> strandCheck = XsamRecordPredicates.createMappingStrandPredicate(XsamRecordStrand.f);

    assertNotEquals(0 , record.getXd());

    if(record.getXd() == 'f'){
      assertTrue(strandCheck.test(record));
    }else{
      assertFalse(strandCheck.test(record));
    }

  }

  @ParameterizedTest
  @MethodSource({"com.sbl.providers.XsamRecordProviders#positionedRecordStream", "com.sbl.providers.XsamRecordProviders#nonMatchRecordStream"})
  void givenRNamePredicated_whenTested_checkTrue(XsamRecord record) {

    Predicate<XsamRecord> rnameCheck = XsamRecordPredicates.createReferenceNameMatchPredicate("mm01");

    if(record.getReferenceName().equals("mm01")){
      assertTrue(rnameCheck.test(record));
    }else{
      assertFalse(rnameCheck.test(record));
    }

  }



  @ParameterizedTest
  @MethodSource({"com.sbl.providers.XsamRecordProviders#positionedRecordStream", "com.sbl.providers.XsamRecordProviders#nonMatchRecordStream"})
  void givenSingleQualityPredicated_whenAllSinglesAboveThreshold_checkTrue(XsamRecord record) {

    int lowest = Integer.MAX_VALUE;

    for(int i = 0; i < record.getQuality().length(); i++){
      lowest = record.getQuality().charAt(i) < lowest ? record.getQuality().charAt(i) : lowest;
    }

    Predicate<XsamRecord> avQualCheck = XsamRecordPredicates.createAllQualityAboveThresholdPredicate(lowest);

    assertTrue(avQualCheck.test(record));

  }

  @ParameterizedTest
  @MethodSource({"com.sbl.providers.XsamRecordProviders#positionedRecordStream", "com.sbl.providers.XsamRecordProviders#nonMatchRecordStream"})
  void givenSingleQualityPredicated_whenSingleBelowThreshold_checkTrue(XsamRecord record) {

    Predicate<XsamRecord> avQualCheck = XsamRecordPredicates.createAverageQualityAboveThresholdPredicate(255);

    assertFalse(avQualCheck.test(record));

  }

}