package com.sbl.filtering.predicates;

import com.sbl.filtering.model.Range;
import com.sbl.model.PairedXsamRecord;
import com.sbl.model.footprints.PairedXsamRecordFootprint;
import com.sbl.model.footprints.PairedXsamRecordFootprintCombination;
import com.sbl.model.xsamtypes.PairedXsamRecordMappingCombination;
import com.sbl.model.xsamtypes.PairedXsamRecordMappingType;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.function.Predicate;

import static org.junit.jupiter.api.Assertions.*;

class PairedXsamRecordPredicatesTest {

    @ParameterizedTest
    @MethodSource({"com.sbl.providers.XsamPairProviders#ppePairProvider", "com.sbl.providers.XsamPairProviders#ppdPairProvider", "com.sbl.providers.XsamPairProviders#xxPairProvider"})
    void givenRecordLEFootprintISinRange_whenTested_checkTrueReturned(PairedXsamRecord record) {

        Range range = new Range(record.getxL() - 10, record.getxL() + 10);

        Predicate<PairedXsamRecord> pred = PairedXsamRecordPredicates.createFootprintInRangePredicate(PairedXsamRecordFootprint.LE, range);

        assertTrue(pred.test(record));

    }

    @ParameterizedTest
    @MethodSource({"com.sbl.providers.XsamPairProviders#ppePairProvider", "com.sbl.providers.XsamPairProviders#ppdPairProvider", "com.sbl.providers.XsamPairProviders#xxPairProvider"})
    void givenRecordREFootprintISinRange_whenTested_checkTrueReturned(PairedXsamRecord record) {

        Range range = new Range(record.getxR() - 10, record.getxR() + 10);

        Predicate<PairedXsamRecord> pred = PairedXsamRecordPredicates.createFootprintInRangePredicate(PairedXsamRecordFootprint.RE, range);

        assertTrue(pred.test(record));

    }

    @ParameterizedTest
    @MethodSource({"com.sbl.providers.XsamPairProviders#ppePairProvider", "com.sbl.providers.XsamPairProviders#ppdPairProvider", "com.sbl.providers.XsamPairProviders#xxPairProvider"})
    void givenRecordLIFootprintISinRange_whenTested_checkTrueReturned(PairedXsamRecord record) {

        int internalFootprint = record.getxL() + record.getRecord1().getSequence().length();
        Range range = new Range(internalFootprint - 10, internalFootprint + 10);

        Predicate<PairedXsamRecord> pred = PairedXsamRecordPredicates.createFootprintInRangePredicate(PairedXsamRecordFootprint.LI, range);

        assertTrue(pred.test(record));

    }

    @ParameterizedTest
    @MethodSource({"com.sbl.providers.XsamPairProviders#ppePairProvider", "com.sbl.providers.XsamPairProviders#ppdPairProvider", "com.sbl.providers.XsamPairProviders#xxPairProvider"})
    void givenRecordRIFootprintISinRange_whenTested_checkTrueReturned(PairedXsamRecord record) {

        int internalFootprintRight = record.getxR() - record.getRecord2().getSequence().length();
        Range range = new Range(internalFootprintRight - 10, internalFootprintRight + 10);

        Predicate<PairedXsamRecord> pred = PairedXsamRecordPredicates.createFootprintInRangePredicate(PairedXsamRecordFootprint.RI, range);

        assertTrue(pred.test(record));

    }

    @ParameterizedTest
    @MethodSource({"com.sbl.providers.XsamPairProviders#ppePairProvider", "com.sbl.providers.XsamPairProviders#ppdPairProvider", "com.sbl.providers.XsamPairProviders#xxPairProvider"})
    void givenRecordAECombinationISinRange_whenTested_checkTrueReturned(PairedXsamRecord record) {

        Range range = new Range(record.getxL() - 10, record.getxL() + 10);
        Range rightRange = new Range(record.getxR() - 10, record.getxR() + 10);

        Predicate<PairedXsamRecord> predLeft = PairedXsamRecordPredicates.createFootprintInRangePredicate(PairedXsamRecordFootprintCombination.AE, range);
        Predicate<PairedXsamRecord> predRight = PairedXsamRecordPredicates.createFootprintInRangePredicate(PairedXsamRecordFootprintCombination.AE, rightRange);

        assertTrue(predLeft.test(record));
        assertTrue(predRight.test(record));

    }

    @ParameterizedTest
    @MethodSource({"com.sbl.providers.XsamPairProviders#ppePairProvider", "com.sbl.providers.XsamPairProviders#ppdPairProvider", "com.sbl.providers.XsamPairProviders#xxPairProvider"})
    void givenRecordAICombinationISinRange_whenTested_checkTrueReturned(PairedXsamRecord record) {

        int internalFootprintLeft = record.getxL() + record.getRecord1().getSequence().length();
        int internalFootprintRight = record.getxR() - record.getRecord2().getSequence().length();

        Range range = new Range(internalFootprintLeft - 10, internalFootprintLeft + 10);
        Range rightRange = new Range(internalFootprintRight - 10, internalFootprintRight + 10);

        Predicate<PairedXsamRecord> predLeft = PairedXsamRecordPredicates.createFootprintInRangePredicate(PairedXsamRecordFootprintCombination.AI, range);
        Predicate<PairedXsamRecord> predRight = PairedXsamRecordPredicates.createFootprintInRangePredicate(PairedXsamRecordFootprintCombination.AI, rightRange);

        assertTrue(predLeft.test(record));
        assertTrue(predRight.test(record));

    }

    @ParameterizedTest
    @MethodSource({"com.sbl.providers.XsamPairProviders#ppePairProvider", "com.sbl.providers.XsamPairProviders#ppdPairProvider", "com.sbl.providers.XsamPairProviders#xxPairProvider"})
    void givenRecordSpanLengthInRange_whenTested_checkTrueReturned(PairedXsamRecord record) {


        Range spanRange = new Range(record.getxS()- 10, record.getxS() + 10);

        Predicate<PairedXsamRecord> spanPred = PairedXsamRecordPredicates.createSpanLengthInRangePredicate(spanRange);

        assertTrue(spanPred.test(record));


    }

    @ParameterizedTest
    @MethodSource({"com.sbl.providers.XsamPairProviders#ppePairProvider", "com.sbl.providers.XsamPairProviders#ppdPairProvider", "com.sbl.providers.XsamPairProviders#xxPairProvider"})
    void givenRecordSpanLengthNotInRange_whenTested_checkFalseReturned(PairedXsamRecord record) {


        Range spanRange = new Range(record.getxS()-100, record.getxS() -90);

        Predicate<PairedXsamRecord> spanPred = PairedXsamRecordPredicates.createSpanLengthInRangePredicate(spanRange);

        assertFalse(spanPred.test(record));

    }

    @ParameterizedTest
    @MethodSource({"com.sbl.providers.XsamPairProviders#ppePairProvider"})
    void givenRecordTypeValid_whenTested_checkTrueReturned(PairedXsamRecord record) {

        //both pairs provided have UUE types.
        Predicate<PairedXsamRecord> pred = PairedXsamRecordPredicates.createMappingTypePredicate(PairedXsamRecordMappingType.UUE);

        assertTrue(pred.test(record));

    }

    @ParameterizedTest
    @MethodSource({"com.sbl.providers.XsamPairProviders#ppePairProvider"})
    void givenRecordTypeNotValidBecauseOfTypes_whenTested_checkFalseReturned(PairedXsamRecord record) {

        //both pairs provided have UUE types.
        Predicate<PairedXsamRecord> pred = PairedXsamRecordPredicates.createMappingTypePredicate(PairedXsamRecordMappingType.URE);

        assertFalse(pred.test(record));

    }

    @ParameterizedTest
    @MethodSource({"com.sbl.providers.XsamPairProviders#ppePairProvider"})
    void givenRecordTypeNotValidBecauseOfReferenceNameMatching_whenTested_checkFalseReturned(PairedXsamRecord record) {

        //both pairs provided have UUE types.
        Predicate<PairedXsamRecord> pred = PairedXsamRecordPredicates.createMappingTypePredicate(PairedXsamRecordMappingType.UUD);

        assertFalse(pred.test(record));

    }


    @ParameterizedTest
    @MethodSource({"com.sbl.providers.XsamPairProviders#ppePairProvider"})
    void givenRecordCombinationTypeValid_whenTested_checkTrueReturned(PairedXsamRecord record) {

        //both pairs provided have UUE types.
        Predicate<PairedXsamRecord> pred = PairedXsamRecordPredicates.createMappingCombinationPredicate(PairedXsamRecordMappingCombination.PPE);

        assertTrue(pred.test(record));

    }

    @ParameterizedTest
    @MethodSource({"com.sbl.providers.XsamPairProviders#ppePairProvider"})
    void givenRecordCombinationTypeNotValid_whenTested_checkFalseReturned(PairedXsamRecord record) {

        //both pairs provided have UUE types.
        Predicate<PairedXsamRecord> pred = PairedXsamRecordPredicates.createMappingCombinationPredicate(PairedXsamRecordMappingCombination.PPD);

        assertFalse(pred.test(record));

    }

    @ParameterizedTest
    @MethodSource({"com.sbl.providers.XsamPairProviders#ppePairProvider", "com.sbl.providers.XsamPairProviders#ppdPairProvider", "com.sbl.providers.XsamPairProviders#xxPairProvider"})
    void givenRecordsWithMatchingAndNonMatchingRNames_whenTested_checkCorrectReturned(PairedXsamRecord record) {


        Predicate<PairedXsamRecord> rnamePred = PairedXsamRecordPredicates.createMatchingReferenceNamesPredicate();

        if(record.areReferenceNamesMatching()) {
           assertTrue(rnamePred.test(record));
        }else{
            assertFalse(rnamePred.test(record));
        }

    }

    @ParameterizedTest
    @MethodSource({"com.sbl.providers.XsamPairProviders#ppePairProvider", "com.sbl.providers.XsamPairProviders#ppdPairProvider", "com.sbl.providers.XsamPairProviders#xxPairProvider"})
    void givenRecordsWithAverageQualityAbove40000_whenTested_checkFalseReturned(PairedXsamRecord record) {

        Predicate<PairedXsamRecord> averageQualityPredicate = PairedXsamRecordPredicates.createAverageQualityAboveThresholdPredicate(40000);

        assertFalse(averageQualityPredicate.test(record));

    }

    @ParameterizedTest
    @MethodSource({"com.sbl.providers.XsamPairProviders#ppePairProvider", "com.sbl.providers.XsamPairProviders#ppdPairProvider", "com.sbl.providers.XsamPairProviders#xxPairProvider"})
    void givenRecordsWithAverageQualityAbove10_whenTested_checkTrueReturned(PairedXsamRecord record) {

        Predicate<PairedXsamRecord> averageQualityPredicate = PairedXsamRecordPredicates.createAverageQualityAboveThresholdPredicate(10);

        assertTrue(averageQualityPredicate.test(record));

    }

    @ParameterizedTest
    @MethodSource({"com.sbl.providers.XsamPairProviders#ppePairProvider", "com.sbl.providers.XsamPairProviders#ppdPairProvider", "com.sbl.providers.XsamPairProviders#xxPairProvider"})
    void givenRecordsWithAllQualityAbove40000_whenTested_checkFalseReturned(PairedXsamRecord record) {

        Predicate<PairedXsamRecord> averageQualityPredicate = PairedXsamRecordPredicates.createAllQualityAboveThresholdPredicate(40000);

        assertFalse(averageQualityPredicate.test(record));

    }

    @ParameterizedTest
    @MethodSource({"com.sbl.providers.XsamPairProviders#ppePairProvider", "com.sbl.providers.XsamPairProviders#ppdPairProvider", "com.sbl.providers.XsamPairProviders#xxPairProvider"})
    void givenRecordsWithAllQualityAbove10_whenTested_checkTrueReturned(PairedXsamRecord record) {

        Predicate<PairedXsamRecord> averageQualityPredicate = PairedXsamRecordPredicates.createAllQualityAboveThresholdPredicate(10);

        assertTrue(averageQualityPredicate.test(record));

    }



}