package com.sbl.filtering.criteria;

import com.sbl.filtering.model.Range;
import com.sbl.filtering.predicates.XsamRecordPredicates;
import com.sbl.model.XsamRecord;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;

import static org.junit.jupiter.api.Assertions.*;

class CoreOrCriteriaTest {

  @ParameterizedTest
  @MethodSource({"com.sbl.providers.XsamRecordProviders#positionedRecordStream", "com.sbl.providers.XsamRecordProviders#nonMatchRecordStream"})
  void givenSpanLimitAndPredicated_whenTested_checkTrue(XsamRecord record) {

    Predicate<XsamRecord> spanLimitPred = XsamRecordPredicates.createSpanInRangePredicate(new Range(0, 0));
    Predicate<XsamRecord> rnamePred = XsamRecordPredicates.createReferenceNameMatchPredicate("mm01");

    List<Predicate<XsamRecord>> list = Arrays.asList(spanLimitPred, rnamePred);

    CoreOrCriteria<XsamRecord> criteria = new CoreOrCriteria<>(list);

    List<XsamRecord> matches = criteria.meetCriteria(Collections.singletonList(record));

    //no reads have span 0, so all matches will depend on the reference name
    if((record.getXs() >= 0 && record.getXs() <= 0) || record.getReferenceName().equals("mm01")){
      assertEquals(1, matches.size());
    }else{
      assertEquals(0, matches.size());
    }

  }

}