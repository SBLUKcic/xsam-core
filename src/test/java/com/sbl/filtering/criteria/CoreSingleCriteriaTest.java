package com.sbl.filtering.criteria;


import com.sbl.filtering.model.Range;
import com.sbl.filtering.predicates.XsamRecordPredicates;
import com.sbl.model.XsamRecord;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;


class CoreSingleCriteriaTest {

  private static XsamRecord record1 = new XsamRecord("SBL_XSNP077_ID:184296_BC1:CTATTTCAG_\t99\tmm01\t188710023\t70\t2S39M\t=\t188710074\t90\tNNGAATAATAATAGTGGGGGATCCACTAGGGCTGTGGCTCC\t~~IIIIIIIIIIIIIIIIIIIIIIIIIIHHHIIIIIIIIII\txl:i:188710023\txr:i:188710061\txs:i:39\txd:A:f\txm:A:u\txa:A:\"\"\t\txL:i:188710023\txR:i:188710112\txS:i:90\txW:i:12\txP:i:0\txQ:i:0\txC:A:\"\"\txD:A:\"\"\tPG:Z:novoalign\tAS:i:12\tUQ:i:12\tNM:i:0\tMD:Z:39\tPQ:i:20\tSM:i:70\tAM:i:70");
  private static XsamRecord record2 = new XsamRecord("SBL_XSNP077_ID:184296_BC1:CTATTTCAG_\t147\tmm01\t188710074\t70\t39M\t=\t188710023\t-90\tTCCTTGGTCAGCTGTGTTTTAACTTGAGGAGGCCTTCCN\tIHIIIIIIIIIIIIIIIIIHIHIIIIIIIIIIIIIIII~\txl:i:188710074\txr:i:188710112\txs:i:39\txd:A:r\txm:A:u\txa:A:\"\"\t\txL:i:188710023\txR:i:188710112\txS:i:90\txW:i:12\txP:i:0\txQ:i:0\txC:A:\"\"\txD:A:\"\"\tPG:Z:novoalign\tAS:i:6\tUQ:i:6\tNM:i:1\tMD:Z:38A0\tPQ:i:20\tSM:i:70\tAM:i:70");

  private static List<XsamRecord> singles;

  private CoreSingleCriteria<XsamRecord> criteria;

  @BeforeAll
  static void setUp() throws Exception {

    singles = Arrays.asList(record1, record2);

  }

  //todo why is this

  @Test
  void givenSingleReadsInSpanLength_whenCriteriaChecked_checkReadsInCriteria() {

    criteria = new CoreSingleCriteria<>(XsamRecordPredicates.createSpanInRangePredicate(new Range(38, 39)));

    List<XsamRecord> pairs = criteria.meetCriteria(singles);

    assertEquals(2, pairs.size());
    assertEquals(record1, pairs.get(0));
    assertEquals(record2, pairs.get(1));

  }

  @Test
  void givenSingleReadsOutSpanLength_whenCriteriaChecked_checkReadsInCriteria() {

    criteria = new CoreSingleCriteria<>(XsamRecordPredicates.createSpanInRangePredicate(new Range(91, 92)));

    List<XsamRecord> pairs = criteria.meetCriteria(singles);

    assertEquals(0, pairs.size());

  }

}