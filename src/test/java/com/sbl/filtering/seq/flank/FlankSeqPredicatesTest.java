package com.sbl.filtering.seq.flank;

import com.sbl.filtering.criteria.CoreSingleCriteria;
import com.sbl.model.PairedXsamRecord;
import com.sbl.model.footprints.PairedXsamRecordFootprint;
import com.sbl.model.footprints.PairedXsamRecordFootprintCombination;
import com.sbl.model.footprints.XsamRecordFootprint;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;

import static org.junit.jupiter.api.Assertions.*;

class FlankSeqPredicatesTest {


  @ParameterizedTest
  @MethodSource("com.sbl.providers.XsamPairProviders#flankExtPairProvider")
  void givenValidFlankExtReads_whenCorrectLeftFlankQueried_checkTrue(PairedXsamRecord pair) {

    String testSearch = "\txLseq:i:ATCG\t";
    String testQuery = "ATCG";

    Predicate<PairedXsamRecord> predicate = FlankSeqPredicates.hasFlankSeqMatch(PairedXsamRecordFootprint.LE, testQuery);

    CoreSingleCriteria<PairedXsamRecord> critera = new CoreSingleCriteria<>(predicate);

    assertTrue(pair.getxLSeq().isPresent());

    List<PairedXsamRecord> records = critera.meetCriteria(Collections.singletonList(pair));

    assertEquals(1, records.size());

  }

  @ParameterizedTest
  @MethodSource("com.sbl.providers.XsamPairProviders#flankExtPairProvider")
  void givenValidFlankExtReads_whenLbFlankQueried_checkTrue(PairedXsamRecord pair) {

    String testSearch = "\txLseq:i:ATCG\t";
    String testQuery = "ATCG";

    Predicate<PairedXsamRecord> predicate = FlankSeqPredicates.hasFlankSeqMatch(XsamRecordFootprint.Lb, testQuery);

    CoreSingleCriteria<PairedXsamRecord> critera = new CoreSingleCriteria<>(predicate);

    assertTrue(pair.getxLSeq().isPresent());

    List<PairedXsamRecord> records = critera.meetCriteria(Collections.singletonList(pair));

    assertEquals(1, records.size());

  }

  @ParameterizedTest
  @MethodSource("com.sbl.providers.XsamPairProviders#flankExtPairProvider")
  void givenValidFlankExtReads_when5pLeftFlankQueried_checkTrue(PairedXsamRecord pair) {

    String testSearch = "\txLseq:i:ATCG\t";
    String testQuery = "ATCG";

    Predicate<PairedXsamRecord> predicate = FlankSeqPredicates.hasFlankSeqMatch(XsamRecordFootprint.prime5, testQuery);

    CoreSingleCriteria<PairedXsamRecord> critera = new CoreSingleCriteria<>(predicate);

    assertTrue(pair.getxLSeq().isPresent());
    assertEquals('f', pair.getRecord1().getXd());

    List<PairedXsamRecord> records = critera.meetCriteria(Collections.singletonList(pair));

    assertEquals(1, records.size());

  }


  @ParameterizedTest
  @MethodSource("com.sbl.providers.XsamPairProviders#flankExtPairProviderR")
  void givenValidFlankExtReads_when5pRightFlankQueried_checkTrue(PairedXsamRecord pair) {

    String testSearch = "xRseq:i:ATTT";
    String testQuery = "AAAT";

    Predicate<PairedXsamRecord> predicate = FlankSeqPredicates.hasFlankSeqMatch(XsamRecordFootprint.prime5, testQuery);

    CoreSingleCriteria<PairedXsamRecord> critera = new CoreSingleCriteria<>(predicate);

    assertTrue(pair.getxLSeq().isPresent());
    assertEquals('r', pair.getRecord1().getXd());

    List<PairedXsamRecord> records = critera.meetCriteria(Collections.singletonList(pair));

    assertEquals(1, records.size());

  }

  @ParameterizedTest
  @MethodSource("com.sbl.providers.XsamPairProviders#flankExtPairProvider")
  void givenValidFlankExtReads_whenQueryLengthTooLong_checkFalse(PairedXsamRecord pair) {

    String testSearch = "\txLseq:i:ATCG\t";
    String testQuery = "ATCGTGGGTGTGTATGTGATGTGTATGT";

    Predicate<PairedXsamRecord> predicate = FlankSeqPredicates.hasFlankSeqMatch(PairedXsamRecordFootprint.LE, testQuery);

    CoreSingleCriteria<PairedXsamRecord> critera = new CoreSingleCriteria<>(predicate);

    assertTrue(pair.getxLSeq().isPresent());

    List<PairedXsamRecord> records = critera.meetCriteria(Collections.singletonList(pair));

    assertEquals(0, records.size());

  }


  @ParameterizedTest
  @MethodSource("com.sbl.providers.XsamPairProviders#flankExtPairProvider")
  void givenValidFlankExtReads_whenWrongLeftFlankQueried_checkTrue(PairedXsamRecord pair) {

    String testQuery = "ATCT";

    Predicate<PairedXsamRecord> predicate = FlankSeqPredicates.hasFlankSeqMatch(PairedXsamRecordFootprint.LE, testQuery);

    CoreSingleCriteria<PairedXsamRecord> critera = new CoreSingleCriteria<>(predicate);

    assertTrue(pair.getxLSeq().isPresent());

    List<PairedXsamRecord> records = critera.meetCriteria(Collections.singletonList(pair));

    assertEquals(0, records.size());

  }

  @ParameterizedTest
  @MethodSource("com.sbl.providers.XsamPairProviders#flankExtPairProvider")
  void givenValidFlankExtReads_whenCorrectRightFlankQueried_checkTrue(PairedXsamRecord pair) {

    String testSearch = "\txRseq:i:ATTT\t";

    //must be the reverse compliment on the right side.
    String testQuery = "AAAT";

    Predicate<PairedXsamRecord> predicate = FlankSeqPredicates.hasFlankSeqMatch(PairedXsamRecordFootprint.RE, testQuery);

    CoreSingleCriteria<PairedXsamRecord> critera = new CoreSingleCriteria<>(predicate);

    assertTrue(pair.getxRSeq().isPresent());

    List<PairedXsamRecord> records = critera.meetCriteria(Collections.singletonList(pair));

    assertEquals(1, records.size());

  }

  @ParameterizedTest
  @MethodSource("com.sbl.providers.XsamPairProviders#flankExtPairProvider")
  void givenValidFlankExtReads_whenWrongRightFlankQueried_checkTrue(PairedXsamRecord pair) {

    String testQuery = "ATCT";

    Predicate<PairedXsamRecord> predicate = FlankSeqPredicates.hasFlankSeqMatch(PairedXsamRecordFootprint.RE, testQuery);

    CoreSingleCriteria<PairedXsamRecord> critera = new CoreSingleCriteria<>(predicate);

    assertTrue(pair.getxLSeq().isPresent());

    List<PairedXsamRecord> records = critera.meetCriteria(Collections.singletonList(pair));

    assertEquals(0, records.size());

  }


  @ParameterizedTest
  @MethodSource("com.sbl.providers.XsamPairProviders#flankExtPairProvider")
  void givenValidFlankExtReads_whenCorrectLeftAndWrongRightQueried_checkTrue(PairedXsamRecord pair) {

    String testSearch = "\txLseq:i:ATCG\t";
    String testQuery = "ATCG";

    Predicate<PairedXsamRecord> predicate = FlankSeqPredicates.hasFlankSeqMatch(PairedXsamRecordFootprintCombination.AE, testQuery);

    CoreSingleCriteria<PairedXsamRecord> critera = new CoreSingleCriteria<>(predicate);

    assertTrue(pair.getxLSeq().isPresent());

    List<PairedXsamRecord> records = critera.meetCriteria(Collections.singletonList(pair));

    assertEquals(1, records.size());

  }

  @ParameterizedTest
  @MethodSource("com.sbl.providers.XsamPairProviders#flankExtPairProvider")
  void givenValidFlankExtReads_whenWrongLeftAndCorrectRightQueried_checkTrue(PairedXsamRecord pair) {

    String testSearch = "\txRseq:i:ATTT\t";

    //must be the reverse compliment on the right side.
    String testQuery = "AAAT";

    Predicate<PairedXsamRecord> predicate = FlankSeqPredicates.hasFlankSeqMatch(PairedXsamRecordFootprintCombination.AE, testQuery);

    CoreSingleCriteria<PairedXsamRecord> critera = new CoreSingleCriteria<>(predicate);

    assertTrue(pair.getxLSeq().isPresent());

    List<PairedXsamRecord> records = critera.meetCriteria(Collections.singletonList(pair));

    assertEquals(1, records.size());

  }

  @ParameterizedTest
  @MethodSource("com.sbl.providers.XsamPairProviders#flankExtPairProvider")
  void givenValidFlankExtReads_whenWrongLeftAndWrongRightQueried_checkTrue(PairedXsamRecord pair) {

    String testSearch = "\txRseq:i:ATTT\t";

    //must be the reverse compliment on the right side.
    String testQuery = "AAATTGTGTSIOQPSOSJDASODJSAD";

    Predicate<PairedXsamRecord> predicate = FlankSeqPredicates.hasFlankSeqMatch(PairedXsamRecordFootprintCombination.AE, testQuery);

    CoreSingleCriteria<PairedXsamRecord> critera = new CoreSingleCriteria<>(predicate);

    assertTrue(pair.getxLSeq().isPresent());

    List<PairedXsamRecord> records = critera.meetCriteria(Collections.singletonList(pair));

    assertEquals(0, records.size());

  }



}