package com.sbl.filtering.seq.in;

import com.sbl.filtering.seq.common.Strength;
import com.sbl.model.PairedXsamRecord;
import com.sbl.model.XsamRecord;
import com.sbl.utils.XsamUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;

import static org.junit.jupiter.api.Assertions.*;

class InSeqPredicatesTest {

    @ParameterizedTest
    @MethodSource({"com.sbl.providers.XsamRecordProviders#positionedRecordStream", "com.sbl.providers.XsamRecordProviders#nonMatchRecordStream", "com.sbl.providers.XsamRecordProviders#softEndingStream"})
    void givenSingleReads_whenInSeqParsedWithAllStrength_checkResultCorrect(XsamRecord record) {

        String query = "AAT";

        Predicate<XsamRecord> pred = InSeqPredicates.recordHasInSeqMatch(Strength.all, query);

        if(record.getXd() == 'f' && record.getSequence().contains(query)){
            assertTrue(pred.test(record));
        }else if(record.getXd() == 'r' && XsamUtils.reverseCompliment(record.getSequence()).contains(query)) {
            assertTrue(pred.test(record));
        } else{
            assertFalse(pred.test(record));
        }

    }

    @ParameterizedTest
    @MethodSource({"com.sbl.providers.XsamRecordProviders#positionedRecordStream", "com.sbl.providers.XsamRecordProviders#nonMatchRecordStream", "com.sbl.providers.XsamRecordProviders#softEndingStream"})
    void givenSingleReads_whenInSeqParsedWithHardStrength_checkResultCorrect(XsamRecord record) {

        String query = "AAT";

        Predicate<XsamRecord> pred = InSeqPredicates.recordHasInSeqMatch(Strength.hard, query);

        if(record.getXd() == 'f' && XsamUtils.removeSoftTrimmedBases(record.getSequence(), record.getCigar()).contains(query)){
            assertTrue(pred.test(record));
        }else if(record.getXd() == 'r' && XsamUtils.reverseCompliment(XsamUtils.removeSoftTrimmedBases(record.getSequence(), record.getCigar())).contains(query)) {
            assertTrue(pred.test(record));
        } else{
            assertFalse(pred.test(record));
        }

        if(XsamUtils.removeSoftTrimmedBases(record.getSequence(), record.getCigar()).contains(query)){
            assertTrue(pred.test(record));
        }else{
            assertFalse(pred.test(record));
        }

    }

    @ParameterizedTest
    @MethodSource({"com.sbl.providers.XsamPairProviders#ppePairProvider", "com.sbl.providers.XsamPairProviders#ppdPairProvider"})
    void givenReadsInSpanLength_whenCriteriaChecked_checkReadsInCriteria(PairedXsamRecord pair) {

        Predicate<PairedXsamRecord> pred = InSeqPredicates.hasInSeqMatch(Strength.all, "AAT");

        boolean tOrF = false;

        for(XsamRecord r : Arrays.asList(pair.getRecord1(), pair.getRecord2())){
            if(r.getXd() == 'r' && XsamUtils.reverseCompliment(r.getSequence()).contains("AAT")){
                tOrF = true;
            }else if(r.getSequence().contains("AAT")){
                tOrF = true;
            }
        }

        if(tOrF){
            assertTrue(pred.test(pair));
        }else{
            assertFalse(pred.test(pair));
        }
    }


}