package com.sbl.filtering.seq.end;

import com.sbl.filtering.seq.common.Strength;
import com.sbl.model.XsamRecord;
import com.sbl.model.footprints.XsamRecordFootprint;
import com.sbl.utils.XsamUtils;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.function.Predicate;

import static org.junit.jupiter.api.Assertions.*;

class EndSeqPredicatesTest {

  @ParameterizedTest
  @MethodSource({"com.sbl.providers.XsamRecordProviders#positionedRecordStream", "com.sbl.providers.XsamRecordProviders#nonMatchRecordStream"})
  void givenValidSingleReads_whenSoftNNSearchedForWithLbFootprint_checkCorrectResult(XsamRecord record) {

    Predicate<XsamRecord> predicate = EndSeqPredicates.hasEndSeqMatch(Strength.soft, XsamRecordFootprint.Lb, "NN");

    if(XsamUtils.findStartingSoftBases(record.getCigar()) != -1){
      assertTrue(predicate.test(record));
    }else{
      assertFalse(predicate.test(record));
    }

  }


  @ParameterizedTest
  @MethodSource({"com.sbl.providers.XsamRecordProviders#positionedRecordStream", "com.sbl.providers.XsamRecordProviders#nonMatchRecordStream"})
  void givenValidSingleReads_whenSoftNNSearchedForWith3pFootprint_checkCorrectResult(XsamRecord record) {

    //should print message and default to Lb

    Predicate<XsamRecord> predicate = EndSeqPredicates.hasEndSeqMatch(Strength.soft, XsamRecordFootprint.prime3, "NN");

    if(XsamUtils.findStartingSoftBases(record.getCigar()) != -1){
      assertTrue(predicate.test(record));
    }else{
      assertFalse(predicate.test(record));
    }

  }

  @ParameterizedTest
  @MethodSource({"com.sbl.providers.XsamRecordProviders#positionedRecordStream", "com.sbl.providers.XsamRecordProviders#nonMatchRecordStream"})
  void givenValidSingleReads_whenAllNNSearchedForWithLbFootprint_checkCorrectResult(XsamRecord record) {

    Predicate<XsamRecord> predicate = EndSeqPredicates.hasEndSeqMatch(Strength.all, XsamRecordFootprint.Lb, "NN");

    if(XsamUtils.findStartingSoftBases(record.getCigar()) != -1){
      assertTrue(predicate.test(record));
    }else{
      assertFalse(predicate.test(record));
    }

  }

  @ParameterizedTest
  @MethodSource({"com.sbl.providers.XsamRecordProviders#positionedRecordStream", "com.sbl.providers.XsamRecordProviders#nonMatchRecordStream"})
  void givenValidSingleReads_whenHardNNSearchedForWithLbFootprint_checkCorrectResult(XsamRecord record) {

    Predicate<XsamRecord> predicate = EndSeqPredicates.hasEndSeqMatch(Strength.hard, XsamRecordFootprint.Lb, "TCCT");

    if(XsamUtils.findStartingSoftBases(record.getCigar()) == -1){
      assertTrue(predicate.test(record));
    }else{
      assertFalse(predicate.test(record));
    }

  }


  @ParameterizedTest
  @MethodSource({"com.sbl.providers.XsamRecordProviders#softEndingStream"})
  void givenValidSingleReads_whenSoftNNSearchedForWithRbFootprint_checkCorrectResult(XsamRecord record) {

    Predicate<XsamRecord> predicate = EndSeqPredicates.hasEndSeqMatch(Strength.soft, XsamRecordFootprint.Rb, "NN");

    System.out.println(record.getCigar());
    int softBasesEnd = XsamUtils.findEndingSoftBases(record.getCigar());

    if(softBasesEnd > 1 && record.getSequence().endsWith("NN")){
      assertTrue(predicate.test(record));
    }else{
      assertFalse(predicate.test(record));
    }

  }

  @ParameterizedTest
  @MethodSource({"com.sbl.providers.XsamRecordProviders#softEndingStream"})
  void givenValidSingleReads_whenAllNNSearchedForWithRbFootprint_checkCorrectResult(XsamRecord record) {

    Predicate<XsamRecord> predicate = EndSeqPredicates.hasEndSeqMatch(Strength.all, XsamRecordFootprint.Rb, "NN");

    if(record.getSequence().endsWith("NN")){
      assertTrue(predicate.test(record));
    }else{
      assertFalse(predicate.test(record));
    }

  }

  @ParameterizedTest
  @MethodSource({"com.sbl.providers.XsamRecordProviders#softEndingStream", "com.sbl.providers.XsamRecordProviders#positionedRecordStream", "com.sbl.providers.XsamRecordProviders#nonMatchRecordStream"})
  void givenValidSingleReads_whenHardASearchedForWithRbFootprint_checkCorrectResult(XsamRecord record) {

    Predicate<XsamRecord> predicate = EndSeqPredicates.hasEndSeqMatch(Strength.hard, XsamRecordFootprint.Rb, "A");

    if(XsamUtils.findEndingSoftBases(record.getCigar()) == -1){
      if(record.getSequence().endsWith("A")) {
        assertTrue(predicate.test(record));
      }else{
        assertFalse(predicate.test(record));
      }
    }else if(record.getSequence().substring(record.getSequence().length() - XsamUtils.findEndingSoftBases(record.getCigar())).endsWith("A")){
      assertTrue(predicate.test(record));
    } else{
      assertFalse(predicate.test(record));
    }

  }


  @ParameterizedTest
  @MethodSource({"com.sbl.providers.XsamRecordProviders#positionedRecordStream", "com.sbl.providers.XsamRecordProviders#nonMatchRecordStream"})
  void givenValidSingleReads_whenAllSearchedForWith5pFootprint_checkCorrectResult(XsamRecord record) {

    Predicate<XsamRecord> predicate = EndSeqPredicates.hasEndSeqMatch(Strength.all, XsamRecordFootprint.prime5, "G");

    if(record.getXd() == 'r' && record.getSequence().endsWith("C")){
      assertTrue(predicate.test(record));
    }else{
      assertFalse(predicate.test(record));
    }

  }

  @ParameterizedTest
  @MethodSource({"com.sbl.providers.XsamRecordProviders#softEndingStream"})
  void givenValidSingleReads_whenSoftsearchedForWith5pFootprint_checkCorrectResult(XsamRecord record) {

    Predicate<XsamRecord> predicate = EndSeqPredicates.hasEndSeqMatch(Strength.soft, XsamRecordFootprint.prime5, "N");

    if(XsamUtils.findEndingSoftBases(record.getCigar()) != -1 && record.getXd() == 'r' && record.getSequence().endsWith("N")){
      assertTrue(predicate.test(record));
    }else{
      assertFalse(predicate.test(record));
    }

  }


  @ParameterizedTest
  @MethodSource({"com.sbl.providers.XsamRecordProviders#positionedRecordStream", "com.sbl.providers.XsamRecordProviders#nonMatchRecordStream", "com.sbl.providers.XsamRecordProviders#softEndingStream"})
  void givenValidSingleReads_whenHardSearchedForWith5pFootprint_checkCorrectResult(XsamRecord record) {

    Predicate<XsamRecord> predicate = EndSeqPredicates.hasEndSeqMatch(Strength.hard, XsamRecordFootprint.prime5, "G");

    if(record.getXd() == 'r' && record.getSequence().endsWith("C")){
      assertTrue(predicate.test(record));
    }else if(record.getXd() == 'f' && record.getSequence().startsWith("G")) {
      assertTrue(predicate.test(record));
    }else {
      assertFalse(predicate.test(record));
    }

  }

}