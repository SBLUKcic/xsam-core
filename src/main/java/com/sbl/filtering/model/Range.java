package com.sbl.filtering.model;

public class Range {

  private final long min;
  private final long max;

  public Range(long min, long max) {
    this.min = min;
    this.max = max;
  }

  public long getMin() {
    return min;
  }

  public long getMax() {
    return max;
  }
}
