package com.sbl.filtering.seq.flank;

import com.sbl.model.PairedXsamRecord;
import com.sbl.model.footprints.PairedXsamRecordFootprint;
import com.sbl.model.footprints.PairedXsamRecordFootprintCombination;
import com.sbl.model.footprints.XsamRecordFootprint;
import com.sbl.utils.XsamUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

/**
 *  Final class for storing predicates used for flank-seq filtering.
 */
public final class FlankSeqPredicates {

  private FlankSeqPredicates(){
    throw new AssertionError("Nope!");
  }

  public static Predicate<PairedXsamRecord> hasFlankSeqMatch(PairedXsamRecordFootprintCombination footprint, String query){
    List<Predicate<PairedXsamRecord>> predicateList = new ArrayList<>();

    for(PairedXsamRecordFootprint f : footprint.getFootprints()){
      Predicate<PairedXsamRecord> predicate = hasFlankSeqMatch(f, query);
      predicateList.add(predicate);
    }

    return predicateList.stream().reduce(Predicate::or).orElse(x->false);
  }

  /** creates a predicate for flankseq searching. defaults to LE footprint if supplied is invalid.
   * @param footprint paired footprint, default LE
   * @param query String to query
   * @return predicate
   */
  public static Predicate<PairedXsamRecord> hasFlankSeqMatch(PairedXsamRecordFootprint footprint, String query){
    switch (footprint){
      case RE: return record -> record.getxRSeq().isPresent() && record.getxRSeq().get().startsWith(XsamUtils.reverseCompliment(query));
      default: { //LE
        return record -> record.getxLSeq().isPresent() && record.getxLSeq().get().endsWith(query);
      }
    }
  }

  /** creates a predicate for flankseq searching. defaults to Lb footprint if supplied is invalid.
   * @param footprint single footprint, default  Lb
   * @param query String to query
   * @return predicate
   */
  public static Predicate<PairedXsamRecord> hasFlankSeqMatch(XsamRecordFootprint footprint, String query){
    switch (footprint){
      case prime5:  {
        return record -> {
          if(record.getRecord1().getXd() == 'f'){
            return hasFlankSeqMatch(PairedXsamRecordFootprint.LE, query).test(record);
          }else{
            return hasFlankSeqMatch(PairedXsamRecordFootprint.RE, query).test(record);
          }
        };
      }
      case prime3:  { //prime3
        return record -> {
          if (record.getRecord1().getXd() == 'r') {
            return hasFlankSeqMatch(PairedXsamRecordFootprint.LE, query).test(record);
          } else {
            return hasFlankSeqMatch(PairedXsamRecordFootprint.RE, query).test(record);
          }
        };
      }
      case Rb: return hasFlankSeqMatch(PairedXsamRecordFootprint.RE, query);
      default: return hasFlankSeqMatch(PairedXsamRecordFootprint.LE, query);

    }
  }



}

