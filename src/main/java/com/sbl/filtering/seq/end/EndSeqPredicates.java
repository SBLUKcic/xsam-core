package com.sbl.filtering.seq.end;

import com.sbl.filtering.seq.common.Strength;
import com.sbl.model.XsamRecord;
import com.sbl.model.footprints.XsamRecordFootprint;
import com.sbl.utils.XsamUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.function.Predicate;

import static com.sbl.model.footprints.XsamRecordFootprint.prime3;

public final class EndSeqPredicates {

  private static final Logger LOGGER = LogManager.getLogger(EndSeqPredicates.class.getSimpleName());

  private EndSeqPredicates(){
    throw new AssertionError("Narp!");
  }

  //todo, add support for Z and X characters in the query. These should allow any character

  public static Predicate<XsamRecord> hasEndSeqMatch(Strength strength, XsamRecordFootprint footprint, String query){

    if(footprint == prime3){
      LOGGER.warn("3 prime footprint not supported in endseq filtering");
    }

    switch (footprint) {
      case Rb: return processRbOptions(strength, query);
      case prime5: return process5POptions(strength, query);
      default:  return processLbOptions(strength, query);
    }
  }

  private static Predicate<XsamRecord> process5POptions(Strength strength, String query){

    String reverseQuery = XsamUtils.reverseCompliment(query);

    switch (strength){
      case all: return record -> record.getXd() == 'r' ? record.getSequence().endsWith(reverseQuery) : record.getSequence().startsWith(query);
      case hard: return record -> {
        if(record.getXd() == 'r'){
          int endingSoftBases = XsamUtils.findEndingSoftBases(record.getCigar());
          if(endingSoftBases == -1){
            return record.getSequence().endsWith(reverseQuery);
          }else {
            return record.getSequence().substring(record.getSequence().length() - endingSoftBases).endsWith(reverseQuery);
          }
        }else{
          return record.getSequence().startsWith(query);
        }
      };
      default: return record -> {
        if(record.getXd() == 'r'){
          int endingSoftBases = XsamUtils.findEndingSoftBases(record.getCigar());
          return endingSoftBases != -1 && record.getSequence().substring(record.getSequence().length() - endingSoftBases).endsWith(reverseQuery);
        }else{
          int bases = XsamUtils.findStartingSoftBases(record.getCigar());
          return bases != -1 && record.getSequence().substring(0, bases).startsWith(query);
        }
      };
    }
  }

  private static Predicate<XsamRecord> processRbOptions(Strength strength, String query){
    switch (strength){
      case soft: return record -> {
        int endingSoftBases = XsamUtils.findEndingSoftBases(record.getCigar());
        return endingSoftBases != -1 && record.getSequence().substring(record.getSequence().length() - endingSoftBases).endsWith(query);
      };
      case hard: return record -> {
        int endingSoftBases = XsamUtils.findEndingSoftBases(record.getCigar());
        if(endingSoftBases == -1){
          return record.getSequence().endsWith(query);
        }else {
          return record.getSequence().substring(record.getSequence().length() - endingSoftBases).endsWith(query);
        }
      };
      default: return record -> record.getSequence().endsWith(query);
    }
  }

  private static Predicate<XsamRecord> processLbOptions(Strength strength, String query) {
    switch (strength) {
      case all:
        return record -> record.getSequence().startsWith(query);
      case hard:
        return record -> XsamUtils.removeSoftTrimmedBases(record.getSequence(), record.getCigar()).startsWith(query);
      default: { //soft
        return record -> {
          int bases = XsamUtils.findStartingSoftBases(record.getCigar());
          return bases != -1 && record.getSequence().substring(0, bases).startsWith(query);
        };
      }
    }
  }







}
