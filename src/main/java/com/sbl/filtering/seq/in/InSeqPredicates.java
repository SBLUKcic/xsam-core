package com.sbl.filtering.seq.in;

import com.sbl.filtering.seq.common.Strength;
import com.sbl.model.PairedXsamRecord;
import com.sbl.model.XsamRecord;
import com.sbl.utils.XsamUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.function.Predicate;


/** in seq - checks the sequence to see if the query is found within the sequence
 */
public final class InSeqPredicates {

    private final static Logger LOGGER = LogManager.getLogger(InSeqPredicates.class.getSimpleName());

    private InSeqPredicates(){
        throw new AssertionError("Not on my watch!");
    }

    /** matches the inside edge of the xsam read pair
     *  currently only designed to work on a prime 5 footprint basis
     * @param query
     * @return
     */
    public static Predicate<PairedXsamRecord> hasInSeqMatch(Strength strength, String query){

        return recordPair -> recordHasInSeqMatch(strength, query).test(recordPair.getRecord1()) || recordHasInSeqMatch(strength, query).test(recordPair.getRecord2());
    }


    public static Predicate<XsamRecord> recordHasInSeqMatch(Strength strength, String query){

        if(strength == Strength.soft){
            LOGGER.warn("Invalid strength chosen for in seq processing, using " + Strength.all);
        }

        switch (strength){
            case hard: return record -> record.getXd() == 'f' ? XsamUtils.removeSoftTrimmedBases(record.getSequence(), record.getCigar()).contains(query) :
                XsamUtils.reverseCompliment(XsamUtils.removeSoftTrimmedBases(record.getSequence(), record.getCigar())).contains(query);
            default: return record -> record.getXd() == 'f' ? record.getSequence().contains(query) : XsamUtils.reverseCompliment(record.getSequence()).contains(query);
        }

    }
}
