package com.sbl.filtering.seq.common;

public enum Strength {
  hard, soft, all
}
