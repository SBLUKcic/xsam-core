package com.sbl.filtering.criteria;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * {@inheritDoc}
 */
public class CoreAndCriteria<T> implements CoreCriteria<T> {

  private final List<Predicate<T>> criteriaList;

  public CoreAndCriteria(List<Predicate<T>> criteriaList) {
    this.criteriaList = criteriaList;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public List<T> meetCriteria(List<T> records) {

    Predicate<T> andPredicate = criteriaList.stream().reduce(Predicate::and).orElse(x->true);

    List<T> pass = new ArrayList<>();
    for(T t : records){
      if(andPredicate.test(t)){
        pass.add(t);
      }
    }

    return pass;
  }
}
