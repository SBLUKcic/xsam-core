package com.sbl.filtering.criteria;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * {@inheritDoc}
 */
public class CoreOrCriteria<T> implements CoreCriteria<T> {


  private final List<Predicate<T>> criteriaList;

  /** Constructor
   * @param criteriaList list of Predicates to concatenate
   */
  public CoreOrCriteria(List<Predicate<T>> criteriaList) {
    this.criteriaList = criteriaList;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public List<T> meetCriteria(List<T> records) {

    Predicate<T> orPredicate = criteriaList.stream().reduce(Predicate::or).orElse(x->false);

    List<T> pass = new ArrayList<>();
    for(T t : records){
      if(orPredicate.test(t)){
        pass.add(t);
      }
    }

    return pass;

  }

}
