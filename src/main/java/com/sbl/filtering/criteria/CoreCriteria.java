package com.sbl.filtering.criteria;

import java.util.List;

/** Interface to handle filtered of Xsam Records by some criterion.
 *
 */
public interface CoreCriteria<T> {

  /** Implementations should filter the input Xsam records and output a sub-list
   * @param records XsamRecord objects to filter
   * @return sub-list of filtered XsamRecord
   */
  List<T> meetCriteria(List<T> records);

}
