package com.sbl.filtering.criteria;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class CoreSingleCriteria<T> implements CoreCriteria<T> {

  private final Predicate<T> predicate;

  public CoreSingleCriteria(Predicate<T> predicate) {
    this.predicate = predicate;
  }

  @Override
  public List<T> meetCriteria(List<T> records) {

    List<T> pass = new ArrayList<>();

    for(T t : records){
      if(predicate.test(t)){
        pass.add(t);
      }
    }

    return pass;

  }


}
