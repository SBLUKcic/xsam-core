package com.sbl.filtering.predicates;

import com.sbl.filtering.model.Range;
import com.sbl.model.footprints.XsamRecordFootprintCombination;
import com.sbl.model.xsamstrands.XsamRecordStrand;
import com.sbl.model.xsamtypes.XsamRecordMappingType;
import com.sbl.model.XsamRecord;
import com.sbl.model.footprints.XsamRecordFootprint;
import com.sbl.utils.XsamUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

/** A class of static predicates for use in the criteria classes
 *
 */
public class XsamRecordPredicates {

  private XsamRecordPredicates() {
    throw new AssertionError("Nope!");
  }

  /** Checks if the span of the record is within the specified range
   * @param range min and max to test
   * @return true, is in range, false otherwise
   */
  public static Predicate<XsamRecord> createSpanInRangePredicate(Range range) {
    return record -> record.getXs() >= range.getMin() && record.getXs() <= range.getMax();
  }

  /** Checks if the footprint is in the range
   * @param footprint position in the read to check
   * @param range min and max positions to use
   * @return true, is in range, false otherwise
   */
  public static Predicate<XsamRecord> createFootprintInRangePredicate(XsamRecordFootprint footprint, Range range) {
    switch (footprint) {
      case Lb:
        return record -> record.getXl() >= range.getMin() && record.getXl() <= range.getMax();
      case Rb:
        return record -> record.getXr() >= range.getMin() && record.getXr() <= range.getMax();
      case prime3: {
        return record -> {
          if (record.getXd() == 'f') {
            return record.getXr() >= range.getMin() && record.getXr() <= range.getMax();
          } else {
            return record.getXl() >= range.getMin() && record.getXl() <= range.getMax();
          }
        };
      }
      default: { //prime5
        return record -> {
          if (record.getXd() == 'f') {
            return record.getXl() >= range.getMin() && record.getXl() <= range.getMax();
          } else {
            return record.getXr() >= range.getMin() && record.getXr() <= range.getMax();
          }
        };
      }
    }
  }

  /** Checks if either of the footprints in the footprint combination is in the range
   *  overloaded to take in a footprint combination
   * @param combination combo of footprints
   * @param range min and max positions to use
   * @return true, is in range, false otherwise
   */
  public static Predicate<XsamRecord> createFootprintInRangePredicate(XsamRecordFootprintCombination combination, Range range){

    List<Predicate<XsamRecord>> predicateList = new ArrayList<>();

    for(XsamRecordFootprint footprint : combination.getFootprints()){

      Predicate<XsamRecord> predicate = createFootprintInRangePredicate(footprint, range);
      predicateList.add(predicate);

    }

    return predicateList.stream().reduce(Predicate::or).orElse(x->false);

  }

  /** Checks if the xsam mapping type is contained in the input type
   * @param type mapping type to check
   * @return true if contained, false otherwise
   */
  public static Predicate<XsamRecord> createMappingTypePredicate(XsamRecordMappingType type){
    return record -> type.getSubTypes().contains(record.getXm());
  }

  /** Checks if the xsam mapping strand is contained in the input strand
   * @param strand mapping strand to check
   * @return true if contained, false otherwise
   */
  public static Predicate<XsamRecord> createMappingStrandPredicate(XsamRecordStrand strand){
    return record -> strand.getSubStrands().contains(record.getXd());
  }

  /** Checks if the xsam reference matches the search term
   * @param reference name to check
   * @return true for match, false otherwise
   */
  public static Predicate<XsamRecord> createReferenceNameMatchPredicate(String reference){
    return record -> reference.equals(record.getReferenceName());
  }

  /** Checks if the average PHRED score is above the specified amount
   * IMPORTANT: uses ASCII values. For different encodings, translate as appropriate.
   * @param quality threshold quality
   * @return true if equal or above, false otherwise
   */
  public static Predicate<XsamRecord> createAverageQualityAboveThresholdPredicate(int quality){
    return record -> XsamUtils.calculateAveragePHRED(record.getQuality()) >= quality;
  }

  /** Checks if all of the base PHRED scores are above the specified amount
   *  IMPORTANT: uses ASCII values. For different encodings, translate as appropriate.
   * @param quality threshold quality
   * @return true if equal or above, false otherwise
   */
  public static Predicate<XsamRecord> createAllQualityAboveThresholdPredicate(int quality){
    return record -> XsamUtils.areAllPHREDValuesAboveThreshold(record.getQuality(), quality);
  }


}

