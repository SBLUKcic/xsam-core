package com.sbl.filtering.predicates;

import com.sbl.filtering.model.Range;
import com.sbl.model.PairedXsamRecord;
import com.sbl.model.footprints.PairedXsamRecordFootprint;
import com.sbl.model.footprints.PairedXsamRecordFootprintCombination;
import com.sbl.model.xsamtypes.PairedXsamRecordMappingCombination;
import com.sbl.model.xsamtypes.PairedXsamRecordMappingType;
import com.sbl.utils.XsamUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class PairedXsamRecordPredicates {

    /** Determines whether a read footprint is within the given range
     * @param footprint to check
     * @param range to check
     * @return Predicate to test read pairs with
     */
    public static Predicate<PairedXsamRecord> createFootprintInRangePredicate(PairedXsamRecordFootprint footprint, Range range){
      switch (footprint) {
          case LI: {
              return record -> {
                int internalFootprint = record.getxL() + record.getRecord1().getSequence().length();
                return internalFootprint >= range.getMin() && internalFootprint <= range.getMax();
              };
          }
          case RI: {
              return record -> {
                  int internalFootprint = record.getxR() - record.getRecord2().getSequence().length();
                  return internalFootprint >= range.getMin() && internalFootprint <= range.getMax();
              };
          }
          case RE:
              return record -> record.getxR() >= range.getMin() && record.getxR() <= range.getMax();
          default: //LE
              return record -> record.getxL() >= range.getMin() && record.getxL() <= range.getMax();
      }
    }

    /** Determines whether a read footprint combination (multiple footprints) are within the given range. This works on an OR basis.
     * @param footprintCombo  to check
     * @param range to check
     * @return Predicate to test read pairs with
     */
    public static Predicate<PairedXsamRecord> createFootprintInRangePredicate(PairedXsamRecordFootprintCombination footprintCombo, Range range){

        if (footprintCombo == PairedXsamRecordFootprintCombination.AI) {
            return record -> {
                int internalFootprint = record.getxL() + record.getRecord1().getSequence().length();
                int internalFootprintRight = record.getxR() - record.getRecord2().getSequence().length();
                return (internalFootprint >= range.getMin() && internalFootprint <= range.getMax())
                        || (internalFootprintRight >= range.getMin() && internalFootprintRight <= range.getMax());
            };
        }

        //AE
        return record -> {
            return (record.getxL() >= range.getMin() && record.getxL() <= range.getMax())
                    || (record.getxR() >= range.getMin() && record.getxR() <= range.getMax());
        };
    }

    /** Determines whether a span length (xS) is within the given range
     * @param range to check
     * @return Predicate to test read pairs with
     */
    public static Predicate<PairedXsamRecord> createSpanLengthInRangePredicate(Range range){
        return record -> record.getxS() >= range.getMin() && record.getxS() <= range.getMax();
    }

    /** Determines whether a mapping type of a given pair matches the specified type
     * @param type to check
     * @return Predicate to test read pairs with
     */
    public static Predicate<PairedXsamRecord> createMappingTypePredicate(PairedXsamRecordMappingType type){
        return record -> type.doReadsMatchMappingType(record.getRecord1().getXm(), record.getRecord2().getXm(), record.areReferenceNamesMatching());
    }

    /** Determines whether one of multiple mapping types matches that of the pair
     * @param combo to check
     * @return Predicate to test read pairs with
     */
    public static Predicate<PairedXsamRecord> createMappingCombinationPredicate(PairedXsamRecordMappingCombination combo){
        return record -> {
            List<Predicate<PairedXsamRecord>> predicates = new ArrayList<>();
            for(PairedXsamRecordMappingType type : combo.getSubTypes()){
                predicates.add(createMappingTypePredicate(type));
            }
            return predicates.stream().reduce(Predicate::or).orElse(x->false).test(record);
        };
    }

    /** Determines if the pair are mapped to the same reference
     * @return Predicate to test read pairs with
     */
    public static Predicate<PairedXsamRecord> createMatchingReferenceNamesPredicate(){
        return PairedXsamRecord::areReferenceNamesMatching;
    }

    /** Determines is the average phred is above the threshold
     * @param threshold to check
     * @return Predicate to test read pairs with
     */
    public static Predicate<PairedXsamRecord> createAverageQualityAboveThresholdPredicate(int threshold){
        return record -> XsamUtils.calculateAveragePHRED(record.getRecord1().getQuality(), record.getRecord2().getQuality()) > threshold;
    }

    /** Determines if any of the individual phred scores is below the threshold
     * @param threshold to check for
     * @return Predicate to test read pairs with
     */
    public static Predicate<PairedXsamRecord> createAllQualityAboveThresholdPredicate(int threshold){
        return record -> XsamUtils.areAllPHREDValuesAboveThreshold(record.getRecord1().getQuality(), threshold)
                && XsamUtils.areAllPHREDValuesAboveThreshold(record.getRecord2().getQuality(), threshold);
    }


}
