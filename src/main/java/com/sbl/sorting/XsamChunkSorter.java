package com.sbl.sorting;

import com.google.code.externalsorting.ExternalSort;
import com.sbl.io.XsamChunk;
import com.sbl.io.header.XsamSection;
import com.sbl.model.PairedXsamRecord;
import me.tongfei.progressbar.ProgressBar;
import me.tongfei.progressbar.ProgressBarBuilder;
import me.tongfei.progressbar.ProgressBarStyle;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;

public final class XsamChunkSorter {

    private final static Logger LOGGER = LogManager.getLogger(XsamChunkSorter.class.getSimpleName());

    private final static int MEGA_BYTE = 1048576;
    private final static int SAFE_CHUNK_SIZE = 25000;
    private final static String SORTED_CHUNK_TEMPLATE  = "%s_%d";

    /** Sorts the chunk using pre-defined sorting comparators determined by whether the file is paired-end or not.
     * @param chunk chunk to sort
     * @throws IOException
     */
    public static void sort(XsamChunk chunk, boolean showProgress) throws IOException {
        sort(chunk.isPaired() ? SortingMethods.createDefaultPairStringComparator() : SortingMethods.createDefaultRecordStringComparator(), chunk, showProgress);
    }

    public static void sort(Comparator<String> comparator, XsamChunk chunk, boolean showProgress) throws IOException {

        //don't sort X section chunks!
        if(chunk.getSection() == XsamSection.X){
            return;
        }

        File temp = new File(chunk.getLocation().getAbsolutePath() + "_temp");
        File tempDir = new File(chunk.getLocation().getParent() + File.separator + "myTempDir");

        if(!tempDir.exists() && !tempDir.mkdirs()){
            throw new IOException("Cannot create temporary sorting folder");
        }

        if(!temp.createNewFile() && !temp.exists()){
            throw new IOException("Cannot create temporary sorting file");
        }


        ProgressBarBuilder pbb = new ProgressBarBuilder()
                .showSpeed()
                .setUnit("MB", MEGA_BYTE)
                .setTaskName("Sorting chunk " + String.format("%s_%s_%d", chunk.getSection(), chunk.getReferenceName().replaceAll("/", "_"), chunk.getSpanSection()))
                .setStyle(ProgressBarStyle.ASCII)
                .setPrintStream(System.out);


        BufferedReader br = showProgress ? new BufferedReader(
                new InputStreamReader(ProgressBar.wrap( new FileInputStream(chunk.getLocation()), pbb))) :
                new BufferedReader(new FileReader(chunk.getLocation()));

        if(!chunk.isPaired()) {
            sortSingleEndedFile(comparator, chunk, temp, tempDir, br);
        }else{
            sortPairedEndedFile(temp, tempDir, br, chunk);
        }

        if(!tempDir.delete()){
            tempDir.deleteOnExit();
        }
    }

    private static void sortSingleEndedFile(Comparator<String> comparator, XsamChunk chunk, File temp, File tempDir, BufferedReader br) throws IOException {



        long allocatedMemory = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
        long presumableFreeMemory = Runtime.getRuntime().maxMemory() - allocatedMemory;

        int fiveMB = 5 * MEGA_BYTE;
        int maxFilesFor5MBFileSize = (int)(chunk.getLocation().length() / fiveMB);

        List<File> files = ExternalSort.sortInBatch(
                br,
                chunk.getLocation().length(),
                comparator,
                maxFilesFor5MBFileSize != 0 ? maxFilesFor5MBFileSize : 5000,
                presumableFreeMemory,
                StandardCharsets.UTF_8,
                tempDir,
                false,
                0,
                false,
                false);

        if(files.size() > 1){
            ExternalSort.mergeSortedFiles(files, temp, comparator, false);
            chunk.getLocation().delete();

            Path newPath = Paths.get(chunk.getLocation().toURI());
            Files.move(temp.toPath(), newPath);

        }else if(files.size() == 1){
            temp.delete();
            chunk.getLocation().delete();

            Path newPath = Paths.get(chunk.getLocation().toURI());
            Files.move(files.get(0).toPath(), newPath);

        }else{
            //0
            throw new IOException("No io produced");
        }
    }

    private static void sortPairedEndedFile(File temp, File tempDir, BufferedReader br, XsamChunk chunk) throws IOException {

        List<File> sortedChunks = convertToSortedChunks(br, tempDir);

        mergeSortedChunks(sortedChunks, temp);

        chunk.getLocation().delete();
        Path newPath = Paths.get(chunk.getLocation().toURI());
        Files.move(temp.toPath(), newPath);

    }

    private static void mergeSortedChunks(List<File> sortedChunks, File temp) throws IOException{

        BufferedWriter bfw = new BufferedWriter(new FileWriter(temp));

        List<BinaryFileBuffer> buffers = new ArrayList<>();
        for(File f : sortedChunks){
            buffers.add(new BinaryFileBuffer(new BufferedReader(new FileReader(f))));
        }

        mergeSortedFiles(bfw, buffers);

        for(File f : sortedChunks){
            if(!f.delete()){
                f.deleteOnExit();
            }
        }




    }

    private static List<File> convertToSortedChunks(BufferedReader br, File tempDir) throws IOException {

        List<File> sortedChunks = new ArrayList<>();

        try {
            String line;
            String line2;
            List<PairedXsamRecord> records = new ArrayList<>();

            while ((line = br.readLine()) != null) {

                if((line2 = br.readLine()) != null){

                    PairedXsamRecord record = new PairedXsamRecord(line, line2);

                    records.add(record);

                    if(records.size() >= SAFE_CHUNK_SIZE){

                        records.sort(SortingMethods.createDefaultPairComparator());
                        sortedChunks.add(writeChunkToFile(tempDir, records));
                        records = new ArrayList<>();

                    }

                }

            }

            if(records.size() > 0){
                records.sort(SortingMethods.createDefaultPairComparator());
                sortedChunks.add(writeChunkToFile(tempDir, records));
            }

        }finally {
            br.close();
        }

        return sortedChunks;
    }

    private static File writeChunkToFile(File tempDir, List<PairedXsamRecord> records) throws IOException{

        File sortedChunk = new File(tempDir + File.separator + String.format(SORTED_CHUNK_TEMPLATE, records.get(0).getRecord1().getReferenceName().replaceAll("/", "_"), System.nanoTime()));

        if(!sortedChunk.createNewFile() && !sortedChunk.exists()){
            throw new IOException("Could not create new sorted chunk");
        }

        try(PrintWriter printWriter = new PrintWriter(new FileWriter(sortedChunk))){
            for(PairedXsamRecord record : records){
                printWriter.println(record);
            }
        }

        return sortedChunk;

    }

    public static long mergeSortedFiles(BufferedWriter fbw,
                                        List<BinaryFileBuffer> buffers) throws IOException {

        Comparator<PairedXsamRecord> cmp = SortingMethods.createDefaultPairComparator();

        PriorityQueue<BinaryFileBuffer> pq = new PriorityQueue<>(
                11, (i, j) -> cmp.compare(i.peek(), j.peek()));
        for (BinaryFileBuffer bfb : buffers) {
            if (!bfb.empty()) {
                pq.add(bfb);
            }
        }
        long rowcounter = 0;
        try {

            while (pq.size() > 0) {
                BinaryFileBuffer bfb = pq.poll();
                PairedXsamRecord r = bfb.pop();
                fbw.write(r.toString());
                fbw.newLine();
                ++rowcounter;
                if (bfb.empty()) {
                    bfb.fbr.close();
                } else {
                    pq.add(bfb); // add it back
                }
            }

        } finally {
            fbw.close();
            for (BinaryFileBuffer bfb : pq) {
                bfb.close();
            }
        }
        return rowcounter;

    }


    /**
     * This is essentially a thin wrapper on top of a BufferedReader... which keeps
     * the last line in memory.
     *
     */
    private static final class BinaryFileBuffer {
        public BinaryFileBuffer(BufferedReader r) throws IOException {
            this.fbr = r;
            reload();
        }
        public void close() throws IOException {
            this.fbr.close();
        }

        public boolean empty() {
            return this.cache == null;
        }

        public PairedXsamRecord peek() {
            return this.cache;
        }

        public PairedXsamRecord pop() throws IOException {
            PairedXsamRecord answer = peek();// make a copy
            reload();
            return answer;
        }

        private void reload() throws IOException {

            String line1 = this.fbr.readLine();
            String line2 = this.fbr.readLine();

            if(line1 != null && line2 != null) {
                this.cache = new PairedXsamRecord(line1, line2);
            }else{
                this.cache = null;
            }
        }

        public BufferedReader fbr;

        private PairedXsamRecord cache;

    }

}
