package com.sbl.sorting;

import com.sbl.model.PairedXsamRecord;
import com.sbl.model.XsamRecord;
import com.sbl.utils.XsamReadQueries;

import java.util.Comparator;

public class SortingMethods {

    private SortingMethods(){
        throw new AssertionError("Cant.. do.. this..");
    }

    public static Comparator<XsamRecord> createDefaultRecordComparator(){
        return Comparator.comparingInt(XsamRecord::getXl).thenComparingInt(XsamRecord::getXr);
    }

    public static Comparator<PairedXsamRecord> createDefaultPairComparator(){
        return Comparator.comparingInt(PairedXsamRecord::getxL).thenComparingInt(PairedXsamRecord::getxR);
    }

    public static Comparator<String> createDefaultRecordStringComparator(){
        return Comparator.comparingInt(XsamReadQueries::findxlField).thenComparingInt(XsamReadQueries::findxrField);
    }

    public static Comparator<String> createDefaultPairStringComparator(){
        return Comparator.comparingInt(XsamReadQueries::findxLField).thenComparingInt(XsamReadQueries::findxRField).thenComparing(XsamReadQueries::findID);
    }
}
