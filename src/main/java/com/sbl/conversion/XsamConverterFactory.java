package com.sbl.conversion;

import java.util.concurrent.atomic.AtomicInteger;

public class XsamConverterFactory {

  public XsamConverter getXsamConverter(int repeatMode){
    if(repeatMode == 0){
      return new XsamConverterZero();
    }else if(repeatMode == 1){
      return new XsamConverterOne();
    }

    return new XsamConverterOne(); //this is defined as the default mode in the docs.
  }

  public XsamConverter getXsamConverter(int repeatMode, AtomicInteger atomicInteger){
    if(repeatMode == 0){
      return new XsamConverterZero(atomicInteger);
    }else if(repeatMode == 1){
      return new XsamConverterOne(atomicInteger);
    }

    return new XsamConverterOne(atomicInteger); //this is defined as the default mode in the docs.
  }

}
