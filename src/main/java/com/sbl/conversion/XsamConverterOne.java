package com.sbl.conversion;

import com.sbl.conversion.model.RepeatMode;
import com.sbl.conversion.model.XsamReturn;
import com.sbl.model.PairedSamRecord;
import com.sbl.model.SamRecord;
import com.sbl.model.XsamRecord;
import com.sbl.utils.FlagBitUtils;
import com.sbl.utils.XsamReadQueries;

import java.util.concurrent.atomic.AtomicInteger;

/**
 *  This implementation of the XsamConvert will treats repeats as X's .
 */
public class XsamConverterOne implements XsamConverter {

  private static final RepeatMode repeatMode = RepeatMode.One;
  private static final String NEW_SAM_TEMPLATE = "%s\t%s\t*\t0\t0\t*\t*\t0\t0\t%s\t%s\tPG:Z:novoalign\tZS:Z:NM";
  private final AtomicInteger atomicInteger;

  public XsamConverterOne(){
    atomicInteger = new AtomicInteger(0);
  }

  /** Constructor allowing the user to specify which atomic integer to use to label to PPD reads.
   * @param atomicInteger atomic integer
   */
  public XsamConverterOne(AtomicInteger atomicInteger){
    this.atomicInteger = atomicInteger;
  }

  @Override
  public XsamReturn convertRecordPair(PairedSamRecord samRecordPair) {

    //create the appropriate common fields for each
    XsamRecord recordSingle1 = convertSingleRecord(samRecordPair.getRecord1());
    XsamRecord recordSingle2 = convertSingleRecord(samRecordPair.getRecord2());

    if(recordSingle1.isMapped() && recordSingle2.isMapped()){
      if(recordSingle1.getReferenceName().equals(recordSingle2.getReferenceName())){
        return new XsamReturn(XsamPairBuilder.buildPPEPair(recordSingle1, recordSingle2));
      }else{
        return XsamPairBuilder.buildPPDPairs(recordSingle1, recordSingle2, atomicInteger);
      }
    }else if(!recordSingle1.isMapped() && !recordSingle2.isMapped()){
      return new XsamReturn(XsamPairBuilder.buildXXPair(recordSingle1, recordSingle2));
    }else{
      return new XsamReturn(XsamPairBuilder.buildPXPair(recordSingle1, recordSingle2));
    }

  }



  /** converts a single XsamRecord
   *  if non-repeat, converted as usual
   *  if repeat, all data is stripped out, and read is treated as an X.
   * @param samRecord record to convert
   * @return
   */
  @Override
  public XsamRecord convertSingleRecord(SamRecord samRecord) {

    if(samRecord.isRepeat()){

      int flag = FlagBitUtils.toggleBit(samRecord.getFlag(), 2);

      if(FlagBitUtils.isReadMappedInProperPair(flag)){
        flag = FlagBitUtils.toggleBit(flag, 1);
      }

      if(FlagBitUtils.isReadReverseStand(flag)){
        flag= FlagBitUtils.toggleBit(flag, 4);
      }

      //create string build to insert
      StringBuilder commonXsamBuilder = new StringBuilder(String.format(NEW_SAM_TEMPLATE, samRecord.getId(), flag, samRecord.getSequence(), samRecord.getQuality()));

      String commonFields = XsamCommonFieldBuilder.buildNonMappedCommonFieldString(repeatMode.getIntegerRepresentation());

      //insert the common fields
      commonXsamBuilder.insert(XsamReadQueries.findVariableRegionStart(commonXsamBuilder.toString()), String.format("%s\t", commonFields));

      return new XsamRecord(commonXsamBuilder.toString());

    }else{

      if (XsamReadQueries.findxlField(samRecord.getRead()) != -1) {
        //sam fields already exist!
        return new XsamRecord(samRecord.getRead());
      }

      //create the appropriate common fields
      String commonFields = samRecord.isMapped() ? XsamCommonFieldBuilder.buildMappedCommonFieldString(samRecord, RepeatMode.Zero.getIntegerRepresentation()) : XsamCommonFieldBuilder.buildNonMappedCommonFieldString(RepeatMode.Zero.getIntegerRepresentation());

      //create string build to insert
      StringBuilder commonXsamBuilder = new StringBuilder(samRecord.getRead());

      //insert the common fields
      commonXsamBuilder.insert(XsamReadQueries.findVariableRegionStart(samRecord.getRead()), String.format("%s\t", commonFields));

      //return
      return new XsamRecord(commonXsamBuilder.toString());

    }

  }
}
