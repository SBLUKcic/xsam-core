package com.sbl.conversion.model;

import com.sbl.model.PairedXsamRecord;

import java.util.Optional;

/** Immutable return type used in Sam to Xsam conversion.
 *  Since the second read pair may be optional, (PPD Only), this encapsulates that functionality with the use of an optional.
 *
 */
public class XsamReturn {

  private final PairedXsamRecord pair;
  private final Optional<PairedXsamRecord> optionalPair;

  public XsamReturn(PairedXsamRecord pair) {
    this.pair = pair;
    this.optionalPair = Optional.empty();
  }

  public XsamReturn(PairedXsamRecord pair1, PairedXsamRecord pair2){
    this.pair = pair1;
    this.optionalPair = Optional.of(pair2);
  }

  public PairedXsamRecord getPair() {
    return pair;
  }

  public Optional<PairedXsamRecord> getOptionalPair() {
    return optionalPair;
  }
}
