package com.sbl.conversion.model;

/**
 * Determines how repeats are handled during conversion from Sam to Xsam
 */
public enum RepeatMode {
  Zero("R's treated as R's", 0),
  One("R's treated as X's", 1);


  private String definition;
  private int integerRepresentation;

  RepeatMode(String definition, int integerRepresentation) {
    this.definition = definition;
    this.integerRepresentation = integerRepresentation;
  }

  public String getDefinition() {
    return definition;
  }

  public int getIntegerRepresentation() {
    return integerRepresentation;
  }
}
