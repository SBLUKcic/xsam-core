package com.sbl.conversion;

import com.sbl.conversion.model.XsamReturn;
import com.sbl.model.PairedSamRecord;
import com.sbl.model.PairedXsamRecord;
import com.sbl.model.SamRecord;
import com.sbl.model.XsamRecord;
import com.sbl.utils.FlagBitUtils;
import com.sbl.utils.XsamReadQueries;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * interface for conversion of SamRecords to XsamRecords
 */
public interface XsamConverter {

  XsamReturn convertRecordPair(PairedSamRecord samRecordPair);

  XsamRecord convertSingleRecord(SamRecord samRecord);

  /**
   *  class designed to build PairedXsamRecord's based on two previously computed records.
   */
  final class XsamPairBuilder {

    //todo add validation that paired fields do not already exist.

    private XsamPairBuilder(){
      throw new AssertionError();
    }

    /** returns an XsamRecord pair for valid PPE pair
     * @param record1 first record to be parsed
     * @param record2 second record to be parsed
     * @return valid Xsam pair
     */
    public static PairedXsamRecord buildPPEPair(XsamRecord record1, XsamRecord record2){

      if(record1.getPos() > record2.getPos()){

        XsamRecord temp = record1;
        record1 = changePositionInPair(record2);
        record2 = changePositionInPair(temp);

      }

      String pairedFields =  buildFields(record1.getXl(),
          record2.getXr(),
          record2.getXr() - record1.getXl() + 1,
          record2.getXl() - record1.getXr() - 1,
          0,0, "\"\"", "\"\"");

      StringBuilder recordBuilder1 = new StringBuilder(record1.getRead());
      recordBuilder1.insert(XsamReadQueries.findVariableRegionStart(record1.getRead()), String.format("%s\t",  pairedFields));

      StringBuilder recordBuilder2 = new StringBuilder(record2.getRead());
      recordBuilder2.insert(XsamReadQueries.findVariableRegionStart(record2.getRead()), String.format("%s\t",  pairedFields));

      return new PairedXsamRecord(new XsamRecord(recordBuilder1.toString()), new XsamRecord(recordBuilder2.toString()));

    }

    /** returns 2 sets of pairs, one 'original' and one 'duplicate'
     * @param record1 first record to be parsed
     * @param record2 second record to be parsed
     * @return List of two duplicates
     */
    public static XsamReturn buildPPDPairs(XsamRecord record1, XsamRecord record2, AtomicInteger atomicInt){

      int val = atomicInt.getAndIncrement();

      String originalFields = buildFields(record1.getXl(),
          record1.getXr(),
          record1.getXr() - record1.getXl() + 1,
          0,
          val,0, "\"\"", "\"\"");

      String duplicateFields = buildFields(record2.getXl(),
          record2.getXr(),
          record2.getXr() - record2.getXl() + 1,
          0,
          val, 1, "\"\"", "\"\"");

      StringBuilder originalRecord1 = new StringBuilder(record1.getRead());
      originalRecord1.insert(XsamReadQueries.findVariableRegionStart(record1.getRead()), String.format("%s\t",  originalFields));

      StringBuilder originalRecord2 = new StringBuilder(record2.getRead());
      originalRecord2.insert(XsamReadQueries.findVariableRegionStart(record2.getRead()), String.format("%s\t",  originalFields));

      XsamRecord temp = record1;
      record1 = changePositionInPair(record2);
      record2 = changePositionInPair(temp);

      StringBuilder duplicateRecord1 = new StringBuilder(record1.getRead());
      duplicateRecord1.insert(XsamReadQueries.findVariableRegionStart(record1.getRead()), String.format("%s\t",  duplicateFields));

      StringBuilder duplicateRecord2 = new StringBuilder(record2.getRead());
      duplicateRecord2.insert(XsamReadQueries.findVariableRegionStart(record2.getRead()), String.format("%s\t",  duplicateFields));

      return new XsamReturn(
          new PairedXsamRecord(new XsamRecord(originalRecord1.toString()), new XsamRecord(originalRecord2.toString())),
          new PairedXsamRecord(new XsamRecord(duplicateRecord1.toString()), new XsamRecord(duplicateRecord2.toString()))
      );

    }

    /** returns an XsamRecord pair for valid PX pair
     * @param record1 first record to be parsed
     * @param record2 second record to be parsed
     * @return valid Xsam pair
     */
    public static PairedXsamRecord buildPXPair(XsamRecord record1, XsamRecord record2){

      if(!record1.isMapped()){
        XsamRecord temp = record1;
        record1 = changePositionInPair(record2);
        record2 = changePositionInPair(temp);
      }


      String pairedFields =  buildFields(record1.getXl(),
          record1.getXr(),
          record1.getXr() - record1.getXl() + 1,
          0,
          0,0, "\"\"", "\"\"");

      StringBuilder recordBuilder1 = new StringBuilder(record1.getRead());
      recordBuilder1.insert(XsamReadQueries.findVariableRegionStart(record1.getRead()), String.format("%s\t",  pairedFields));

      StringBuilder recordBuilder2 = new StringBuilder(record2.getRead());
      recordBuilder2.insert(XsamReadQueries.findVariableRegionStart(record2.getRead()), String.format("%s\t",  pairedFields));

      return new PairedXsamRecord(new XsamRecord(recordBuilder1.toString()), new XsamRecord(recordBuilder2.toString()));

    }

    /** returns an XsamRecord pair for valid PX pair
     * @param record1 first record to be parsed
     * @param record2 second record to be parsed
     * @return valid Xsam pair
     */
    public static PairedXsamRecord buildXXPair(XsamRecord record1, XsamRecord record2){

      String pairedFields = buildFields(0,0,0,0,0,0,"\"\"", "\"\"");

      StringBuilder recordBuilder1 = new StringBuilder(record1.getRead());
      recordBuilder1.insert(XsamReadQueries.findVariableRegionStart(record1.getRead()), String.format("%s\t",  pairedFields));

      StringBuilder recordBuilder2 = new StringBuilder(record2.getRead());
      recordBuilder2.insert(XsamReadQueries.findVariableRegionStart(record2.getRead()), String.format("%s\t",  pairedFields));

      return new PairedXsamRecord(new XsamRecord(recordBuilder1.toString()), new XsamRecord(recordBuilder2.toString()));

    }



    //if first in pair, return XsamRecord that is second in pair
    //if second in pair, return XsamRecord that is first in pair
    private static XsamRecord changePositionInPair(XsamRecord xsamRecord){


      int flag = xsamRecord.getFlag();

      return new XsamRecord(xsamRecord.getRead().replaceFirst(String.format("\t%s\t",String.valueOf(flag)), String.format("\t%s\t", FlagBitUtils.changePositionInPair(flag))));

    }


    private static String buildFields(int L, int R, int S, int W, int P, int Q, String C, String D){
      return String.format("xL:i:%d\txR:i:%d\txS:i:%d\txW:i:%d\txP:i:%d\txQ:i:%d\txC:A:%s\txD:A:%s", L,R,S,W,P,Q,C,D);
    }


  }


  /** Contains static methods for creating the common Xsam fields according to the input record
   *
   */
  final class XsamCommonFieldBuilder {

    private final static String NON_MAPPED_COMMON_FIELDS = "xl:i:0\txr:i:0\txs:i:0\txd:A:x\txm:A:x\txa:A:x\txx:i:%d";

    private XsamCommonFieldBuilder() {
      throw new AssertionError();
    }

    /**
     * Creates the common field string (lower case characters) corresponding to the single sam record.
     *
     * @param record read to process
     * @return common field string (xl:i:%d	xr:i:%d	xs:i:%d	xd:A:%s	xm:A:%s	xa:A:%s)
     */
    public static String buildMappedCommonFieldString(SamRecord record, int rModeValue) {

      int spanLength = calculateSpan(record.getCigar());

      return String.format("xl:i:%d\txr:i:%d\txs:i:%d\txd:A:%s\txm:A:%s\txa:A:%s\txx:i:%d",
          record.getPos(),
          record.getPos() + spanLength - 1,
          spanLength,
          findOrientation(record.getFlag()),
          record.isRepeat() ? 'r' : 'u',
          "\"\"",
          rModeValue
      );

    }

    public static String buildNonMappedCommonFieldString(int rModeValue) {
      return String.format(NON_MAPPED_COMMON_FIELDS, rModeValue);
    }

    private static int calculateSpan(String s) {
      int total = 0;
      String[] splitString = s.split("(?<=\\D)(?=\\d)|(?<=\\d)(?=\\D)");
      for (int pos = 0; pos < splitString.length; pos++) {
        if (splitString[pos].equals("M") || splitString[pos].equals("D") || splitString[pos].equals("X") || splitString[pos].equals("=")) {
          total += Integer.parseInt(splitString[pos - 1]);
        }
      }
      return total;
    }

    private static char findOrientation(int flag) {
      return (flag & (1 << 4)) != 0 ? 'r' : 'f';
    }

  }

}
