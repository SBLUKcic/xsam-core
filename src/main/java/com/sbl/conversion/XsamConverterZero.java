package com.sbl.conversion;

import com.sbl.conversion.model.RepeatMode;
import com.sbl.conversion.model.XsamReturn;
import com.sbl.model.SamRecord;
import com.sbl.model.PairedSamRecord;
import com.sbl.model.XsamRecord;
import com.sbl.utils.XsamReadQueries;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * implementation concerned with sam to xsam conversion for repeat mode '0' - R's treated as R's.
 * Holds an internal atomic integer to convert the records.
 */
public class XsamConverterZero implements XsamConverter {


  private final static RepeatMode repeatMode = RepeatMode.Zero;
  private final AtomicInteger atomicInteger;

  public XsamConverterZero(){
    atomicInteger = new AtomicInteger(0);
  }

  /** Constructor allowing the user to specify which atomic integer to use to label to PPD reads.
   * @param atomicInteger atomic integer
   */
  public XsamConverterZero(AtomicInteger atomicInteger){
    this.atomicInteger = atomicInteger;
  }

  /** converts a sam record pair into an xsam record pair
   * @param samRecordPair pair to be processed
   * @return XsamReturn object, containing a valid Xsam read, and an optional second one.
   */
  @Override
  public XsamReturn convertRecordPair(PairedSamRecord samRecordPair) {

    //create the appropriate common fields for each
    XsamRecord recordSingle1 = convertSingleRecord(samRecordPair.getRecord1());
    XsamRecord recordSingle2 = convertSingleRecord(samRecordPair.getRecord2());

    //work out the mapping combination between the pairs
    switch (samRecordPair.getMappingCombination()){
      case PPE:
        return new XsamReturn(XsamPairBuilder.buildPPEPair(recordSingle1, recordSingle2));
      case PPD:
        return XsamPairBuilder.buildPPDPairs(recordSingle1, recordSingle2, atomicInteger);
      case PX:
        return new XsamReturn(XsamPairBuilder.buildPXPair(recordSingle1, recordSingle2));
      default:
        //XX
        return new XsamReturn(XsamPairBuilder.buildXXPair(recordSingle1, recordSingle2));
    }

  }

  /** converts a single record to Xsam format, this will not produce paired-end fields!
   * @param samRecord record to input
   * @return Valid single Xsam record
   */
  @Override
  public XsamRecord convertSingleRecord(SamRecord samRecord) {

      if (XsamReadQueries.findxlField(samRecord.getRead()) != -1) {
        //sam fields already exist!
        return new XsamRecord(samRecord.getRead());
      }

      //create the appropriate common fields
      String commonFields = samRecord.isMapped() ? XsamCommonFieldBuilder.buildMappedCommonFieldString(samRecord, repeatMode.getIntegerRepresentation()) : XsamCommonFieldBuilder.buildNonMappedCommonFieldString(repeatMode.getIntegerRepresentation());

      //create string build to insert
      StringBuilder commonXsamBuilder = new StringBuilder(samRecord.getRead());

      //insert the common fields
      commonXsamBuilder.insert(XsamReadQueries.findVariableRegionStart(samRecord.getRead()), String.format("%s\t", commonFields));

      //return
      return new XsamRecord(commonXsamBuilder.toString());

  }

}
