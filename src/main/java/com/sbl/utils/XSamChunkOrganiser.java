package com.sbl.utils;

import com.sbl.io.XsamChunk;
import com.sbl.io.header.SpanLimits;
import com.sbl.io.header.XsamSection;

import java.io.*;
import java.util.*;

/**
 *  Contains methods for efficiently splitting a file full of random xsam reads into a list of xsam chunks
 *  This has been kept single-threaded to avoid disk contention.
 *  This utility is especially handy after multi-threaded operations where order becomes scrambled.
 */
public final class XSamChunkOrganiser {

    private final static String SPLIT_FILE_TEMPLATE = "%s_%s_%d_%d"; //input file name, rname, span limit, nanotime
    private final static int MAX_LIST_SIZE = 50;
    private final static int MAX_LOOP_COUNT_BEFORE_CHECK = 100;

    /** splits the input Xsam file into r-name and span selected chunks
     * @param input file of Xsam reads, single or paired-end.
     * @param tempDirectory temporary directory to hold the xsam chunks
     * @param spanLimits span limits to use when splitting the input file
     * @return List of r-name and span limit separated Xsam chunks.
     * @throws IOException something went wrong :(
     */
    public static List<XsamChunk> split(File input, File tempDirectory, SpanLimits spanLimits) throws IOException{
        return XsamUtils.isFilePaired(input) ? splitPaired(input, tempDirectory, spanLimits) : splitSingle(input, tempDirectory, spanLimits);
    }

    /** splits the (random bunch of xsam reads ) file, uses default span limits.
     * @param input reads, can be single-end of paired-end
     * @param tempDirectory temporary directory to store the output
     * @return list of XsamChunks
     * @throws IOException something went wrong
     */
    public static List<XsamChunk> split(File input, File tempDirectory) throws IOException{
        return split(input, tempDirectory, new SpanLimits());
    }

    /** simplest possible split option, uses default directory and span limits
     * @param input file to split
     * @return list of XsamChunks
     * @throws IOException something went wrong
     */
    public static List<XsamChunk> split(File input) throws IOException{
        File tempDirectory = new File(System.getProperty("user.dir") + File.separator + "split" + System.nanoTime());
        if(!tempDirectory.mkdirs() && !tempDirectory.exists()){
            throw new IOException("Cannot instantiate default XsamChunkSplitter directory: " + tempDirectory.getAbsolutePath());
        }
        return split(input, tempDirectory);
    }

    private static List<XsamChunk> splitPaired(File input, File tempDirectory, SpanLimits spanLimits) throws IOException {

        Map<SplitKey, List<String>> splitMap = new HashMap<>();
        Map<SplitKey, Integer> lineCountMap = new HashMap<>();

        try(BufferedReader reader = new BufferedReader(new FileReader(input))){

            String line;
            String line2;
            int loopCounter = 0;
            while((line = reader.readLine()) != null && (line2 = reader.readLine()) != null){

                SplitKey key = createKeyPaired(line, line2, spanLimits);

                if(!splitMap.containsKey(key)){

                    splitMap.put(key, new ArrayList<>());
                    File location = new File(
                            tempDirectory + File.separator + String.format(
                                    SPLIT_FILE_TEMPLATE,
                                    input.getName(),
                                    key.getReferenceName(),
                                    key.getSpanSection(),
                                    System.nanoTime()));
                    if(!location.exists() && !location.createNewFile()){
                        throw new IOException("Cannot create new xsam chunk");
                    }
                    key.setFile(location);
                    lineCountMap.put(key, 0);
                }

                splitMap.get(key).add(line);
                splitMap.get(key).add(line2);
                lineCountMap.replace(key, lineCountMap.get(key), lineCountMap.get(key)+1);

                loopCounter++;
                if(loopCounter == MAX_LOOP_COUNT_BEFORE_CHECK){

                    for(Map.Entry<SplitKey, List<String>> entry : splitMap.entrySet()){

                        if(entry.getValue().size() >= MAX_LIST_SIZE){

                            printList(entry);
                            splitMap.replace(entry.getKey(), splitMap.get(entry.getKey()), new ArrayList<>());


                        }

                    }

                    loopCounter = 0;
                }


            }


        }

        for(Map.Entry<SplitKey, List<String>> entry : splitMap.entrySet()){
            printList(entry);
        }


        return convertKeySet(splitMap.keySet(), lineCountMap);


    }

    private static List<XsamChunk> splitSingle(File input, File tempDirectory, SpanLimits spanLimits) throws IOException{

        Map<SplitKey, List<String>> splitMap = new HashMap<>();
        Map<SplitKey, Integer> lineCountMap = new HashMap<>();

        try(BufferedReader reader = new BufferedReader(new FileReader(input))){

            String line;
            int loopCounter = 0;
            while((line = reader.readLine()) != null){

                SplitKey key = createKeySingle(line, spanLimits);

                if(!splitMap.containsKey(key)){
                    splitMap.put(key, new ArrayList<>());
                    File location = new File(
                            tempDirectory + File.separator + String.format(
                                    SPLIT_FILE_TEMPLATE,
                                    input.getName(),
                                    key.getReferenceName(),
                                    key.getSpanSection(),
                                    System.nanoTime()));
                    if(!location.exists() && !location.createNewFile()){
                        throw new IOException("Cannot create new xsam chunk");
                    }
                    key.setFile(location);
                    lineCountMap.put(key, 0);
                }

                splitMap.get(key).add(line);
                lineCountMap.replace(key, lineCountMap.get(key), lineCountMap.get(key)+1);

                loopCounter++;
                if(loopCounter == MAX_LOOP_COUNT_BEFORE_CHECK){

                    for(Map.Entry<SplitKey, List<String>> entry : splitMap.entrySet()){

                        if(entry.getValue().size() >= MAX_LIST_SIZE){

                            printList(entry);
                            splitMap.replace(entry.getKey(), splitMap.get(entry.getKey()), new ArrayList<>());


                        }

                    }

                    loopCounter = 0;
                }


            }


        }

        for(Map.Entry<SplitKey, List<String>> entry : splitMap.entrySet()){
            printList(entry);
        }


        return convertKeySet(splitMap.keySet(), lineCountMap);


    }

    private static void printList(Map.Entry<SplitKey, List<String>> entry) throws IOException{

        if(entry.getValue().size() > 0) {
            try (PrintWriter printWriter = new PrintWriter(new FileWriter(entry.getKey().getFile(), true), true)) {

                for (String s : entry.getValue()) {
                    printWriter.println(s);
                }

                printWriter.flush();

            }

        }
    }

    private static List<XsamChunk> convertKeySet(Set<SplitKey> keys, Map<SplitKey, Integer> lineMap){

        List<XsamChunk> chunks = new ArrayList<>();

        for(SplitKey s : keys){

            chunks.add(new XsamChunk(lineMap.get(s), s.getFile(), s.getSpanSection(), s.getSection(), s.getReferenceName()));

        }

        return chunks;
    }

    private static SplitKey createKeySingle(String read, SpanLimits spanLimits){

        int span = XsamReadQueries.findxsField(read);
        String rname = XsamReadQueries.findReferenceName(read);
        XsamSection s = rname.equals("*") ? XsamSection.X : XsamSection.P;

        if(span <= spanLimits.getSpanLimit1()){
            return new SplitKey(rname, 1,  s);
        }else if(span > spanLimits.getSpanLimit1() && span <= spanLimits.getSpanLimit2() ){
            return new SplitKey(rname, 2,  s);
        }else if(span > spanLimits.getSpanLimit1() && span <= spanLimits.getSpanLimit3()){
            return new SplitKey(rname, 3, s);
        }


        return new SplitKey(rname, 4, s);

    }

    private static SplitKey createKeyPaired(String line, String line2, SpanLimits spanLimits) {

        int span = XsamReadQueries.findxSField(line);
        String rname = XsamReadQueries.findReferenceName(line);
        String rname2 = XsamReadQueries.findReferenceName(line2);
        XsamSection s = rname.equals(rname2) ? rname.equals("*") ? XsamSection.X : XsamSection.P : XsamSection.D;

        if(span <= spanLimits.getSpanLimit1()){
            return new SplitKey(rname, 1,  s);
        }else if(span > spanLimits.getSpanLimit1() && span <= spanLimits.getSpanLimit2() ){
            return new SplitKey(rname, 2,  s);
        }else if(span > spanLimits.getSpanLimit1() && span <= spanLimits.getSpanLimit3()){
            return new SplitKey(rname, 3, s);
        }


        return new SplitKey(rname, 4, s);

    }



    public static class SplitKey{

        private final String referenceName;
        private final int spanSection;
        private final XsamSection section;
        private File file;

        public SplitKey(String referenceName, int spanSection, XsamSection section) {
            this.referenceName = referenceName;
            this.spanSection = spanSection;
            this.section = section;
        }

        public void setFile(File file) {
            this.file = file;
        }

        public String getReferenceName() {
            return referenceName;
        }

        public int getSpanSection() {
            return spanSection;
        }

        public XsamSection getSection() {
            return section;
        }

        public File getFile() {
            return file;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            SplitKey splitKey = (SplitKey) o;
            return spanSection == splitKey.spanSection &&
                    Objects.equals(referenceName, splitKey.referenceName) &&
                    section == splitKey.section;
        }

        @Override
        public int hashCode() {
            return Objects.hash(referenceName, spanSection, section);
        }
    }


}
