package com.sbl.utils;

import com.sbl.exceptions.FileException;
import com.sbl.model.PairedXsamRecord;
import com.sbl.model.SamRecord;
import com.sbl.model.XsamRecord;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public final class XsamFileChecker {

    private XsamFileChecker(){
        throw new AssertionError();
    }

    public static FileType findFileType(File file) throws FileException {

        if(file == null){
            throw new FileException("File is null");
        }

        if(!file.exists()){
            throw new FileException("File does not exist");
        }

        if(!file.isFile()){
            throw new FileException("File is a directory");
        }

        if(file.length() == 0){
            throw new FileException("File is empty");
        }

        try(BufferedReader br = new BufferedReader(new FileReader(file))){

            String line;
            boolean isSamRecord = false;
            boolean isXsamRecord = false;

            while((line = br.readLine()) != null){

                if(!line.startsWith("@") && !line.isEmpty()){
                    try{
                        XsamRecord record = new XsamRecord(line);
                        isXsamRecord = true;
                        break;
                    }catch (IllegalArgumentException iae){
                        //not an xsam record, xl not present.
                    }

                    try{
                        SamRecord record = new SamRecord(line);
                        if(record.getPos() != -1) { //0 is valid
                            isSamRecord = true;
                        }
                        break;
                    }catch (IllegalArgumentException iae){
                        //not a sam record either/
                    }

                    break;
                }

            }

            if(isXsamRecord){
                return FileType.xsam;
            }else if(isSamRecord){
                return FileType.sam;
            }


        }catch (IOException ioe){
            throw new FileException("Cannot read file: " + ioe.getMessage());
        }


        return FileType.other;
    }

    public static boolean isXsamFileValid(File file) throws FileException{
        return isXsamFileValid(file, Long.MAX_VALUE);
    }

    public static boolean isXsamFileValid(File file, long sampleRange) throws FileException {

        if(findFileType(file) != FileType.xsam){
            return false;
        }

        try(BufferedReader br = new BufferedReader(new FileReader(file))){

            boolean paired = XsamUtils.isFilePaired(file);

            String line;
            String line2;

            int linecount = 0;

            while((line = br.readLine()) != null){

                if(!line.startsWith("@") && !line.isEmpty()){

                    if(linecount >= sampleRange){
                        break;
                    }
                    if(paired){
                        line2 = br.readLine();
                        if(line2 == null){
                            throw new FileException("Broken read pair found: " + line);
                        }
                        try{
                            new PairedXsamRecord(line, line2);
                        }catch (IllegalArgumentException iae){
                            return false;
                        }
                    }else {
                        try {
                            new XsamRecord(line);
                        } catch (IllegalArgumentException iae) {
                            //not an xsam record, xl not present.
                            return false;
                        }
                    }
                }
            }
        }catch (IOException ioe){
            throw new FileException("Cannot read file: " + ioe.getMessage());
        }


        return true;
    }


}
