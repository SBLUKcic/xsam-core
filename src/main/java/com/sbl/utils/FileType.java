package com.sbl.utils;

public enum FileType {
    sam, xsam, other
}
