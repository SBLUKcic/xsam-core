package com.sbl.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public final class XsamUtils {

  /** calculates the average phred across a read pair
   * @param phred1 phread1
   * @param phred2 phread2
   * @return average PHRED.
   */
  public static int calculateAveragePHRED(String phred1, String phred2) {

    int total1 = 0, total2 = 0;

    for (int i = 0; i < phred1.length(); i++) {
      total1 += (int) phred1.charAt(i);
    }

    for (int i = 0; i < phred2.length(); i++) {
      total2 += (int) phred2.charAt(i);
    }

    return (total1 + total2) / (phred2.length() + phred1.length());
  }

  /** calculates the average phred across a single phred
   * @param phred PHRED
   * @return avarage phred
   */
  public static int calculateAveragePHRED(String phred) {

    int total1 = 0;

    for (int i = 0; i < phred.length(); i++) {
      total1 += (int) phred.charAt(i);
    }

    return (total1 /  phred.length());
  }

  public static boolean areAllPHREDValuesAboveThreshold(String phred, int threshold){
    for(int i = 0; i < phred.length(); i++){
      if(phred.charAt(i) < threshold){
        return false;
      }
    }
    return true;
  }

  public static int findLinesInFile(File file) throws IOException{

    try(BufferedReader br = new BufferedReader(new FileReader(file))){

      int count = 0;
      while(br.readLine() != null){
        count++;
      }

      return count;
    }

  }

  public static boolean isFilePaired(File file) throws IOException{

    try(BufferedReader br = new BufferedReader(new FileReader(file))){

      String line;
      String line2;
      while((line = br.readLine()) != null){

        if(!line.startsWith("@")){
          if((line2 = br.readLine()) != null){

            if(XsamReadQueries.findID(line).equals(XsamReadQueries.findID(line2))){
              return true;
            }

          }else{
            return false; //only one read
          }

          break;

        }

      }

    }

    return false;
  }

  public static String reverseCompliment(String sequence){
    StringBuilder complimented = new StringBuilder();
    for(char c : sequence.toCharArray()){
      switch (c){
        case 'A': complimented.append('T');
          break;
        case 'T': complimented.append('A');
          break;
        case 'C': complimented.append('G');
          break;
        case 'G': complimented.append('C');
          break;
        case 'N': complimented.append('N');
          break;
        default: break;
      }
    }
    return complimented.reverse().toString();
  }

  /** removes all N's from the input sequence
   * @param sequence base string to remove N's from
   * @return N-less string
   */
  public static String removeNs(String sequence){
    StringBuilder sb = new StringBuilder();
    for(int i = 0; i < sequence.length(); i++){
      char c;
      if((c = sequence.charAt(i)) != 'N'){
        sb.append(c);
      }
    }

    return sb.toString();
  }

  /** removes all soft-trimmed bases, as specified from the cigar.
   * @param sequence sequence to trim
   * @param cigar CIGAR field from the Xsam file, soft-trimmed regions denote by a number and then S. e.g 3S38M2D1S
   * @return soft-trimmed-less sequence
   */
  public static String removeSoftTrimmedBases(String sequence, String cigar){

    StringBuilder mySequence = new StringBuilder(sequence);
    int softPointer = 0;
    StringBuilder numberBuilder = new StringBuilder();
    for(int i = 0; i < cigar.length(); i++){
      if(cigar.charAt(i) >= 65 && cigar.charAt(i) <= 90){
        int num = Integer.parseInt(numberBuilder.toString());
        if(cigar.charAt(i) == 'S'){
          mySequence.replace(softPointer, softPointer +  num, "");
          softPointer -= num;
        }else if(cigar.charAt(i) == 'D'){
          softPointer -= num;
        }
        softPointer += num;
        numberBuilder = new StringBuilder();
      }else{
        numberBuilder.append(cigar.charAt(i));
      }
    }

    return mySequence.toString();
  }

  /** returns -1 if cigar does not start with NS, return # bases otherwise
   * @param cigar cigar to check
   * @return -1 or # of bases
   */
  public static int findStartingSoftBases(String cigar){

    for(int i = 0; i < cigar.length(); i++){
      if(cigar.charAt(i) < 48 || cigar.charAt(i) > 57 ){
        //Character is A - Z
        if(cigar.charAt(i) == 'S'){
          return Integer.parseInt(cigar.substring(0, i));
        }else{
          return -1;
        }

      }
    }

    return -1;
  }

  /** returns -1 if cigar does not start with NS, return # bases otherwise
   * @param cigar cigar to check
   * @return -1 or # of bases
   */
  public static int findEndingSoftBases(String cigar){

    if(cigar.endsWith("S")) {
      for (int i = cigar.length() - 2; i >= 0; i--) {
        if (cigar.charAt(i) < 48 || cigar.charAt(i) > 57) {
          //Character is A - Z
          return Integer.parseInt(cigar.substring(i+1, cigar.length()-1));
        }
      }
      //must be all soft-trimmed bases
      return Integer.parseInt(cigar.substring(0, cigar.length()-1));
    }

    return -1;
  }

}
