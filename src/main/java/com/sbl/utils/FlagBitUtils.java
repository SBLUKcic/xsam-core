package com.sbl.utils;

public final class FlagBitUtils {

  private FlagBitUtils(){
   throw new AssertionError();
  }

  public static boolean isBitSet(int flag, int bit){

    if(flag == 0) return false;

    return (flag & (1 << bit)) != 0;

  }

  public static boolean isReadPaired(int flag){
    return isBitSet(flag, 0);
  }

  public static boolean isReadMappedInProperPair(int flag){
    return isBitSet(flag, 1);
  }

  public static boolean isReadUnmapped(int flag){
    return isBitSet(flag, 2);
  }

  public static boolean isMateUnmapped(int flag){
    return isBitSet(flag, 3);
  }

  public static boolean isReadReverseStand(int flag){
    return isBitSet(flag, 4);
  }

  public static boolean isMateReverseStrand(int flag){
    return isBitSet(flag, 5);
  }

  public static boolean isFirstInPair(int flag){
    return isBitSet(flag, 6);
  }

  public static boolean isSecondInPair(int flag){
    return isBitSet(flag, 7);
  }

  public static boolean isNotPrimaryAlignment(int flag){
    return isBitSet(flag, 8);
  }

  public static boolean isReadQualityCheckFailed(int flag){
    return isBitSet(flag, 9);
  }

  public static boolean isReadPCROrOpticalDuplicate(int flag){
    return isBitSet(flag, 10);
  }

  public static boolean isReadSupplementaryAlignment(int flag){
    return isBitSet(flag, 11);
  }

  /** changes the position of the pair by switching on or off the 'first in pair' or 'second in pair' bits
   * @param flag flag to change
   * @return new flag with bit changed
   */
  public static int changePositionInPair(int flag){
    if(isFirstInPair(flag)){
      //set bit for second, remove bit for first
      flag = flag & ~(1 << (7-1));
      return flag | (1 << (8-1));
    }

    flag = flag & ~(1 << (8-1));
    return flag | (1 << (7-1));
  }


  public static int toggleBit(int flag, int bit){

    if(isBitSet(flag, bit)){
      return flag & ~(1 << (bit));
    }else {
      return flag | (1 << (bit));
    }
  }



}
