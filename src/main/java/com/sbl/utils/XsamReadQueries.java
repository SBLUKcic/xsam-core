package com.sbl.utils;

import com.sbl.model.SamRecord;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

/**
 *  Non-instantiable utility class for working with Xsam reads
 */
public final class XsamReadQueries {

  // Suppress instantiation
  private XsamReadQueries() {
    throw new AssertionError();
  }

  /** finds the variable regions
   * @param read whole sam or Xsam read to search
   * @return variable regions
   */
  public static List<String> findVariableRegions(String read, int tabsToSkip){

    List<String> foundFields = new ArrayList<>();
    int found = 0;

    for(int i = 0; i < read.length(); i++){
      if(read.charAt(i) == '\t'){

        if(found >= tabsToSkip) {
          if (read.charAt(i + 1) != '\t') { //guard against double tabs.
            int j = i + 1;
            while (j < read.length() && read.charAt(j) != '\t') {
              j++;
            }
            if(j == read.length()-1){
              //end
              return foundFields;
            }
            String s = read.substring(i + 1, j);
            if (SamRecord.OptionalField.isFieldOptional(s)) {
              foundFields.add(s);
            }
          }
        }
        found++;
      }
    }

    return foundFields;

  }

  /** finds the position of the tab directly before the start of the variable region
   * @param read whole sam or Xsam read to search
   * @return position of the tab in the String
   */
  public static int findVariableRegionStart(String read){

    int numberOfTabsToEndOfQualitySequence = 10;
    int tabCount = 0;

    for(int i = 0; i < read.length(); i++){
      if(read.charAt(i) == '\t'){
        //check the next two.
        if(tabCount >= numberOfTabsToEndOfQualitySequence){
          if(SamRecord.OptionalField.isFieldOptional(read.substring(i+1, i+3))){
            return i+1;
          }
        }
        tabCount++;

      }
    }

    return read.length()-1;

  }

  /** Attempts to find the library name from SBL reads
   *  where SBL reads have the id SBL_LibraryName_ID:XXXXX
   *  if LibraryName end's with a lower case letter, the letter will be removed.
   *  if SBL_LibID is not valid, return the full ID.
   * @param ID or String to search.
   * @return Library name with lower case endings removed
   */
  public static String findLibraryName(String ID){

    if(!ID.startsWith("SBL")) return "";

    try {
      int firstPos = XsamReadQueries.findPosAfter(ID, "_");
      int i = firstPos;
      while (ID.charAt(i) != '_' && ID.charAt(i) != '\t') {
        i++;
      }

      String library = ID.substring(firstPos, i);

      char lastChar = library.charAt(library.length()-1);
      if(lastChar >= 97 && lastChar <= 122){
        library = library.substring(0, library.length()-1);
      }

      return library;

    }catch (Exception e){
      int i = 0;
      while(ID.charAt(i) != '\t'){
        i++;
        if(i == ID.length()){
          break;
        }
      }
      return ID.substring(0, i);
    }
  }

  /** Returns the ID from the sample
   * @param sample Xsam read
   * @return ID
   */
  public static String findID(String sample){
    return findElement(sample, 0);
  }

  public static String findFlag(String sample){
    return findElement(sample, 1);
  }

  /** finds the reference name field
   * @param sample String to search
   * @return String if found, empty string if not.
   */
  public static String findReferenceName(String sample){
    return findElement(sample, 2);
  }

  /** finds the Position
   * @param sample String to search
   * @return String if found, empty string if not.
   */
  public static String findPosition(String sample){
    return findElement(sample, 3);
  }

  /** finds the mapping quality
   * @param sample String to search
   * @return String if found, empty string if not.
   */
  public static String findMappingQuality(String sample){
    return findElement(sample, 4);
  }

  /**
   * Returns the cigar from the xsam read
   *
   * @param sample read
   * @return cigar string
   */
  public static String findCigar(String sample) {
    return findElement(sample, 5);
  }

  /** finds the mate reference name,
   * @param sample String to search
   * @return String if found, empty string if not. = if equal
   */
  public static String findMateReferenceName(String sample){
    return findElement(sample, 6);
  }

  /** finds the mate position
   * @param sample String to search
   * @return String if found, empty string if not.
   */
  public static String findMatePosition(String sample){
    return findElement(sample, 7);
  }

  /** finds the template length
   * @param sample String to search
   * @return String if found, empty string if not.
   */
  public static String findTemplateLength(String sample){
    return findElement(sample, 8);
  }

  /**
   * Returns the bases from the xsam read
   *
   * @param sample read
   * @return base string
   */
  public static String findBaseSequence(String sample) {
    return findElement(sample, 9);
  }

  /** Returns the phred score from the sample
   * @param sample Xsam read
   * @return phred string
   */
  public static String findPhred(String sample){
    return findElement(sample, 10);
  }


  /** Finds the tabs in the input read. This info can be used to retrieve fields
   * @param sample read to index
   * @return integer array of tab positions.
   */
  public static int[] findTabIndexes(String sample){
    List<Integer> indexes = new LinkedList<>();
    for(int i = 0; i < sample.length(); i++){
      if(sample.charAt(i) == '\t'){
        indexes.add(i);
      }
    }
    return indexes.stream().mapToInt(i->i).toArray();
  }

  /**
   * finds the n'th element in the tab delimited sample
   * i.e findElement(0) returns one from "one\ttwo"
   * 0 indexed.
   *
   * @param sample  String to search
   * @param element element to find
   * @return found element or "" if not found
   */
  private static String findElement(String sample, int element) {
    boolean tabsFound = false;
    int i = 0;
    int firstTab = 0;
    int secondTab = 0;
    int tabsToSkip = element - 1 >= 0 ? element - 1 : 0;
    int skippedTabs = 0;
    if (element == 0) {
      while (sample.charAt(i) != '\t') {
        i++;
      }
      return sample.substring(0, i);
    } else {
      while (!tabsFound) {
        if (sample.charAt(i) != '\t') {
          i++;
        } else {
          if (skippedTabs == tabsToSkip) {
            if (firstTab == 0) {
              firstTab = i;
            } else {
              secondTab = i;
              tabsFound = true;
            }
          } else {
            skippedTabs++;
          }
          i++;
        }
      }
    }

    return sample.substring(firstTab + 1, secondTab);
  }

  /** finds the xL field
   * @param sample String to search
   * @return position if found, '\0' (null) value if not.
   */
  public static int findxLField(String sample) {
    int chartStart = findPosAfter(sample, "\txL:i:");
    if (chartStart == -1) {
      return -1; //return -1 if not found.
    }
    int i = chartStart;
    while (sample.charAt(i) != '\t') {
      i++;
    }
    return Integer.parseInt(sample.substring(chartStart, i));

  }

  /** finds the xR field
   * @param sample String to search
   * @return position if found, '\0' (null) value if not.
   */
  public static int findxRField(String sample) {
    int chartStart = findPosAfter(sample, "\txR:i:");
    if (chartStart == -1) {
      return '\0'; //return NULL if not found.
    }
    int i = chartStart;
    while (sample.charAt(i) != '\t') {
      i++;
    }
    return Integer.parseInt(sample.substring(chartStart, i));

  }

  /** finds the xLSeq field
   * @param sample String to search
   * @return String if found, empty string if not.
   */
  public static Optional<String> findxLSeqField(String sample) {
    int charStart = findPosAfter(sample, "\txLseq:i:");
    if (charStart == -1) {
      return Optional.empty(); //return NULL if not found.
    }
    int i = charStart;
    while (sample.charAt(i) != '\t') {
      i++;
    }
    return Optional.of(sample.substring(charStart, i));

  }

  /** finds the xRSeq field
   * @param sample String to search
   * @return String if found, empty string if not.
   */
  public static Optional<String> findxRSeqField(String sample) {
    int charStart = findPosAfter(sample, "\txRseq:i:");
    if (charStart == -1) {
      return Optional.empty(); //return NULL if not found.
    }
    int i = charStart;
    while (sample.charAt(i) != '\t') {
      i++;
    }
    return Optional.of(sample.substring(charStart, i));

  }

  /** finds the xS field
   * @param sample String to search
   * @return position if found, '\0' (null) value if not.
   */
  public static int findxSField(String sample) {
    int charStart = findPosAfter(sample, "\txS:i:");
    if (charStart == -1) {
      return '\0'; //return NULL if not found.
    }
    int i = charStart;
    while (sample.charAt(i) != '\t') {
      i++;
    }
    return Integer.parseInt(sample.substring(charStart, i));

  }

  /** finds the xW field
   * @param sample String to search
   * @return position if found, '\0' (null) value if not.
   */
  public static int findxWField(String sample) {
    int charStart = findPosAfter(sample, "\txW:i:");
    if (charStart == -1) {
      return '\0'; //return NULL if not found.
    }
    int i = charStart;
    while (sample.charAt(i) != '\t') {
      i++;
    }
    return Integer.parseInt(sample.substring(charStart, i));

  }

  /** finds the xP field
   * @param sample String to search
   * @return position if found, '\0' (null) value if not.
   */
  public static int findxPField(String sample) {
    int charStart = findPosAfter(sample, "\txP:i:");
    if (charStart == -1) {
      return '\0'; //return NULL if not found.
    }
    int i = charStart;
    while (sample.charAt(i) != '\t') {
      i++;
    }
    return Integer.parseInt(sample.substring(charStart, i));

  }

  /** finds the xQ field
   * @param sample String to search
   * @return position if found, '\0' (null) value if not.
   */
  public static int findxQField(String sample) {
    int charStart = findPosAfter(sample, "\txQ:i:");
    if (charStart == -1) {
      return '\0'; //return NULL if not found.
    }
    int i = charStart;
    while (sample.charAt(i) != '\t') {
      i++;
    }
    return Integer.parseInt(sample.substring(charStart, i));

  }

  /** finds the xC field
   * @param sample String to search
   * @return position if found, '\0' (null) value if not.
   */
  public static String findxCField(String sample) {
    int charStart = findPosAfter(sample, "\txC:A:");
    if (charStart == -1) {
      return null; //return NULL if not found.
    }
    int i = charStart;
    while (sample.charAt(i) != '\t') {
      i++;
    }
    return sample.substring(charStart, i);

  }

  /** finds the xD field
   * @param sample String to search
   * @return position if found, '\0' (null) value if not.
   */
  public static String findxDField(String sample) {
    int charStart = findPosAfter(sample, "\txD:A:");
    if (charStart == -1) {
      return null; //return NULL if not found.
    }
    int i = charStart;
    while (sample.charAt(i) != '\t') {
      i++;
    }
    return sample.substring(charStart, i);

  }

  /** finds the xs field
   * @param sample String to search
   * @return position if found, '\0' (null) value if not.
   */
  public static int findxsField(String sample) {
    int charStart = findPosAfter(sample, "\txs:i:");
    if (charStart == -1) {
      return '\0'; //return NULL if not found.
    }
    int i = charStart;
    while (sample.charAt(i) != '\t') {
      i++;
    }
    return Integer.parseInt(sample.substring(charStart, i));

  }

  /** finds the xl field
   * @param sample String to search
   * @return position if found, '\0' (null) value if not.
   */
  public static int findxlField(String sample) {
    int charStart = findPosAfter(sample, "\txl:i:");
    if (charStart == -1) {
      return -1; //return -1 if not found.
    }
    int i = charStart;
    while (sample.charAt(i) != '\t') {
      i++;
    }
    return Integer.parseInt(sample.substring(charStart, i));

  }

  /** finds the xr field
   * @param sample String to search
   * @return position if found, '\0' (null) value if not.
   */
  public static int findxrField(String sample) {
    int chartStart = findPosAfter(sample, "\txr:i:");
    if (chartStart == -1) {
      return '\0'; //return NULL if not found.
    }
    int i = chartStart;
    while (sample.charAt(i) != '\t') {
      i++;
    }
    return Integer.parseInt(sample.substring(chartStart, i));

  }

  /** finds the xm field
   * @param sample String to search
   * @return char if found, '\0' (null) value if not.
   */
  public static char findxmField(String sample) {
    int charPos = findPosAfter(sample, "\txm:A:");
    if (charPos == -1) {
      return '\0'; // return NULL character if not found.
    }
    return sample.charAt(charPos);
  }

  /** finds the xd field
   * @param sample String to search
   * @return char if found, '\0' (null) value if not.
   */
  public static char findxdField(String sample) {
    int charPos = findPosAfter(sample, "\txd:A:");
    if (charPos == -1) {
      return '\0'; // return NULL character if not found.
    }
    return sample.charAt(charPos);
  }

  /**
   * Finds the needle in the haystack, and returns the position of the single next digit.
   *
   * @param haystack The string to search
   * @param needle   String field to search on.
   * @return position of the end of the needle
   */
  private static int findPosAfter(String haystack, String needle) {
    int hLen = haystack.length();
    int nLen = needle.length();
    int maxSearch = hLen - nLen;

    outer:
    for (int i = 0; i < maxSearch; i++) {
      for (int j = 0; j < nLen; j++) {
        if (haystack.charAt(i + j) != needle.charAt(j)) {
          continue outer;
        }
      }
      // If it reaches here, match has been found:
      return i + nLen;
    }
    return -1; // Not found
  }
}
