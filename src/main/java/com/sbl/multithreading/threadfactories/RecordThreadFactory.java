package com.sbl.multithreading.threadfactories;

import java.util.List;

public interface RecordThreadFactory<T> {

    Runnable createRecordProcessingThread(List<T> records);
}
