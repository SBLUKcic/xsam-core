package com.sbl.multithreading.distribution;

import com.sbl.io.XsamChunk;

import java.util.List;

public interface RecordDistributor {

    void distribute(XsamChunk chunk) throws Exception;

    void shutdown();

    void distribute(List<XsamChunk> chunks) throws Exception;
}
