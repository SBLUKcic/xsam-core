package com.sbl.multithreading.distribution;

import com.sbl.io.XsamChunk;
import com.sbl.model.Record;
import com.sbl.model.factories.RecordFactory;
import com.sbl.multithreading.threadfactories.RecordThreadFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class SimpleRecordDistributor<T extends Record> implements RecordDistributor {

    private static final Logger LOGGER = LogManager.getLogger(SimpleRecordDistributor.class.getSimpleName());

    private final ExecutorService executorService;
    private final RecordThreadFactory<T> threadFactory;
    private final int maxReadBlockSize; //maximum reads to be handed to a single thread.
    private final RecordFactory<T> recordFactory;

    public SimpleRecordDistributor(int threads,
                                   int maxQueueSize,
                                   int maxReadBlockSize,
                                   RecordThreadFactory<T> threadFactory,
                                   RecordFactory<T> recordFactory) {
        executorService = new ThreadPoolExecutor(threads, threads, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>(maxQueueSize));
        this.threadFactory = threadFactory;
        this.maxReadBlockSize = maxReadBlockSize;
        this.recordFactory = recordFactory;
    }

    public SimpleRecordDistributor(ExecutorService executorService,
                                   int maxReadBlockSize,
                                   RecordThreadFactory<T> threadFactory,
                                   RecordFactory<T> recordFactory) {
        this.executorService = executorService;
        this.threadFactory = threadFactory;
        this.maxReadBlockSize = maxReadBlockSize;
        this.recordFactory = recordFactory;
    }

    @Override
    public void distribute(XsamChunk chunk) throws Exception {

        try(BufferedReader reader = new BufferedReader(new FileReader(chunk.getLocation()))){

            List<T> records = new ArrayList<>();

            String read;
            while((read = reader.readLine()) != null){

                records.add(recordFactory.createRecord(read));

                if(records.size() == maxReadBlockSize){

                    addTask(executorService, threadFactory.createRecordProcessingThread(records));
                    records = new ArrayList<>();

                }

            }

            if(records.size() > 0){
                addTask(executorService, threadFactory.createRecordProcessingThread(records));
            }



        }catch (IOException ioe){
            throw new IOException(ioe.getMessage());
        }

    }

    @Override
    public void distribute(List<XsamChunk> chunks) throws Exception {
        for(XsamChunk chunk : chunks){
            distribute(chunk);
        }
    }

    @Override
    public void shutdown() {
        executorService.shutdown();
        try {
            executorService.awaitTermination(1, TimeUnit.DAYS);
        } catch (InterruptedException e) {
            LOGGER.warn("executor service interrupted: " + e.getMessage());
        }
    }

    private void addTask(ExecutorService eService, Runnable worker) {
        boolean taskAdded = false;
        while (!taskAdded) {
            try {
                eService.execute(worker);
                taskAdded = true;
            } catch (RejectedExecutionException ex) {
                taskAdded = false;
            }
        }
    }
}
