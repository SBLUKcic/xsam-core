package com.sbl.exceptions;

public class ConversionException extends Exception{

  public ConversionException(String message) {
    super(message);
  }
}
