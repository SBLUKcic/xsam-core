package com.sbl.model;

import com.sbl.model.xsamtypes.PairedXsamRecordMappingCombination;

public abstract class PairedRecordBase<T extends Record> implements PairedRecord<T> {


  private PairedXsamRecordMappingCombination combo = null;

  @Override
  public abstract T getRecord1();

  @Override
  public abstract T getRecord2();

  @Override
  public FastQRecord getFastQRecord1() {
    return getRecord1().convertToFastQ(1);
  }

  @Override
  public FastQRecord getFastQRecord2() {
    return getRecord2().convertToFastQ(2);
  }


  @Override
  public PairedXsamRecordMappingCombination getMappingCombination(){
    if(combo == null) {

      if (getRecord1().isMapped()) {
        if (getRecord2().isMapped()) {
          if (getRecord1().getMateReferenceName().equals("=")) {
            combo = PairedXsamRecordMappingCombination.PPE;
          } else {
            combo = PairedXsamRecordMappingCombination.PPD;
          }
        } else {
          if (getRecord1().isQualityFailed()) {
            combo = PairedXsamRecordMappingCombination.XX;
          } else {
            combo = PairedXsamRecordMappingCombination.PX;
          }
        }
      } else {
        if (getRecord2().isMapped()) {
          if (getRecord2().isQualityFailed()) {
            combo = PairedXsamRecordMappingCombination.XX;
          } else {
            combo = PairedXsamRecordMappingCombination.PX;
          }
        }else{
          combo = PairedXsamRecordMappingCombination.XX;
        }
      }

    }

    return combo;
  }
}
