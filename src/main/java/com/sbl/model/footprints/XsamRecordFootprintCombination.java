package com.sbl.model.footprints;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public enum XsamRecordFootprintCombination {
  Ab(Arrays.asList(
      XsamRecordFootprint.Lb,
      XsamRecordFootprint.Rb
    )
  );

  private List<XsamRecordFootprint> footprints;

  XsamRecordFootprintCombination(List<XsamRecordFootprint> footprints) {
    this.footprints = footprints;
  }

  public List<XsamRecordFootprint> getFootprints() {
    return footprints;
  }

  public static Optional<List<XsamRecordFootprint>> isValidsamRecordFootprintCombination(String combination){
    for(XsamRecordFootprintCombination combinations : XsamRecordFootprintCombination.values()){
      if(combinations.name().toUpperCase().equals(combination.toUpperCase())){
        return Optional.of(combinations.getFootprints());
      }
    }
    return Optional.empty();
  }
}
