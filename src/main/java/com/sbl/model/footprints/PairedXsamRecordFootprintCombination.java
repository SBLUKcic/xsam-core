package com.sbl.model.footprints;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public enum PairedXsamRecordFootprintCombination {
  AE("Any External", Arrays.asList(
      PairedXsamRecordFootprint.LE,
      PairedXsamRecordFootprint.RE
    )
  ),
  AI("Any Internal", Arrays.asList(
      PairedXsamRecordFootprint.LI,
      PairedXsamRecordFootprint.RI
    )
  );


  private String meaning;
  private List<PairedXsamRecordFootprint> footprints;

  PairedXsamRecordFootprintCombination(String meaning, List<PairedXsamRecordFootprint> footprints) {
    this.meaning = meaning;
    this.footprints = footprints;
  }

  public String getMeaning() {
    return meaning;
  }

  public List<PairedXsamRecordFootprint> getFootprints() {
    return footprints;
  }

  public static Optional<List<PairedXsamRecordFootprint>> isValidPairedXsamRecordFootprintCombination(String combination){
    for(PairedXsamRecordFootprintCombination combinations : PairedXsamRecordFootprintCombination.values()){
      if(combinations.name().toUpperCase().equals(combination.toUpperCase())){
        return Optional.of(combinations.getFootprints());
      }
    }
    return Optional.empty();
  }
}
