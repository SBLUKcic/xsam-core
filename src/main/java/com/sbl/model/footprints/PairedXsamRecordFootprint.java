package com.sbl.model.footprints;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public enum PairedXsamRecordFootprint {
  LE("Left External", "LE", "le", "Le"),
  RE("Right External", "RE", "re", "Re"),
  LI("Left Internal", "LI", "li", "Li"),
  RI("Right Interal", "RI", "ri", "Ri");

  private String meaning;
  private List<String> acceptedStrings;

  PairedXsamRecordFootprint(String meaning, String... acceptedStrings) {
    this.meaning = meaning;
    this.acceptedStrings = Arrays.asList(acceptedStrings);
  }

  public static Optional<PairedXsamRecordFootprint> isValidXsamRecordFootprint(String footprint){
    for(PairedXsamRecordFootprint xrFootprint : PairedXsamRecordFootprint.values()){
      if(xrFootprint.acceptedStrings.contains(footprint)){
        return Optional.of(xrFootprint);
      }
    }
    return Optional.empty();
  }

  public String getMeaning() {
    return meaning;
  }

  public List<String> getAcceptedStrings() {
    return acceptedStrings;
  }
}
