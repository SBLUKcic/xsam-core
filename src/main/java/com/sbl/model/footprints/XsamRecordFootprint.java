package com.sbl.model.footprints;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/** An xsam record footprint - denoting a position along the mapped read.
 *
 */
public enum XsamRecordFootprint {
  Lb("Left base", "Lb", "lb"),
  Rb("Right base", "Rb", "rb"),
  prime5("5 prime", "5p", "p5"),
  prime3("3 prime", "3p", "p3");

  private String meaning;
  private List<String> acceptedStrings;

  XsamRecordFootprint(String meaning, String... acceptedStrings) {
    this.meaning = meaning;
    this.acceptedStrings = Arrays.asList(acceptedStrings);
  }

  public static Optional<XsamRecordFootprint> isValidXsamRecordFootprint(String footprint){
     for(XsamRecordFootprint xrFootprint : XsamRecordFootprint.values()){
       if(xrFootprint.acceptedStrings.contains(footprint)){
         return Optional.of(xrFootprint);
       }
     }
     return Optional.empty();
  }

  public String getMeaning() {
    return meaning;
  }

  public List<String> getAcceptedStrings() {
    return acceptedStrings;
  }
}
