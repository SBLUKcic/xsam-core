package com.sbl.model.factories;

import com.sbl.model.PairedRecord;

public interface PairedRecordFactory<T extends PairedRecord> {

    T createPairedRecord(String record1, String record2);
}
