package com.sbl.model.factories;

import com.sbl.model.Record;

public interface RecordFactory<T extends Record> {

    T createRecord(String recordString);
}
