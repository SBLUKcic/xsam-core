package com.sbl.model;

import com.sbl.utils.XsamReadQueries;

import java.util.Objects;
import java.util.Optional;

/** Records an Xsam record pair - lots of data sets are paired-end in nature
 *
 * Upon construction, only the two records are stored.
 * All querying of fields is done on demand, to save time.
 *
 */
public class PairedXsamRecord extends PairedRecordBase<XsamRecord> {

  private final XsamRecord record1;
  private final XsamRecord record2;

  private int xL = -1;
  private int xR = -1;
  private int xS = -1;
  private int xW = -1;
  private int xP = -1;
  private int xQ = -1;
  private String xC;
  private String xD;

  private final Optional<String> xLSeq;
  private final Optional<String> xRSeq;

  /** Simple constructor, neither param may be null.
   *  A further check is made to see that the IDs are matching
   * @param record1 first record
   * @param record2 second record
   * @throws IllegalArgumentException if records are null, or IDs do not match
   */
  public PairedXsamRecord(XsamRecord record1, XsamRecord record2) {

    this.record1 = record1;
    this.record2 = record2;

    if(record1 == null){
      throw new IllegalArgumentException("Record 1 cannot be null");
    }

    if(record2 == null){
      throw new IllegalArgumentException("Record 2 cannot be null");
    }

    if(!record1.getId().equals(record2.getId())){
      throw new IllegalArgumentException("Record 1 and Record 2 id's do not match");
    }

    if(getxL() == -1 && record1.isMapped()){
      throw new IllegalArgumentException("Paired Xsam fields are missing");
    }

    xLSeq = XsamReadQueries.findxLSeqField(record1.getRead());
    xRSeq = XsamReadQueries.findxRSeqField(record2.getRead());


  }


  /** Overloaded constructor to allow Strings
   * @param record1 record 1 in String format
   * @param record2 recrod 2 in String format
   */
  public PairedXsamRecord(String record1, String record2){
    this(new XsamRecord(record1), new XsamRecord(record2));
  }

  @Override
  public XsamRecord getRecord1() {
    return record1;
  }

  @Override
  public XsamRecord getRecord2() {
    return record2;
  }

  /** left end of the read-pair
   * @return integer, xL
   */
  public int getxL() {
    if(xL == -1){
      xL = XsamReadQueries.findxLField(getRecord1().getRead());
    }

    return xL;
  }

  /** right end of the read-pair
   * @return integer, xL
   */
  public int getxR() {
    if(xR == -1){
      xR = XsamReadQueries.findxRField(getRecord1().getRead());
    }

    return xR;
  }

  /** span length of the pair, denoted as the count of X - |XXXXXXXXXXXXXXX|XXXX|XXXXXXXXXXXXXX|
   * @return integer, xS
   */
  public int getxS() {
    if(xS == -1){
      xS = XsamReadQueries.findxSField(getRecord1().getRead());
    }

    return xS;
  }

  /** window length of the pair, denoted as the count of X -  |-------------|XXXXX|--------------|
   * @return integer, xW
   */
  public int getxW() {
    if(xW == -1){
      xW = XsamReadQueries.findxWField(getRecord1().getRead());
    }

    return xW;
  }

  /** integer count of the number of duplicates, P is this duplicates number
   * @return integer, xP
   */
  public int getxP() {
    if(xP == -1){
      xP = XsamReadQueries.findxPField(getRecord1().getRead());
    }

    return xP;
  }

  /** Stores information on whether this read-pair is a duplicate or original (employed for PPD read pairs only)
   * @return 1 for duplicate, 0 for original
   */
  public int getxQ() {
    if(xQ == -1){
      xQ = XsamReadQueries.findxQField(getRecord1().getRead());
    }

    return xQ;
  }

  /** currently a spare field
   * @return String, xC
   */
  public String getxC() {
    if(xC == null){
      xC = XsamReadQueries.findxCField(getRecord1().getRead());
    }

    return xC;
  }

  /** currently a spare field
   * @return String, xD
   */
  public String getxD() {
    if(xD == null){
      xD = XsamReadQueries.findxDField(getRecord1().getRead());
    }

    return xD;
  }

  /** xLSeq, if present
   *
   * @return Optional, String
   */
  public Optional<String> getxLSeq() {
    return xLSeq;
  }

  /** xRSeq, if present
   *
   * @return Optional, String
   */
  public Optional<String> getxRSeq() {
    return xRSeq;
  }

  public boolean areReferenceNamesMatching(){
    return record1.getMateReferenceName().equals("=");
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    PairedXsamRecord that = (PairedXsamRecord) o;
    return record1.equals(that.record1) &&
            record2.equals(that.record2);
  }

  @Override
  public int hashCode() {
    return Objects.hash(record1, record2);
  }

  @Override
  public String toString() {
    return record1.toString() + '\n' + record2.toString();
  }
}
