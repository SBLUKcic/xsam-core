package com.sbl.model;

import com.sbl.utils.XsamReadQueries;

import java.util.StringJoiner;

/** Class for storing and working with Xsam formatted DNA sequence.
 *
 * Upon construction, only the String record is stored.
 * All querying of fields is done on demand, to save time.
 */
public class XsamRecord extends SamRecord {

  /** left position of the read */
  private int xl;
  /** right position of the read */
  private int xr;
  /** span length of the read */
  private int xs;
  /** mapping orientation of the read */
  private char xd;
  /** mapping type (u,r,x) of the read */
  private char xm;
  /** left position of the read */
  private String xa = "";

  /** Constructor for single records
   * @param read single read, must not be null
   * If read is null, Illegal Argument Exception will be thrown
   */
  public XsamRecord(String read) {

    super(read);

    if(getXl() == -1){
      throw new IllegalArgumentException("Input read must not be a .sam record, expected Xsam fields - xl:i:...");
    }

  }

  /** left position of the read
   * @return int position
   */
  public int getXl() {
    if(xl == 0){
      xl = XsamReadQueries.findxlField(getRead());
    }

    return xl;
  }


  /** right position of the read
   * @return int position
   */
  public int getXr() {
    if(xr == 0){
      xr = XsamReadQueries.findxrField(getRead());
    }

    return xr;
  }


  /** span length of the read
   * @return int span length
   */
  public int getXs() {
    if(xs == 0){
      xs = XsamReadQueries.findxsField(getRead());
    }

    return xs;
  }


  /** mapping orientation of the read
   * @return char oritentation, f or r
   */
  public char getXd() {
    if(xd == 0){
      xd = XsamReadQueries.findxdField(getRead());
    }

    return xd;
  }


  /** mapping type (u,r,x) of the read
   * @return char type
   */
  public char getXm() {
    if(xm == 0){
      xm = XsamReadQueries.findxmField(getRead());
    }

    return xm;
  }

  /** spare term
   * @return int position
   */
  public String getXa() {

    return xa;
  }

  /** Pulls apart the sam-only components and returns a new SamRecord based upon it.
   * @return valid SamRecord
   */
  public SamRecord convertToSam(){

    StringJoiner samBuilder = new StringJoiner("\t");
    samBuilder.add(super.getId());
    samBuilder.add(String.valueOf(super.getFlag()));
    samBuilder.add(super.getReferenceName());
    samBuilder.add(String.valueOf(super.getPos()));
    samBuilder.add(String.valueOf(super.getMappingQuality()));
    samBuilder.add(super.getCigar());
    samBuilder.add(super.getMateReferenceName());
    samBuilder.add(String.valueOf(super.getMatePosition()));
    samBuilder.add(String.valueOf(super.getTemplateLength()));
    samBuilder.add(super.getSequence());
    samBuilder.add(super.getQuality());
    for(String optional : super.getVariableTerms()){
        samBuilder.add(optional);
    }

    return new SamRecord(samBuilder.toString());
  }





  @Override
  public String toString() {
    return getRead();
  }
}
