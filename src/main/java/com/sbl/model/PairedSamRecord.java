package com.sbl.model;

import java.util.Objects;

/** Records an sam record pair - lots of data sets are paired-end in nature */
public class PairedSamRecord extends PairedRecordBase<SamRecord> {

  private final SamRecord record1;
  private final SamRecord record2;

  /** Simple constructor, neither param may be null.
   * @param record1 first record
   * @param record2 second record
   */
  public PairedSamRecord(SamRecord record1, SamRecord record2) {

    if(record1 == null){
      throw new IllegalArgumentException("Record 1 cannot be null");
    }

    if(record2 == null){
      throw new IllegalArgumentException("Record 2 cannot be null");
    }

    this.record1 = record1;
    this.record2 = record2;
  }

  @Override
  public SamRecord getRecord1() {
    return record1;
  }

  @Override
  public SamRecord getRecord2() {
    return record2;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    PairedSamRecord that = (PairedSamRecord) o;
    return Objects.equals(record1, that.record1) &&
        Objects.equals(record2, that.record2);
  }

  @Override
  public int hashCode() {
    return Objects.hash(record1, record2);
  }

  @Override
  public String toString() {
    return record1.toString() + '\n' + record2.toString();
  }
}
