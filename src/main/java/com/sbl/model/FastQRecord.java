package com.sbl.model;

import java.util.Objects;

public class FastQRecord {

    private final String id;
    private final String sequence;
    private final String quality;

    public FastQRecord(String id, String sequence, String quality) {
        this.id = id;
        this.sequence = sequence;
        this.quality = quality;
    }

    public String getId() {
        return id;
    }

    public String getSequence() {
        return sequence;
    }

    public String getQuality() {
        return quality;
    }

    @Override
    public String toString() {
        return id + "\n" + sequence + "\n+\n" + quality;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FastQRecord that = (FastQRecord) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
