package com.sbl.model.xsamstrands;

import java.util.Arrays;
import java.util.List;

public enum PairedXsamRecordStrand {
    ff("ff"),
    fr("fr"),
    rf("rf"),
    rr("rr"),
    xx("xx"),
    rx("rx", "xr"),
    fx("fx", "xf");

    private final List<String> acceptedStrands;

    PairedXsamRecordStrand(String... acceptedStrands) {
        this.acceptedStrands = Arrays.asList(acceptedStrands);
    }

    public List<String> getAcceptedStrands() {
        return acceptedStrands;
    }

}
