package com.sbl.model.xsamstrands;

import java.util.Arrays;
import java.util.List;

/** An xsam record mapping strand - denoting the direction of the mapping.
 *
 */
public enum XsamRecordStrand {
  a('f', 'r', 'x'),
  p('f', 'r'),
  f('f'),
  r('r'),
  x('x');

  private final List<Character> subStrands;

  XsamRecordStrand(Character... subStrands) {
    this.subStrands = Arrays.asList(subStrands);
  }

  public List<Character> getSubStrands() {
    return subStrands;
  }
}
