package com.sbl.model.xsamstrands;

import java.util.Arrays;
import java.util.List;

public enum PairedXsamRecordStrandCombination {
    aaa(PairedXsamRecordStrand.ff, PairedXsamRecordStrand.fr, PairedXsamRecordStrand.rf, PairedXsamRecordStrand.rr, PairedXsamRecordStrand.xx, PairedXsamRecordStrand.rx, PairedXsamRecordStrand.fx),
    aae(PairedXsamRecordStrand.ff, PairedXsamRecordStrand.rr, PairedXsamRecordStrand.xx),
    aad(PairedXsamRecordStrand.fr, PairedXsamRecordStrand.rf,  PairedXsamRecordStrand.fx, PairedXsamRecordStrand.rx ),
    ppe(PairedXsamRecordStrand.ff, PairedXsamRecordStrand.rr),
    ppd(PairedXsamRecordStrand.fr),
    ppa(PairedXsamRecordStrand.ff, PairedXsamRecordStrand.rr, PairedXsamRecordStrand.fr, PairedXsamRecordStrand.rf),
    px(PairedXsamRecordStrand.rx, PairedXsamRecordStrand.fx),
    fx(PairedXsamRecordStrand.fx),
    rx(PairedXsamRecordStrand.rx),
    ff(PairedXsamRecordStrand.ff),
    rr(PairedXsamRecordStrand.rr),
    fr(PairedXsamRecordStrand.fr),
    rf(PairedXsamRecordStrand.rf),
    pfe(PairedXsamRecordStrand.ff),
    fpe(PairedXsamRecordStrand.ff),
    fpd(PairedXsamRecordStrand.fr),
    pfd(PairedXsamRecordStrand.fr),
    xx(PairedXsamRecordStrand.xx);


    private final List<PairedXsamRecordStrand> subTypes;

    PairedXsamRecordStrandCombination(PairedXsamRecordStrand... subTypes) {
        this.subTypes = Arrays.asList(subTypes);
    }

    public List<PairedXsamRecordStrand> getSubTypes() {
        return subTypes;
    }
}
