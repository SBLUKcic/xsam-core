package com.sbl.model;

import com.sbl.model.xsamtypes.PairedXsamRecordMappingCombination;

public interface PairedRecord<T extends Record> {

  T getRecord1();

  T getRecord2();

  PairedXsamRecordMappingCombination getMappingCombination();

  FastQRecord getFastQRecord1();

  FastQRecord getFastQRecord2();

}
