package com.sbl.model;

import com.sbl.utils.XsamReadQueries;

import java.util.List;
import java.util.Objects;

/** Class for storing and working with sam formatted DNA sequence.
 *
 * Upon construction, only the String record is stored.
 * All querying of fields is done on demand, to save time.
 *
 */
public class SamRecord implements Record {

  private final String read;
  private int[] tabIndexes;
  private String id = null;
  private int flag = -1;
  private String referenceName = null;
  private int pos = -1;
  private int mappingQuality = -1;
  private String cigar = null;
  private String mateReferenceName = null;
  private int matePosition = -1;
  private int templateLength = -1;
  private String sequence = null;
  private String quality = null;
  private List<String> variableTerms = null;

  private final static String REPEAT_TERM = "ZS:Z:R";
  private final static String MATCH_TERM = "ZS:Z:NM";
  private final static String QUALITY_CHECK_TERM = "ZS:Z:QC";

  private final static int TAB_INDEX_END_OF_ID = 0;
  private final static int TAB_INDEX_END_OF_FLAG = 1;
  private final static int TAB_INDEX_END_OF_REFERENCE_NAME = 2;
  private final static int TAB_INDEX_END_OF_POS = 3;
  private final static int TAB_INDEX_END_OF_MAPPING_QUALITY = 4;
  private final static int TAB_INDEX_END_OF_CIGAR = 5;
  private final static int TAB_INDEX_END_OF_MATE_REF_NAME = 6;
  private final static int TAB_INDEX_END_OF_MATE_POS = 7;
  private final static int TAB_INDEX_END_OF_TEMPLATE_LENGTH = 8;
  private final static int TAB_INDEX_END_OF_SEQUENCE = 9;
  private final static int TAB_INDEX_END_OF_QUALITY = 10;


  /** Simple constructor for the sam record
   * @param read full read
   */
  public SamRecord(String read) {

    if(read == null){
      throw new IllegalArgumentException("Input read cannot be null");
    }

    this.read = read;
    tabIndexes = XsamReadQueries.findTabIndexes(this.read);
  }

  public String getRead() {
    return read;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getId() {
    if(id == null){
      id = this.read.substring(0, tabIndexes[TAB_INDEX_END_OF_ID]);
    }

    return id;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public int getFlag() throws NumberFormatException {
    if(flag == -1) {
      flag = Integer.parseInt(
              this.read.substring(
                      tabIndexes[TAB_INDEX_END_OF_ID]+1,
                      tabIndexes[TAB_INDEX_END_OF_FLAG])
      );
    }
    return flag;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getReferenceName() {
    if(referenceName == null){
      referenceName = this.read.substring(
              tabIndexes[TAB_INDEX_END_OF_FLAG]+1,
              tabIndexes[TAB_INDEX_END_OF_REFERENCE_NAME]);
    }

    return referenceName;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public int getPos() throws NumberFormatException{
    if(pos == -1){
      pos = Integer.parseInt(this.read.substring(
              tabIndexes[TAB_INDEX_END_OF_REFERENCE_NAME]+1,
              tabIndexes[TAB_INDEX_END_OF_POS]));
    }

    return pos;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public int getMappingQuality() throws NumberFormatException {
    if(mappingQuality == -1){
      mappingQuality = Integer.parseInt(this.read.substring(
              tabIndexes[TAB_INDEX_END_OF_POS]+1,
              tabIndexes[TAB_INDEX_END_OF_MAPPING_QUALITY])
      );
    }

    return mappingQuality;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getCigar() {
    if(cigar == null){
      cigar = this.read.substring(
              tabIndexes[TAB_INDEX_END_OF_MAPPING_QUALITY]+1,
              tabIndexes[TAB_INDEX_END_OF_CIGAR]);
    }

    return cigar;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getMateReferenceName() {
    if(mateReferenceName == null){
      mateReferenceName = this.read.substring(
              tabIndexes[TAB_INDEX_END_OF_CIGAR]+1,
              tabIndexes[TAB_INDEX_END_OF_MATE_REF_NAME]);
    }

    return mateReferenceName;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public int getMatePosition() throws NumberFormatException {
    if(matePosition == -1){
      matePosition = Integer.parseInt(
              this.read.substring(
                      tabIndexes[TAB_INDEX_END_OF_MATE_REF_NAME]+1,
                      tabIndexes[TAB_INDEX_END_OF_MATE_POS])
      );
    }

    return matePosition;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public int getTemplateLength() throws NumberFormatException {
    if(templateLength == -1){
      templateLength = Integer.parseInt(this.read.substring(
              tabIndexes[TAB_INDEX_END_OF_MATE_POS]+1,
              tabIndexes[TAB_INDEX_END_OF_TEMPLATE_LENGTH])
      );
    }

    return templateLength;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getSequence() {
    if(sequence == null){
      sequence = this.read.substring(
              tabIndexes[TAB_INDEX_END_OF_TEMPLATE_LENGTH]+1,
              tabIndexes[TAB_INDEX_END_OF_SEQUENCE]);
    }

    return sequence;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getQuality() {
    if(quality == null){
      quality = this.read.substring(
              tabIndexes[TAB_INDEX_END_OF_SEQUENCE]+1,
              tabIndexes[TAB_INDEX_END_OF_QUALITY]);
    }

    return quality;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isRepeat() {
    return read.contains(REPEAT_TERM);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isMapped() {
    return !read.contains(MATCH_TERM);
  }


  /**
   * {@inheritDoc}
   * @return
   */
  @Override
  public List<String> getVariableTerms() {
    if(variableTerms == null){
      variableTerms = XsamReadQueries.findVariableRegions(read, TAB_INDEX_END_OF_QUALITY);
    }
    return variableTerms;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isQualityFailed() {
    return read.contains(QUALITY_CHECK_TERM);
  }

  @Override
  public FastQRecord convertToFastQ(int number) {
    return new FastQRecord(String.format("@%s/%d", getId(), number), getSequence(), getQuality());
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    SamRecord samRecord = (SamRecord) o;
    return read.equals(samRecord.read);
  }

  @Override
  public int hashCode() {
    return Objects.hash(read);
  }

  @Override
  public String toString() {
    return read;
  }


  public enum OptionalField{

    AM("AM"),
    AS("AS"),
    BC("BC"),
    BQ("BQ"),
    BZ("BZ"),
    CB("CB"),
    CC("CC"),
    CG("CG"),
    CM("CM"),
    CO("CO"),
    CP("CP"),
    CQ("CQ"),
    CR("CR"),
    CS("CS"),
    CT("CT"),
    CY("CY"),
    E2("E2"),
    FI("FI"),
    FS("FS"),
    FZ("FZ"),
    GC("GC"),
    GQ("GQ"),
    GS("GS"),
    H0("H0"),
    H1("H1"),
    H2("H2"),
    HI("HI"),
    IH("IH"),
    LB("LB"),
    MC("MC"),
    MD("MD"),
    MF("MF"),
    MI("MI"),
    MQ("MQ"),
    NH("NH"),
    NM("NM"),
    OA("OA"),
    OC("OC"),
    OP("OP"),
    OQ("OQ"),
    OX("OX"),
    PG("PG"),
    PQ("PQ"),
    PT("PT"),
    PU("PU"),
    Q2("Q2"),
    QT("QT"),
    QX("QX"),
    R2("R2"),
    RG("RG"),
    RT("RT"),
    RX("RX"),
    S2("S2"),
    SA("SA"),
    SM("SM"),
    SQ("SQ"),
    TC("TC"),
    U2("U2"),
    UQ("UQ"),
    X("X*"),
    Y("Y*"),
    Z("Z*");

    private String tag;

    OptionalField(String tag) {
      this.tag = tag;
    }

    public static boolean isFieldOptional(String field){
      for(OptionalField o : OptionalField.values()){
        if(field.charAt(0) == o.tag.charAt(0) && (field.charAt(1) == o.tag.charAt(1) || o.tag.charAt(1) == '*')){
          return true;
        }
      }
      return false;
    }


  }
}
