package com.sbl.model;

import java.util.List;

/** interface that all sam-derived formats must implement.
 *
 */
public interface Record {

  /** Query template NAME, or ID
   * @return String
   */
  String getId();

  /** bitwise FLAG
   * @return Integer
   */
  int getFlag() throws NumberFormatException;

  /** References sequence NAME
   * @return String
   */
  String getReferenceName();

  /** 1- based leftmost mapping POSition
   * @return Integer
   */
  int getPos() throws NumberFormatException;

  /** MAPping Quality
   * @return Integer
   */
  int getMappingQuality() throws NumberFormatException;

  /** CIGAR String
   * @return String
   */
  String getCigar();

  /** Ref. name of the mate/next read
   * @return String
   */
  String getMateReferenceName();

  /** Position of the mate/next read
   * @return Integer
   */
  int getMatePosition() throws NumberFormatException;

  /**  observed Template LENgth
   * @return Integer
   */
  int getTemplateLength() throws NumberFormatException;

  /** segment SEQuence
   * @return String
   */
  String getSequence();

  /** ASCII of Phred-scaled base QUALity+33
   * @return String
   */
  String getQuality();


  /** Determine if the read is repeated using the ZS:Z:R field
   * @return true for repeat, false for unique
   */
  boolean isRepeat();

  /** Determine if the read is mapped or not, using the ZS:Z:NM field
   * @return true is mapped, false if not.
   */
  boolean isMapped();

  /** Get the variable terms from the end of the Sam record
   * @return A string comprising the variable terms, may be empty if none exist
   */
  List<String> getVariableTerms();


  /** Determines if the read failed QC, using the ZS:Z:ZR field
   * @return true if QC failed, false is passed
   */
  boolean isQualityFailed();


  /** Should convert the implementing record into fastq format.
   * @return valid FastQRecord.
   */
  FastQRecord convertToFastQ(int number);


}
