package com.sbl.model.xsamtypes;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public enum XsamRecordMappingType {
  A(Arrays.asList(
      'u',
      'r',
      'x'
    )
  ),
  P(Arrays.asList(
    'u',
    'r'
    )
  ),
  U(Collections.singletonList('u')),
  R(Collections.singletonList('r')),
  X(Collections.singletonList('x')),
  Z(Arrays.asList(
      'r',
      'x'
    )
  );

  List<Character> subTypes;

  XsamRecordMappingType(List<Character> subTypes) {
    this.subTypes = subTypes;
  }

  public List<Character> getSubTypes() {
    return subTypes;
  }
}
