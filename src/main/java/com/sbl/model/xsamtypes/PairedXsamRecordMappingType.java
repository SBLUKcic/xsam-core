package com.sbl.model.xsamtypes;

import java.util.Arrays;
import java.util.List;

/**
 * Paired end mapping type possibilities
 * <ul>
 * <li>U - Unique xm:A:u</li>
 * <li>R - Repeat xm:A:r</li>
 * <li>X - Non-mapped xm:A:x</li>
 * <li>Endings:
 * <ul>
 * <li>E - rnames match</li>
 * <li>D - rnames don't match</li>
 * </ul>
 * </li>
 * </ul>
 */
public enum PairedXsamRecordMappingType {
  UUE( true, "uu"),
  UUD(false, "uu" ),
  RRE(true, "rr"),
  RRD(false, "rr"),
  URE(true, "ur", "ru"),
  URD(false, "ur", "ru"),
  RUE(true, "ur", "ru"),
  RUD(false, "ur", "ru"),
  UX(true, "ux", "xu"),
  RX(true, "rx", "xr"),
  XX(true, "xx");

  private List<String> acceptedTypes;
  private boolean referenceNamesMatchRequired;

  PairedXsamRecordMappingType(boolean referenceNamesMatchRequired, String... acceptedTypes) {
    this.acceptedTypes = Arrays.asList(acceptedTypes);
    this.referenceNamesMatchRequired = referenceNamesMatchRequired;
  }

  public boolean doReadsMatchMappingType(char type1, char type2, boolean referenceNamesMatch){
    if(this.referenceNamesMatchRequired == referenceNamesMatch && containsType(type1, type2)){
      return true;
    }
    return false;
  }

  private boolean containsType(char type1, char type2){
    for(String type : acceptedTypes){
      if(type.charAt(0) == type1 && type.charAt(1) == type2){
        return true;
      }
    }
    return false;
  }



}
