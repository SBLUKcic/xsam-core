package com.sbl.model.xsamtypes;

import com.sbl.model.PairedXsamRecord;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public enum PairedXsamRecordMappingCombination {

  AAA("All", PairedXsamRecordMappingType.values()),
  AAE("All with reference name matching",
      PairedXsamRecordMappingType.UUE,
      PairedXsamRecordMappingType.RRE,
      PairedXsamRecordMappingType.URE,
      PairedXsamRecordMappingType.RUE
  ),
  AAD("All with reference names not matching",
      PairedXsamRecordMappingType.UUD,
      PairedXsamRecordMappingType.RRD,
      PairedXsamRecordMappingType.URD,
      PairedXsamRecordMappingType.RUD
  ),
  PX("Positioned - X",
          PairedXsamRecordMappingType.UX,
          PairedXsamRecordMappingType.RX),
  XP("Positioned - X",
          PairedXsamRecordMappingType.UX,
          PairedXsamRecordMappingType.RX),
  AX("Anything - X",
          PairedXsamRecordMappingType.UX,
          PairedXsamRecordMappingType.RX,
          PairedXsamRecordMappingType.XX),
  XA("Anything - X",
          PairedXsamRecordMappingType.UX,
          PairedXsamRecordMappingType.RX,
          PairedXsamRecordMappingType.XX),
  UX("Unique - X", PairedXsamRecordMappingType.UX),
  RX("Unique - X", PairedXsamRecordMappingType.RX),
  XU("Unique - X", PairedXsamRecordMappingType.UX),
  XR("Unique - X", PairedXsamRecordMappingType.RX),
  XX("Unique - X", PairedXsamRecordMappingType.XX),
  UUE("Unique, Unique, matching rname",
          PairedXsamRecordMappingType.UUE),
  UUD("Unique, Unique, different rname",
          PairedXsamRecordMappingType.UUD),
  RRE("Repeat, Repeat, matching rname",
          PairedXsamRecordMappingType.RRE),
  URE("Unique, Repeat, matching rname",
          PairedXsamRecordMappingType.URE,
          PairedXsamRecordMappingType.RUE),
  URA("Unique, Repeat, Any",
          PairedXsamRecordMappingType.URE,
          PairedXsamRecordMappingType.RUE,
          PairedXsamRecordMappingType.URD,
          PairedXsamRecordMappingType.RUD),
  UUA("Unique, Unique, Any",
          PairedXsamRecordMappingType.UUE,
          PairedXsamRecordMappingType.UUD),
  RUA("Unique, Repeat, Any",
          PairedXsamRecordMappingType.URE,
          PairedXsamRecordMappingType.RUE,
          PairedXsamRecordMappingType.URD,
          PairedXsamRecordMappingType.RUD),
  RRD("Repeat, Repeat, Different",
          PairedXsamRecordMappingType.RRD),
  URD("Unique, Repeat, Different",
          PairedXsamRecordMappingType.URD,
          PairedXsamRecordMappingType.RUD),
  RUE("Repeat, Unique, Equal",
          PairedXsamRecordMappingType.URE,
          PairedXsamRecordMappingType.RUE),
  RUD("Repeat, Unique, Different",
          PairedXsamRecordMappingType.RUD,
          PairedXsamRecordMappingType.URD),
  PPA("Any mapped reads",
          PairedXsamRecordMappingType.UUE,
          PairedXsamRecordMappingType.RRE,
          PairedXsamRecordMappingType.URE,
          PairedXsamRecordMappingType.RUE,
          PairedXsamRecordMappingType.UUD,
          PairedXsamRecordMappingType.RRD,
          PairedXsamRecordMappingType.URD,
          PairedXsamRecordMappingType.RUD

  ),
  PAA("Anything but XX",
          PairedXsamRecordMappingType.UUE,
          PairedXsamRecordMappingType.RRE,
          PairedXsamRecordMappingType.URE,
          PairedXsamRecordMappingType.RUE,
          PairedXsamRecordMappingType.UUD,
          PairedXsamRecordMappingType.RRD,
          PairedXsamRecordMappingType.URD,
          PairedXsamRecordMappingType.RUD,
          PairedXsamRecordMappingType.UX,
          PairedXsamRecordMappingType.RX
  ),
  RRA("Any repeats",
          PairedXsamRecordMappingType.RRE,
          PairedXsamRecordMappingType.RRD
  ),
  UAA("Unique any any",
          PairedXsamRecordMappingType.UUD,
          PairedXsamRecordMappingType.UUE,
          PairedXsamRecordMappingType.URD,
          PairedXsamRecordMappingType.URE,
          PairedXsamRecordMappingType.UX
  ),
  UAE("Unique, Any, Equals",
          PairedXsamRecordMappingType.UUE,
          PairedXsamRecordMappingType.UX,
          PairedXsamRecordMappingType.URE),
  RAA("Repeat, Any, Any",
          PairedXsamRecordMappingType.URE,
          PairedXsamRecordMappingType.URD,
          PairedXsamRecordMappingType.RUD,
          PairedXsamRecordMappingType.RUE,
          PairedXsamRecordMappingType.RX),
  AUA("Any, Unique, Any",
          UAA.getSubTypes()),
  ARA("Any, Repeat, Any",
          RAA.getSubTypes()),
  APA("Any, Positioned, Any",
          PAA.getSubTypes()),
  APE("Any, Positioned, Equal",
          PairedXsamRecordMappingType.UUE,
          PairedXsamRecordMappingType.RRE,
          PairedXsamRecordMappingType.URE,
          PairedXsamRecordMappingType.RUE),
  PAE("Positioned, Any, Equal",
          APE.getSubTypes()),
  APD("Any, Positioned, Different",
          PairedXsamRecordMappingType.UUD,
          PairedXsamRecordMappingType.RRD,
          PairedXsamRecordMappingType.URD,
          PairedXsamRecordMappingType.RUD),
  PAD("Positioned, Any, Different",
          APD.getSubTypes()),
  PPE("Both reads mapped to the same reference name", AAE.getSubTypes()),
  PPD("Both reads mapped to different reference names", AAD.getSubTypes()),
  RA("Repeat, Any, Any",
          RAA.getSubTypes()),
  UA("Unique, Any, Any",
          UAA.getSubTypes()),
  PA("Positioned, Any, Any",
          PAA.getSubTypes()),
  PP("Positioned, Positioned, Any",
          PPA.getSubTypes()),
  RR("Repeat, Repeat, Any",
          RRA.getSubTypes()),
  UU("Unique, Unique, Any",
          UUA.getSubTypes()),
  UR("Unique, Repeat, Any",
          URA.getSubTypes()),
  RU("Repeat, Unique, Any",
          RUA.getSubTypes()),
  UZA("Unique, any but unique, Any",
          PairedXsamRecordMappingType.UX,
          PairedXsamRecordMappingType.URE,
          PairedXsamRecordMappingType.URD,
          PairedXsamRecordMappingType.RUD,
          PairedXsamRecordMappingType.RUE,
          PairedXsamRecordMappingType.UX),
  UZ("Unique, any but unique, Any",
          UZA.getSubTypes()),
  UZE("Unique, any but unique, Equal",
          PairedXsamRecordMappingType.URE,
          PairedXsamRecordMappingType.RUE),
  UZD("Unique, any but unique, Different",
          PairedXsamRecordMappingType.URD,
          PairedXsamRecordMappingType.RUD);

  private final String meaning;
  private final List<PairedXsamRecordMappingType> subTypes;

  PairedXsamRecordMappingCombination(String meaning, PairedXsamRecordMappingType... subTypes) {
    this.meaning = meaning;
    this.subTypes = Arrays.asList(subTypes);
  }

  PairedXsamRecordMappingCombination(String meaning, List<PairedXsamRecordMappingType> subTypes) {
    this.meaning = meaning;
    this.subTypes = subTypes;
  }

  public String getMeaning() {
    return meaning;
  }

  public List<PairedXsamRecordMappingType> getSubTypes() {
    return subTypes;
  }
}
