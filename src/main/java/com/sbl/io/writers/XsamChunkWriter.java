package com.sbl.io.writers;

import com.sbl.io.XsamChunk;
import com.sbl.io.header.XsamHeaderLine;

import java.io.File;
import java.io.IOException;

public interface XsamChunkWriter {

    XsamChunk writeBytesToFile(File master, File location, XsamHeaderLine line, long headerBytes) throws IOException;
}
