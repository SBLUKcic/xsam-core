package com.sbl.io.writers;

import com.sbl.io.XsamChunk;
import com.sbl.io.header.XsamHeaderLine;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;

public class XsamChunkWriterImpl implements XsamChunkWriter {

    @Override
    public XsamChunk writeBytesToFile(File master, File location, XsamHeaderLine line, long headerBytes) throws IOException {
        if(line.getNumBytes() == 0L) return null;
        int lineCount = 0;

        if(master == null){
            throw new IOException("Master cannot be null");
        }

        if(location == null){
            throw new IOException("Chunk file cannot be null");
        }

        if(!location.createNewFile() && !location.exists()){
            throw new IOException("Could not create chunk file: " + location);
        }

        try(RandomAccessFile raf = new RandomAccessFile(master, "r")){


            try(FileOutputStream pw = new FileOutputStream(location)) {
                raf.seek(headerBytes + line.getStartByte());

                final int BUFFER_SIZE = 4096;

                byte[] buffer = new byte[BUFFER_SIZE];
                long bytesLeft = line.getNumBytes();


                while (bytesLeft > BUFFER_SIZE) {
                    raf.read(buffer);
                    pw.write(buffer);
                    for(byte b: buffer){
                        if(b == '\n') lineCount++;
                    }
                    bytesLeft -= BUFFER_SIZE;
                }

                if(bytesLeft > 0){
                    buffer = new byte[(int)bytesLeft];
                    raf.read(buffer);
                    for(byte b: buffer){
                        if(b == '\n') lineCount++;
                    }
                    pw.write(buffer);
                }

                pw.flush();

            }

        }

        return new XsamChunk(lineCount, location, line.getSpanSection(), line.getSection(), line.getReferenceName());
    }
}
