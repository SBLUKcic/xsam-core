package com.sbl.io;

import com.sbl.exceptions.ConversionException;
import com.sbl.io.writers.XsamChunkWriter;
import com.sbl.io.header.XsamSection;
import com.sbl.io.header.XsamHeaderLine;
import com.sbl.utils.XsamReadQueries;
import com.sbl.utils.XsamUtils;
import me.tongfei.progressbar.ProgressBar;
import me.tongfei.progressbar.ProgressBarBuilder;

import java.io.*;
import java.util.*;

/** Simple representation of a sam file
 *  File must not be null, empty and must exist.
 *  Xsam file must have a valid header to split the chunks correctly.
 */
public class XsamFile {

  private final File file;
  private final XsamChunkWriter writer;
  private final LinkedList<String> samHeaderReads = new LinkedList<>();
  private final LinkedList<String> xSamHeaderReads = new LinkedList<>();
  private final Set<XsamHeaderLine> pSection = new HashSet<>();
  private final Set<XsamHeaderLine> dSection = new HashSet<>();
  private XsamHeaderLine unmapped;
  private boolean isPaired = false;

  private long headerBytes = 0L;
  private long section1Limit;
  private long section2Limit;
  private long section3Limit;

  private File subFileDirectory;

  private List<XsamChunk> chunks = new LinkedList<>();

  private final static String HEADER_SECTION_PREFIX = "@CO\t";
  private final static String HEADER_SECTION_INFO_PREFIX = "@CO\tXsam:F:";
  private final static String HEADER_SECTION_SPAN_PREFIX = "@CO\tXsam:S:";

  private final static String SAM_HEADER_PREFIX = "@";

  /** Reads in the Xsam file and instantiates the header and file info.
   *  file must not be null, empty and must exist.
   * @param file Java File object representing the file location
   * @throws Exception - file is not valid.
   */
  public XsamFile(File file, XsamChunkWriter writer) throws Exception {

    if(file == null){
      throw new IllegalArgumentException("Input file cannot be null");
    }

    if(!file.exists()){
      throw new IllegalArgumentException("Input file must exist");
    }

    if(file.length() == 0){
      throw new IllegalArgumentException("Input file cannot be empty");
    }

    if(writer == null){
      throw new IllegalArgumentException("XsamChunkWriter cannot be null");
    }
    this.writer = writer;
    this.file = file;

    try(BufferedReader reader = new BufferedReader(new FileReader(file))){

      String line;
      while((line = reader.readLine()).startsWith(SAM_HEADER_PREFIX)){
        headerBytes += line.length() + 1; //+1 for the end line byte in the file
        if(line.startsWith(HEADER_SECTION_PREFIX)){
          xSamHeaderReads.add(line);
        }else{
          samHeaderReads.add(line);
        }

      }

      String secondLine = reader.readLine();

      if(line != null && secondLine != null){
         if(XsamReadQueries.findID(line).equals(XsamReadQueries.findID(secondLine))){
           this.isPaired = true;
         }
      }

    }catch (IOException ioe){
      throw new Exception("Error reading Xsam header: " + ioe.getMessage());
    }

    if(xSamHeaderReads.size() == 0){
      throw new IllegalArgumentException("Expected input xsam header lines - are you sure this is a valid Xsam file? ");
    }

    for(String s : xSamHeaderReads){
      if(s.startsWith(HEADER_SECTION_SPAN_PREFIX)){
        //catch the spans
        String p = getDataBetweenColons(2, s);
        switch (p) {
          case "P1":
            section1Limit = Long.parseLong(getDataBetweenColons(3, s));
            break;
          case "P2":
            section2Limit = Long.parseLong(getDataBetweenColons(3, s));
            break;
          default:
            section3Limit = Long.parseLong(getDataBetweenColons(3, s));
            break;
        }

      }else if(s.startsWith(HEADER_SECTION_PREFIX) && !s.startsWith(HEADER_SECTION_INFO_PREFIX)){
        XsamHeaderLine line = parseXsamHeaderLine(s);
        if(line.getSection() == XsamSection.P){
          pSection.add(line);
        }else if(line.getSection() == XsamSection.D){
          dSection.add(line);
        }else{
          unmapped = line;
        }
      }

    }

  }

  public boolean isPaired() {
    return isPaired;
  }

  public List<XsamChunk> getChunks() {
    return chunks;
  }

  public LinkedList<String> getSamHeaderReads() {
    return samHeaderReads;
  }

  public LinkedList<String> getxSamHeaderReads() {
    return xSamHeaderReads;
  }

  public Set<XsamHeaderLine> getpSection() {
    return pSection;
  }

  public Set<XsamHeaderLine> getdSection() {
    return dSection;
  }

  public XsamHeaderLine getUnmapped() {
    return unmapped;
  }

  public long getHeaderBytes() {
    return headerBytes;
  }

  public long getSection1Limit() {
    return section1Limit;
  }

  public long getSection2Limit() {
    return section2Limit;
  }

  public long getSection3Limit() {
    return section3Limit;
  }

  public File getFile() {
    return file;
  }

  public File getSubFileDirectory() {
    return subFileDirectory;
  }

  /** splits the io described by the Xsam file header lines into required directory
   * directory will be auto-created based on the xsam file name and current time.
   * will show progress by default;
   * @return true if successful, false if not.
   * @throws IOException something went wrong!
   */
  public boolean splitFiles() throws IOException{
    return splitFiles(true);
  }

  /** splits the io described by the Xsam file header lines into required directory
   * directory will be auto-created based on the xsam file name and current time.
   * @param showProgress show progress bar in system.out
   * @return true if successful, false if not.
   * @throws IOException something went wrong!
   */
  public boolean splitFiles(boolean showProgress) throws IOException{
    File directory = new File(System.getProperty("user.dir") + File.separator + String.format("%s_%d_dir", file.getName(), System.nanoTime()));

    if(!directory.mkdirs()){
      return false;
    }

    splitFiles(directory, showProgress);

    return true;
  }

  /** splits the io described by the Xsam file header lines into required directory
   *  will show progress bar by default;
   * @param directory location to split the io into
   * @return true if successful, false if not.
   * @throws IOException something went wrong!
   */
  public boolean splitFiles(File directory) throws IOException{
    return splitFiles(directory, true);
  }

  /** splits the io described by the Xsam file header lines into required directory
   * @param directory location to split the io into
   * @param showProgress show progress bar in system.out
   * @return true if successful, false if not.
   * @throws IOException something went wrong!
   */
  public boolean splitFiles(File directory, boolean showProgress) throws IOException{
    this.subFileDirectory = directory;

    if(directory == null){
      return false;
    }

    if(!directory.isDirectory()){
      return false;
    }

    this.chunks = new ArrayList<>(); //need to erase any previous chunks.

    ProgressBar pb = null;

    if(showProgress) {
      pb = new ProgressBarBuilder()
              .setInitialMax(pSection.size() + dSection.size() + 1)
              .setPrintStream(System.out)
              .setTaskName("Splitting " + this.file.getName())
              .build();
    }


    for(XsamHeaderLine line : pSection){


      writeChunkToFile(directory, line);

      if(showProgress){
        pb.step();
      }

    }

    for(XsamHeaderLine line : dSection){

      writeChunkToFile(directory, line);

      if(showProgress){
        pb.step();
      }
    }



    if(unmapped != null && unmapped.getNumBytes() != 0L){

      File outFile = new File(directory + File.separator + String.format("%s_%s_%s", unmapped.getSection(), unmapped.getReferenceName().replaceAll("/", "_"), String.valueOf(unmapped.getSpanSection())));

      if(outFile.createNewFile()){

        chunks.add(writer.writeBytesToFile(this.file, outFile, unmapped, headerBytes));

      }

    }

    if(showProgress){
      pb.step();
      pb.close();
    }

    return true;
  }

  private void writeChunkToFile(File directory, XsamHeaderLine line) throws IOException {
    if (line.getNumBytes() != 0L) {

      File outFile = new File(directory + File.separator + String.format("%s_%s_%s", line.getSection(), line.getReferenceName().replaceAll("/", "_"), String.valueOf(line.getSpanSection())));

      if (outFile.createNewFile()) {

        chunks.add(writer.writeBytesToFile(this.file, outFile, line, headerBytes));

      }

    }
  }

  private XsamHeaderLine parseXsamHeaderLine(String plainText) throws ConversionException {

    XsamSection readSection = XsamSection.findSectionFromString(getDataBetweenColons(1,  plainText));

    if(readSection == null){
      throw new ConversionException("Invalid read section, P, D or X allowed only");
    }

    if(readSection == XsamSection.X){

      long numReads = Long.parseLong(getDataBetweenColons(2, plainText));
      long numBytes = Long.parseLong(getDataBetweenColons(3, plainText));
      long startByte = Long.parseLong(getDataBetweenColons(4, plainText));

      return new XsamHeaderLine(numReads, numBytes, startByte, 1, XsamSection.X, "*");

    }

    String referenceName = getDataBetweenColons(2, plainText);
    int spanSection = Integer.parseInt(getDataBetweenColons(3, plainText));
    long numReads = Long.parseLong(getDataBetweenColons(4, plainText));
    long numBytes = Long.parseLong(getDataBetweenColons(5, plainText));
    long startByte = Long.parseLong(getDataBetweenColons(6, plainText));


    return new XsamHeaderLine(numReads, numBytes, startByte, spanSection, readSection, referenceName);
  }

  private String getDataBetweenColons(int position, String text){

    int colonCount = 0;
    int start = 0;
    int end =  text.length();

    for(int i = 0; i < text.length(); i++){
      if(text.charAt(i) == ':'){
        colonCount ++;
        if(colonCount == position){
          start = i+1;
        }
        if(colonCount == position + 1){
          end = i;
          break;
        }
      }
    }

    return text.substring(start, end);

  }



}
