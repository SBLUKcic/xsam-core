package com.sbl.io;

import com.sbl.io.header.XsamSection;
import com.sbl.utils.XsamUtils;

import java.io.File;
import java.io.IOException;
import java.util.Objects;

public class XsamChunk {

  private int numLines;
  private File location;
  private final int spanSection;
  private final XsamSection section;
  private final String referenceName;
  private final boolean isPaired;

  public XsamChunk(int numLines, File location, int spanSection, XsamSection section, String referenceName) {
    this.numLines = numLines;
    this.location = location;
    this.spanSection = spanSection;
    this.section = section;
    this.referenceName = referenceName;

    try {
      isPaired = XsamUtils.isFilePaired(location);
    } catch (IOException e) {
      throw new IllegalArgumentException("Location must be valid: " + e.getMessage());
    }
  }

  public boolean isPaired() {
    return isPaired;
  }

  public int getNumLines() {
    return numLines;
  }

  public File getLocation() {
    return location;
  }

  public int getSpanSection() {
    return spanSection;
  }

  public XsamSection getSection() {
    return section;
  }

  public String getReferenceName() {
    return referenceName;
  }

  public void incrementLineCount(int amount){
    numLines += amount;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    XsamChunk chunk = (XsamChunk) o;
    return spanSection == chunk.spanSection &&
            isPaired == chunk.isPaired &&
            section == chunk.section &&
            Objects.equals(referenceName, chunk.referenceName);
  }

  @Override
  public int hashCode() {
    return Objects.hash(spanSection, section, referenceName, isPaired);
  }
}
