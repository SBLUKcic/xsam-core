package com.sbl.io;

import com.sbl.io.header.SpanLimits;

import java.util.ArrayList;
import java.util.List;

public class XsamFileParameter {

    private List<String> headerLines = new ArrayList<>();
    private SpanLimits spanLimits = new SpanLimits();
    private String currentVersion;
    private boolean delete = false;
    private boolean sorting = true;

    public XsamFileParameter(String currentVersion) {
        this.currentVersion = currentVersion;
    }

    public List<String> getHeaderLines() {
        return headerLines;
    }

    public SpanLimits getSpanLimits() {
        return spanLimits;
    }

    public String getCurrentVersion() {
        return currentVersion;
    }

    public boolean isDelete() {
        return delete;
    }

    public boolean isSorting() {
        return sorting;
    }

    public static class XsamFileParameterBuilder {

        private List<String> headerLines = new ArrayList<>();
        private SpanLimits spanLimits = new SpanLimits();
        private String currentVersion;
        private boolean delete = false;
        private boolean sorting = true;

        public XsamFileParameterBuilder(String currentVersion) {
            this.currentVersion = currentVersion;
        }

        public XsamFileParameterBuilder withHeaderLines(List<String> lines){
            this.headerLines = lines;
            return this;
        }

        public XsamFileParameterBuilder withSpanLimits(SpanLimits spanLimits){
            this.spanLimits = spanLimits;
            return this;
        }

        public XsamFileParameterBuilder withDelete(){
            this.delete = true;
            return this;
        }

        public XsamFileParameterBuilder withoutSorting(){
            this.sorting = false;
            return this;
        }

        public XsamFileParameter build(){

            XsamFileParameter parameters = new XsamFileParameter(this.currentVersion);
            parameters.spanLimits = this.spanLimits;
            parameters.headerLines = this.headerLines;
            parameters.currentVersion = this.currentVersion;
            parameters.delete = this.delete;
            parameters.sorting = this.sorting;

            return parameters;

        }
    }
}
