package com.sbl.io.header;

public enum XsamSection {

  P("positioned on reference name", 1),
  D("positioned on different reference names - paired io only", 2),
  X("not positioned", 3);

  private final String meaning;
  private final int naturalPosition;

  XsamSection(String meaning, int naturalPosition) {
    this.meaning = meaning;
    this.naturalPosition = naturalPosition;
  }



  public String getMeaning() {
    return meaning;
  }

  public int getNaturalPosition() {
    return naturalPosition;
  }

  public static XsamSection findSectionFromString(String character){
    switch (character) {
      case "P":
        return XsamSection.P;
      case "D":
        return XsamSection.D;
      case "X":
        return XsamSection.X;
    }

    return null;
  }

}
