package com.sbl.io.header;

public class SpanLimits {

    private final long spanLimit1;
    private final long spanLimit2;
    private final long spanLimit3;

    public SpanLimits(long spanLimit1, long spanLimit2, long spanLimit3) {
        this.spanLimit1 = spanLimit1;
        this.spanLimit2 = spanLimit2;
        this.spanLimit3 = spanLimit3;
    }

    public SpanLimits() {
        this.spanLimit1 = 2000;
        this.spanLimit2 = 100000;
        this.spanLimit3 = 3000000;
    }

    public long getSpanLimit1() {
        return spanLimit1;
    }

    public long getSpanLimit2() {
        return spanLimit2;
    }

    public long getSpanLimit3() {
        return spanLimit3;
    }
}
