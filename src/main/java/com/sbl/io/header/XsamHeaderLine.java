package com.sbl.io.header;

import com.sbl.io.XsamChunk;

import java.io.*;
import java.util.Comparator;
import java.util.Objects;

public class XsamHeaderLine implements Comparable<XsamHeaderLine>{

  private final long numReads;
  private final long numBytes;
  private final long startByte;
  private final int spanSection;
  private final XsamSection section;
  private final String referenceName;
  private final static Comparator<XsamHeaderLine> DEFAULT_COMPARATOR = Comparator
          .comparing(XsamHeaderLine::getSection)
          .thenComparing(XsamHeaderLine::getReferenceName)
          .thenComparing(XsamHeaderLine::getSpanSection);

  public XsamHeaderLine(long numReads, long numBytes, long startByte, int spanSection, XsamSection section, String referenceName) {
    this.numReads = numReads;
    this.numBytes = numBytes;
    this.startByte = startByte;
    this.spanSection = spanSection;
    this.section = section;
    this.referenceName = referenceName;
  }

  public int getSpanSection() {
    return spanSection;
  }

  public long getNumReads() {
    return numReads;
  }

  public long getNumBytes() {
    return numBytes;
  }

  public long getStartByte() {
    return startByte;
  }

  public XsamSection getSection() {
    return section;
  }

  public String getReferenceName() {
    return referenceName;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    XsamHeaderLine that = (XsamHeaderLine) o;
    return spanSection == that.spanSection &&
        section == that.section &&
        Objects.equals(referenceName, that.referenceName);
  }

  @Override
  public int hashCode() {
    return Objects.hash(spanSection, section, referenceName);
  }

  @Override
  public int compareTo(XsamHeaderLine o) {
    return DEFAULT_COMPARATOR.compare(this, o);
  }

  @Override
  public String toString() {
    return section == XsamSection.X ?
            String.format("@CO\tXsam:%s:%d:%d:%d",
                section.name(),
                numReads,
                numBytes,
                startByte)
            : String.format("@CO\tXsam:%s:%s:%d:%d:%d:%d",
              section.name(),
              referenceName,
              spanSection,
              numReads,
              numBytes,
              startByte);
  }
}
